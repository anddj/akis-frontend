AKIS frontend application
=========================

This is AKIS (book indexing service) frontend application.

Created to assist user in [creating book index](https://en.wikipedia.org/wiki/Index_(publishing)#Indexing_process) process.

This application is part of AKIS book indexing platform:

```
  +---------------+     +------------------+     +-----------------------+
  |               |     |                  |     |                       |
  | Frontend app. +-----+   Backend app.   +-----+ Book indexing workers |
  |               |     |                  |     |                       |
  +---------------+     +------------------+     +-----------------------+
          ^
          |
          --- We are here!
```

Frontend technology stack:

  - [Create React App](https://github.com/facebook/create-react-app)
  - [React](https://reactjs.org/)
  - [Redux](https://redux.js.org/)
  - [Blueprintjs](https://blueprintjs.com/)
  - [Codemirror](https://github.com/scniro/react-codemirror2)

For production, application is built as PWA ([progressive web appplication](https://developers.google.com/web/progressive-web-apps/)).


Development
-----------

Clone the repository to your local environment.

Copy and edit configuration file:
```
cp src/config/appConfig.EXAMPLE.js src/config/appConfig.js
```

Run install ([yarn](https://yarnpkg.com/en/docs/install) is recommended):
```
yarn install
```

Starting development environment:
```
yarn start
```

Build for poduction:
```
yarn build
```

Run tests:
```
yarn test
```

`src` directory structure:

```bash
actions/     # Redux actions
components/  # React components
config/      # configuration files
containers/  # React containers components
index.js     # React app entry script
lib/         # helper functions, libraries
reducers/    # Redux reducers
services/    # fetch API calls
styles/      # SCSS styles
__tests__/   # Jest tests
```
