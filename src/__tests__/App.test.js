import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import '../styles/index.css';
import App from '../containers/App';
import store from '../lib/store';

it('renders without crashing', () => {
  const div = document.createElement('div');
  debugger;
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
