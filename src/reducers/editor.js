import {
  SET_EDITOR_INSTANCE,
  SET_EDITOR_GO_TO_COORDS,
  SET_EDITOR_LINKED_DOCS,
  SET_SHOW_CONCORDANCES
} from '../actions/actionTypes';

export const editorInstance = (state = null, action) => {
  switch (action.type) {
    case SET_EDITOR_INSTANCE:
      return action.editor;
    default:
      return state;
  }
};

export const editorGoToCoords = (state = null, action) => {
  switch (action.type) {
    case SET_EDITOR_GO_TO_COORDS:
      return Object.assign({}, action.editorGoToCoords);
    default:
      return state;
  }
};

export const editorLinkedDocs = (state = null, action) => {
  switch (action.type) {
    case SET_EDITOR_LINKED_DOCS:
      return Object.assign({}, action.editorLinkedDocs);
    default:
      return state;
  }
};

export const showConcordances = (state = false, action) => {
  switch (action.type) {
    case SET_SHOW_CONCORDANCES:
      return action.showConcordances;
    default:
      return state;
  }
};
