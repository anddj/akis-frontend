import { combineReducers } from 'redux';
import theme from './theme';
import userData from './userData';
import { showSettings } from './settings';
import { bookFinished } from './books';
import {
  bookItems,
  bookItemsBuffers,
  bookItemActive,
  savedContent,
  fileNamesMap
} from './bookItems';
import {
  bookItemIdsToIndex,
  currentIndexLocatorId,
  currentIndexNodeId,
  indexData,
  isGettingIndexData,
  locatorsButtons,
  nodesMarkers
} from './bookIndex';
import {
  editorGoToCoords,
  editorInstance,
  editorLinkedDocs,
  showConcordances
} from './editor';
import { logs, logsShowOnlyErrors } from './logs';
import { userSettings, fontScaling, concordanceLines } from './userSettings';

const rootReducer = combineReducers({
  bookFinished,
  bookItemActive,
  bookItemIdsToIndex,
  bookItems,
  bookItemsBuffers,
  concordanceLines,
  currentIndexLocatorId,
  currentIndexNodeId,
  editorGoToCoords,
  editorInstance,
  editorLinkedDocs,
  fileNamesMap,
  fontScaling,
  indexData,
  isGettingIndexData,
  locatorsButtons,
  logs,
  logsShowOnlyErrors,
  savedContent,
  showSettings,
  theme,
  userData,
  userSettings,
  nodesMarkers,
  showConcordances
});

export default rootReducer;
