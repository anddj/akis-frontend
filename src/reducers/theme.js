import { THEME_LIGHT, THEME_DARK } from '../lib/constants';
import { SET_THEME, TOGGLE_THEME } from '../actions/actionTypes';

const themeReducer = (state = THEME_LIGHT, action) => {
  switch (action.type) {
    case SET_THEME:
      return action.theme;
    case TOGGLE_THEME:
      return state === THEME_LIGHT ? THEME_DARK : THEME_LIGHT;
    default:
      return state;
  }
};

export default themeReducer;
