import { TOGGLE_SETTINGS } from '../actions/actionTypes';

const showSettings = (state = false, action) => {
  switch (action.type) {
    case TOGGLE_SETTINGS:
      return !state;
    default:
      return state;
  }
};

export { showSettings };
