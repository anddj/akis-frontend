import {
  SET_BOOK_ITEM_IDS_TO_INDEX,
  SET_CURRENT_INDEX_LOCATOR_ID,
  SET_CURRENT_INDEX_NODE_ID,
  SET_INDEX_DATA,
  SET_LOCATORS_BUTTONS,
  SET_NODES_MARKERS,
  TOGGLE_IS_GETTING_INDEX_DATA
} from '../actions/actionTypes';

export const indexData = (state = {}, action) => {
  switch (action.type) {
    case SET_INDEX_DATA:
      if (!action.indexData) return state;
      return Object.assign({}, action.indexData);
    default:
      return state;
  }
};

export const isGettingIndexData = (state = false, action) => {
  switch (action.type) {
    case TOGGLE_IS_GETTING_INDEX_DATA:
      return !state;
    default:
      return state;
  }
};

export const bookItemIdsToIndex = (state = [], action) => {
  switch (action.type) {
    case SET_BOOK_ITEM_IDS_TO_INDEX:
      return [...action.bookItemIds];
    default:
      return state;
  }
};

export const currentIndexNodeId = (state = null, action) => {
  switch (action.type) {
    case SET_CURRENT_INDEX_NODE_ID:
      return action.currentIndexNodeId;
    default:
      return state;
  }
};

export const currentIndexLocatorId = (state = null, action) => {
  switch (action.type) {
    case SET_CURRENT_INDEX_LOCATOR_ID:
      return action.currentIndexLocatorId;
    default:
      return state;
  }
};

export const locatorsButtons = (state = null, action) => {
  switch (action.type) {
    case SET_LOCATORS_BUTTONS:
      if (!action.locatorsButtons) return state;
      return action.locatorsButtons;
    default:
      return state;
  }
};

export const nodesMarkers = (state = null, action) => {
  switch (action.type) {
    case SET_NODES_MARKERS:
      if (!action.nodesMarkers) return state;
      return action.nodesMarkers;
    default:
      return state;
  }
};
