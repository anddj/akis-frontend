import CodeMirrorVanilla from 'codemirror';
import {
  SET_BOOK_ITEMS,
  SET_BOOK_ITEMS_BUFFERS,
  SET_BOOK_ITEM_ACTIVE,
  SET_FILE_NAMES_MAP,
  SET_SAVED_CONTENT,
  UPDATE_BOOK_ITEM,
  UPDATE_BOOK_ITEMS,
  UPDATE_SAVED_CONTENT
} from '../actions/actionTypes';
import { CODEMIRROR_MODE_STEX } from '../lib/constants';

const bookItemsBuffers = (state = {}, action) => {
  switch (action.type) {
    case SET_BOOK_ITEMS_BUFFERS:
      if (!action.bookItems) return state;
      const buffers = {};
      action.bookItems.forEach(bookItem => {
        buffers[bookItem._id] = CodeMirrorVanilla.Doc(
          bookItem.current_content,
          CODEMIRROR_MODE_STEX
        );
      });
      return buffers;
    default:
      return state;
  }
};

const bookItems = (state = [], action) => {
  switch (action.type) {
    case SET_BOOK_ITEMS:
      if (!action.bookItems) return state;
      return action.bookItems.map(bookItem => {
        return bookItem;
      });
    case UPDATE_BOOK_ITEMS:
      if (!action.bookItems) return state;
      return state.map(bookItem => {
        const updatedBookItem = action.bookItems.find(bi => {
          return bi._id === bookItem._id;
        });
        if (updatedBookItem) {
          return Object.assign({}, bookItem, updatedBookItem);
        }
        return bookItem;
      });
    case UPDATE_BOOK_ITEM:
      return state.map(bookItem => {
        if (bookItem._id === action.bookItem._id) {
          return Object.assign({}, bookItem, action.bookItem);
        }
        return Object.assign({}, bookItem);
      });
    default:
      return state;
  }
};

const fileNamesMap = (state = {}, action) => {
  switch (action.type) {
    case SET_FILE_NAMES_MAP:
      const _map = {};
      if (!action.bookItems) return state;
      action.bookItems.forEach(bookItem => {
        _map[`/${bookItem.filepath}`] = bookItem._id;
      });
      return _map;
    default:
      return state;
  }
};

const bookItemActive = (state = null, action) => {
  switch (action.type) {
    case SET_BOOK_ITEM_ACTIVE:
      if (!action.bookItemId) {
        return state;
      }
      return action.bookItemId;
    default:
      return state;
  }
};

const savedContent = (state = {}, action) => {
  switch (action.type) {
    case SET_SAVED_CONTENT:
      let savedContent = {};
      if (!action.bookItems) return state;
      action.bookItems.forEach(bookItem => {
        savedContent = Object.assign(savedContent, {
          [bookItem._id]: bookItem.current_content
        });
      });
      return savedContent;
    case UPDATE_SAVED_CONTENT:
      if (!action.bookItems) return state;
      let updatedContent = {};
      action.bookItems.forEach(bookItem => {
        updatedContent = Object.assign(updatedContent, {
          [bookItem._id]: bookItem.current_content
        });
      });
      return Object.assign({}, state, updatedContent);
    default:
      return state;
  }
};

export {
  bookItems,
  bookItemsBuffers,
  bookItemActive,
  savedContent,
  fileNamesMap
};
