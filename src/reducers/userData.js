import { UPDATE_USER_DATA } from '../actions/actionTypes';

const initialState = {
  loggedIn: false,
  username: ''
};

const userDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_USER_DATA:
      return Object.assign({}, state, action.data);
    default:
      return state;
  }
};

export default userDataReducer;
