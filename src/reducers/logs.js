import { SET_LOGS, SET_LOGS_SHOW_ONLY_ERRORS } from '../actions/actionTypes';

export const logs = (state = {}, action) => {
  switch (action.type) {
    case SET_LOGS:
      if (!action.logs) return state;
      return Object.assign({}, action.logs);
    default:
      return state;
  }
};

export const logsShowOnlyErrors = (state = {}, action) => {
  switch (action.type) {
    case SET_LOGS_SHOW_ONLY_ERRORS:
      return action.logsShowOnlyErrors;
    default:
      return state;
  }
};
