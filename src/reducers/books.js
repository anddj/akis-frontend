import { UPDATE_BOOK_FINISHED } from '../actions/actionTypes';

export const bookFinished = (state = false, action) => {
  switch (action.type) {
    case UPDATE_BOOK_FINISHED:
      if (
        typeof action.bookFinished === 'undefined' ||
        action.bookFinished === null
      )
        return state;
      return action.bookFinished;
    default:
      return state;
  }
};
