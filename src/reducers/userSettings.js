import {
  SET_USER_SETTINGS,
  SET_FONT_SCALING,
  SET_CONCORDANCE_LINES
} from '../actions/actionTypes';

export const userSettings = (state = {}, action) => {
  switch (action.type) {
    case SET_USER_SETTINGS:
      if (!action.userSettings) return state;
      return Object.assign({}, action.userSettings);
    default:
      return state;
  }
};

export const fontScaling = (state = {}, action) => {
  switch (action.type) {
    case SET_FONT_SCALING:
      if (!action.fontScaling) return state;
      return action.fontScaling;
    default:
      return state;
  }
};

export const concordanceLines = (state = {}, action) => {
  switch (action.type) {
    case SET_CONCORDANCE_LINES:
      if (!action.concordanceLines) return state;
      return Object.assign({}, action.concordanceLines);
    default:
      return state;
  }
};
