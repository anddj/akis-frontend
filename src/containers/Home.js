import React from 'react';
import { connect } from 'react-redux';
import { Mosaic } from 'react-mosaic-component';
import { Classes } from '@blueprintjs/core';

import MainContainer from './MainContainer';
import Settings from './Settings';
import FileContent from './FileContent';
import Supplementary from './Supplementary';
import FilesList from './FilesList';
import Preview from './Preview';
import BookIndexDetail from './BookIndexDetail';
import * as constants from '../lib/constants';
import { getBookItems } from '../services/bookItems';
import { getBookMeta } from '../services/books';
import { getUserSettings } from '../services/userSettings';

class Home extends React.Component {
  mosaicComponent;

  LAYOUT_MAP = {
    settings: <Settings />,
    filesList: (
      <FilesList
        splitPercentage={percentage => {
          this.setState({ filesListSplitPercentage: percentage });
        }}
      />
    ),
    fileContent: <FileContent />,
    supplementary: <Supplementary />,
    supplementaryEmpty: <div />,
    preview: <Preview />,
    bookIndexDetail: <BookIndexDetail />
  };

  constructor(props) {
    super(props);
    const { dispatch } = props;
    dispatch(getBookItems());
    dispatch(getUserSettings());
    dispatch(getBookMeta());
    this.state = {
      filesListSplitPercentage: constants.FILES_LIST_SPLIT_PERCENTAGE_MAX
    };
  }

  render() {
    const { theme, showSettings, showConcordances } = this.props;
    const themeClass =
      // Use dark or mosaic default theme
      theme === constants.THEME_DARK ? Classes.DARK : 'mosaic-blueprint-theme';
    return (
      <MainContainer>
        <Mosaic
          ref={ref => {
            this.mosaicComponent = ref;
          }}
          className={themeClass}
          renderTile={id => {
            return this.LAYOUT_MAP[id];
          }}
          initialValue={{
            direction: 'row',
            first: 'settings',
            second: {
              direction: 'row',
              first: 'filesList',
              second: {
                direction: 'row',
                first: {
                  direction: 'column',
                  first: 'fileContent',
                  second: showConcordances
                    ? 'supplementary'
                    : 'supplementaryEmpty',
                  splitPercentage: showConcordances ? 40 : 100
                },
                second: {
                  direction: 'column',
                  first: 'preview',
                  second: 'bookIndexDetail',
                  splitPercentage: 90
                },
                splitPercentage: 60
              },
              splitPercentage: this.state.filesListSplitPercentage
            },
            splitPercentage: showSettings ? 35 : 0
          }}
        />
      </MainContainer>
    );
  }
}

const mapStateToProps = ({ theme, showSettings, showConcordances }) => ({
  theme,
  showSettings,
  showConcordances
});

export default connect(mapStateToProps)(Home);
