import React from 'react';
import { connect } from 'react-redux';
import { Colors, ProgressBar, Card, Overlay } from '@blueprintjs/core';
import Transition from 'react-transition-group/Transition';

import TopNavbar from './TopNavbar';
import FileUploadDialog from './FileUploadDialog';
import * as constants from '../lib/constants';
import store from '../lib/store';

const transitionDuration = 200;
const defaultTransitionStyle = {
  transition: `height ${transitionDuration}ms`,
  height: '0px',
  overflow: 'hidden',
  textAlign: 'right',
  color: Colors.GRAY4
};
const transitionStyles = {
  entering: { height: '0px' },
  entered: { height: '30px' }
};

const TimeElapsed = props => {
  return (
    <Transition in={props.in} timeout={transitionDuration}>
      {state => (
        <div
          style={{
            ...defaultTransitionStyle,
            ...transitionStyles[state]
          }}
        >
          {props.children}
        </div>
      )}
    </Transition>
  );
};

class MainContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFileUploadDialogOpen: false,
      overlayElapsedTime: 0
    };
    this.toggleFileUploadDialog = this.toggleFileUploadDialog.bind(this);
    this.interval = null;
  }

  toggleFileUploadDialog() {
    return this.setState({
      isFileUploadDialogOpen: !this.state.isFileUploadDialogOpen
    });
  }

  render() {
    const { theme } = store.getState();
    const akisOverlayBackDropClass =
      theme === constants.THEME_DARK
        ? 'akisOverlayBackdropDark'
        : 'akisOverlayBackdropLight';
    if (!this.props.userData.loggedIn) {
      return (
        <Card
          className="akisFullHeight"
          style={{ height: this.props.maxContent ? 'max-content' : '100%' }}
        >
          {this.props.children}
        </Card>
      );
    }
    return (
      <div
        className="akisFullHeight"
        style={{
          fontSize:
            (this.props.fontScaling || constants.DEFAULT_FONT_SCALING) + '%'
        }}
      >
        <Overlay
          isOpen={this.props.isGettingIndexData}
          backdropClassName={akisOverlayBackDropClass}
          onOpened={el => {
            const startTime = Date.now();
            this.interval = setInterval(
              function() {
                const elapsedTime = ((Date.now() - startTime) / 1000).toFixed(
                  3
                );
                this.setState({ overlayElapsedTime: elapsedTime });
              }.bind(this),
              100
            );
          }}
          onClosing={el => {
            clearInterval(this.interval);
            this.setState({ overlayElapsedTime: 0 });
          }}
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              height: '100%'
            }}
          >
            <Card
              style={{
                minWidth: '30%',
                minHeight: '100px'
              }}
            >
              <code>Processing...</code>
              <ProgressBar />
              <TimeElapsed in={this.state.overlayElapsedTime > 0}>
                time elapsed:&nbsp;
                {('00000000' + this.state.overlayElapsedTime).substr(-8, 8)}
                &nbsp;secs
              </TimeElapsed>
            </Card>
          </div>
        </Overlay>
        <TopNavbar toggleFileUploadDialog={this.toggleFileUploadDialog} />
        <FileUploadDialog
          isOpen={this.state.isFileUploadDialogOpen}
          toggleDialog={this.toggleFileUploadDialog}
        />
        <Card
          className="akisFullHeight"
          style={{ height: this.props.maxContent ? 'max-content' : '100%' }}
        >
          {this.props.children}
        </Card>
      </div>
    );
  }
}

const mapStateToProps = ({ isGettingIndexData, fontScaling, userData }) => ({
  isGettingIndexData,
  fontScaling,
  userData
});

export default connect(mapStateToProps)(MainContainer);
