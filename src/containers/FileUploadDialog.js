import React from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Callout,
  Checkbox,
  Classes,
  Dialog,
  FileInput,
  Intent,
  ProgressBar,
  Icon
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

import config from '../config/appConfig';
import store from '../lib/store';
import * as constants from '../lib/constants';
import { resetBookItemsData } from '../lib/utils';
import { TopToaster } from '../components/TopToaster';
import {
  setBookItemActive,
  setBookItems,
  setBookItemsBuffers,
  setFileNamesMap,
  setSavedContent
} from '../actions/actionCreators';

const { storageProvider } = config;

const FileUploadReport = props => {
  const { meta } = props;
  if (Object.keys(meta).length === 0) return null; // nothing to render
  const smallClasses = `${Classes.TEXT_DISABLED} ${Classes.TEXT_SMALL}`;
  const items = Object.keys(meta).map((field, idx) => {
    return (
      <li key={idx}>
        {field}&nbsp;
        {meta[field].filenameExtOK === false || meta[field].isText === false ? (
          <Icon icon={IconNames.REMOVE} intent={Intent.DANGER} />
        ) : (
          <span>
            <Icon icon={IconNames.TICK_CIRCLE} intent={Intent.SUCCESS} />
            {meta[field].lineEnding === 'CRLF' ? (
              <span className={smallClasses}>
                &nbsp;<em>win</em>
              </span>
            ) : (
              <span className={smallClasses}>
                &nbsp;<em>lin</em>
              </span>
            )}
          </span>
        )}
        &nbsp;
        {meta[field].filenameExtOK === false && <span>(not a *.tex file)</span>}
        {meta[field].isText === false && <span>(binary)</span>}
      </li>
    );
  });
  return (
    <div className={Classes.RUNNING_TEXT}>
      <ul>{items}</ul>
    </div>
  );
};

class FileUploadDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      fileInputText: 'Select zip file to upload',
      processTexOnly: true,
      processing: false,
      project: '',
      publisher: '',
      reportDialogOpen: false,
      reportDoc: {}
    };
    this.fileInputChange = this.fileInputChange.bind(this);
    this.inputChange = this.inputChange.bind(this);
    this.reset = this.reset.bind(this);
    this.upload = this.upload.bind(this);
  }

  fileInputChange(e) {
    e.preventDefault();
    if (!(e.target.files && e.target.files[0])) {
      return;
    }

    this.setState(
      {
        file: e.target.files[0]
      },
      () => {
        this.setState({
          fileInputText:
            this.state.file.name + ' (' + this.state.file.size + ')'
        });
      }
    );
  }

  inputChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  upload(e) {
    e.preventDefault();
    if (!this.state.file) return;
    this.setState({ processing: true });
    const uri = `${config.api}/books`;
    const xhr = new XMLHttpRequest();
    const fd = new FormData();
    xhr.open('POST', uri, true);
    if (!!storageProvider.token) {
      xhr.setRequestHeader('x-access-token', storageProvider.token);
    }
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 201) {
          let bookItems = [];
          let reportDoc = {};
          try {
            reportDoc = JSON.parse(xhr.responseText);
          } catch (e) {
            console.log(e);
            this.setState({ reportDoc: {} });
          } finally {
            this.setState({ reportDoc }, () => {
              this.setState({ reportDialogOpen: true });
              bookItems = reportDoc.bookItems;
              resetBookItemsData();
              store.dispatch(setBookItems(bookItems));
              store.dispatch(setBookItemsBuffers(bookItems));
              store.dispatch(setSavedContent(bookItems));
              store.dispatch(setFileNamesMap(bookItems));
              if (bookItems && bookItems.length > 0) {
                store.dispatch(setBookItemActive(bookItems[0]._id));
              }
              this.reset(this.props.toggleDialog);
            });
          }
        } else {
          let error;
          try {
            error = JSON.parse(xhr.responseText).error;
            if (
              error ===
              'Error: end of central directory record signature not found'
            ) {
              error =
                error +
                ' (please double check you are actually ' +
                'uploading zip format file).';
            }
          } catch (e) {
            console.log(e);
          } finally {
            TopToaster.show({
              intent: Intent.DANGER,
              message: (error || xhr.responseText) + ` (${xhr.status})`,
              timeout: 15000
            });
            this.setState({ processing: false });
          }
        }
      }
    };
    fd.append('publisher', this.state.publisher);
    fd.append('project', this.state.project);
    fd.append('processTexOnly', this.state.processTexOnly);
    fd.append('zipFile', this.state.file);
    // Initiate a multipart/form-data upload
    xhr.send(fd);
  }

  reset(callback) {
    this.setState(
      {
        publisher: '',
        project: '',
        processTexOnly: true,
        fileInputText: 'Select zip file to upload',
        processing: false,
        file: null
      },
      () => {
        if (callback) callback();
      }
    );
  }

  render() {
    const theme =
      this.props.theme === constants.THEME_DARK ? Classes.DARK : Classes.LIGHT;

    return (
      <div>
        <Dialog
          icon={IconNames.TICK}
          isOpen={this.state.reportDialogOpen}
          onClose={() => {
            this.setState({ reportDialogOpen: false });
          }}
          title="zip file upload report"
          className={theme}
        >
          <Callout>
            Processed zip content:&nbsp;
            {this.state.reportDoc.bookItems &&
              this.state.reportDoc.bookItems.length}{' '}
            files (out of&nbsp;
            {this.state.reportDoc.meta &&
              Object.keys(this.state.reportDoc.meta).length}
            )
          </Callout>
          {this.state.reportDoc.meta && (
            <FileUploadReport meta={this.state.reportDoc.meta} />
          )}
        </Dialog>

        <Dialog
          icon={IconNames.UPLOAD}
          isOpen={this.props.isOpen}
          onClose={this.props.toggleDialog}
          title="zip file upload"
          className={theme}
        >
          <div className={Classes.DIALOG_BODY + ' ' + theme}>
            <Callout
              intent={Intent.PRIMARY}
              icon={null}
              style={{ marginBottom: '5px' }}
            >
              We accept <em>zip</em> compressed <em>utf8</em> encoded text files
              for upload.
            </Callout>
            {config.vtexMode && (
              <div>
                <label className={Classes.LABEL}>
                  Publisher
                  <input
                    className={Classes.INPUT}
                    placeholder="Publisher"
                    onChange={this.inputChange}
                    name="publisher"
                  />
                </label>

                <label className={Classes.LABEL}>
                  Project
                  <input
                    className={Classes.INPUT}
                    placeholder="Project"
                    onChange={this.inputChange}
                    name="project"
                  />
                </label>
              </div>
            )}

            <Checkbox
              checked={this.state.processTexOnly}
              label={
                <span>
                  Extract only <em>*.tex</em> (by extension) files.
                </span>
              }
              name="processTexOnly"
              onChange={e => {
                this.setState({ processTexOnly: e.target.checked });
              }}
            />

            <FileInput
              text={this.state.fileInputText}
              onInputChange={this.fileInputChange}
              fill={true}
              inputProps={{
                accept: '.zip'
              }}
            />
            <div
              style={{
                visibility: this.state.processing ? 'visible' : 'hidden'
              }}
            >
              <ProgressBar
                animate={this.state.processing}
                stripes={this.state.processing}
              />
            </div>
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                text="Cancel"
                onClick={e => {
                  this.reset(this.props.toggleDialog);
                }}
              />
              <Button
                intent={Intent.PRIMARY}
                onClick={this.upload}
                disabled={!this.state.file || this.state.processing}
                text="Upload"
              />
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    theme: state.theme
  };
};

export default connect(mapStateToProps)(FileUploadDialog);
