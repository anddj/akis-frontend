import React from 'react';
import { connect } from 'react-redux';
import sanitize from 'sanitize-filename';
import {
  Button,
  Card,
  Classes,
  Collapse,
  Divider,
  HTMLTable,
  TextArea,
  Text
} from '@blueprintjs/core';

import MainContainer from './MainContainer';
import { getLogs, getZip } from '../services/logs';
import store from '../lib/store';

class LogMeta extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  render() {
    return (
      <div>
        <Button
          onClick={e => {
            this.setState({ isOpen: !this.state.isOpen });
          }}
          text="Data"
        />
        {this.props.metaContent.zipFile && (
          <Button
            text="Download Zip"
            onClick={e =>
              store.dispatch(
                getZip(this.props.logId, data => {
                  if (!data) return;
                  const blobUrl = URL.createObjectURL(data);
                  const link = document.createElement('a');
                  link.href = blobUrl;
                  link.download =
                    sanitize(this.props.metaContent.zipFile) + '.akis.err.zip';
                  document.body.appendChild(link);
                  link.click();
                  setTimeout(function() {
                    document.body.removeChild(link);
                    window.URL.revokeObjectURL(blobUrl);
                  }, 0);
                })
              )
            }
          />
        )}
        <Collapse isOpen={this.state.isOpen}>
          <TextArea
            rows="12"
            readOnly={true}
            value={JSON.stringify(this.props.metaContent, null, 2)}
            fill={true}
            className={`${Classes.MONOSPACE_TEXT} akisTextarea`}
          />
        </Collapse>
      </div>
    );
  }
}

class Logs extends React.Component {
  constructor(props) {
    super(props);
    this.props.dispatch(getLogs(1, null, store.getState().logsShowOnlyErrors)); // get first log data page
  }

  render() {
    if (!this.props.logs.items) return <p>Empty log.</p>;
    const { items, meta } = this.props.logs;
    const logsShift = (meta.pageNr - 1) * meta.itemsPerPage;
    const logs = items.map((log, index) => {
      return (
        <tr key={index}>
          <td style={{ textAlign: 'center' }}>
            <Text ellipsize={true}>{meta.totalItems - logsShift - index}</Text>
          </td>
          <td>
            <Text ellipsize={true}>
              {(log.meta &&
                typeof log.meta.username === 'string' &&
                log.meta.username) ||
                ''}
            </Text>
          </td>
          <td>
            <Text ellipsize={true}>
              {(log.meta &&
                typeof log.meta.filesToIndex === 'string' &&
                log.meta.filesToIndex) ||
                ''}
            </Text>
          </td>
          <td style={{ textAlign: 'center' }} className="akisNoWrap">
            <Text ellipsize={true}>{log.timestamp}</Text>
          </td>
          <td
            style={{
              textAlign: 'center'
            }}
          >
            <Text ellipsize={true}>{log.level}</Text>
          </td>
          <td style={{ wordBreak: 'break-all' }}>
            {log.message}
            {log.meta && (
              <div>
                <Divider />
                <LogMeta metaContent={log.meta} logId={log._id} key={log._id} />
              </div>
            )}
          </td>
        </tr>
      );
    });

    return (
      <MainContainer>
        <div className="akisColumn">
          <Card className="akisCard">
            <HTMLTable
              small={true}
              bordered={true}
              style={{ width: '100%', tableLayout: 'fixed' }}
            >
              <thead>
                <tr>
                  <th style={{ width: '10%', textAlign: 'center' }}>Nr</th>
                  <th style={{ width: '10%', textAlign: 'center' }}>User</th>
                  <th style={{ width: '10%', textAlign: 'center' }}>Files</th>
                  <th style={{ width: '20%', textAlign: 'center' }}>
                    Timestamp
                  </th>
                  <th style={{ width: '10%', textAlign: 'center' }}>Level</th>
                  <th style={{ width: '40%' }}>Message</th>
                </tr>
              </thead>
              <tbody className={Classes.MONOSPACE_TEXT}>{logs}</tbody>
            </HTMLTable>
          </Card>
        </div>
      </MainContainer>
    );
  }
}

const mapStateToProps = ({ logs }) => ({
  logs
});

export default connect(mapStateToProps)(Logs);
