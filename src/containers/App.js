import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import { connect } from 'react-redux';

import Logs from './Logs';
import Login from './Login';
import Register from './Register';
import Reset from './Reset';
import About from './About';
import Policies from './Policies';
import ResetPwd from './ResetPwd';
import VerifyEmail from './VerifyEmail';
import RegisterAlmostDone from './RegisterAlmostDone';
import Home from './Home';
import NoMatch from './NoMatch';
import auth from '../lib/auth';
import { UPDATE_USER_DATA } from '../actions/actionTypes';

// CSS
import '../styles/App.css';
import 'normalize.css/normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import 'react-mosaic-component/react-mosaic-component.css';

const withAuth = (component, nextPathname) => {
  if (auth.loggedIn()) {
    return component;
  }
  const location = {
    pathname: '/login',
    state: { nextPathname }
  };
  return <Redirect to={location} />;
};

class App extends Component {
  constructor(props) {
    super(props);
    const loggedIn = auth.loggedIn();
    const username = loggedIn ? auth.getUsername() : '';
    this.props.dispatch({
      type: UPDATE_USER_DATA,
      data: { loggedIn, username }
    });
  }

  render() {
    return (
      <Router>
        <div className={`bp3-${this.props.theme} akisFullHeight`}>
          <Switch>
            <Route exact path="/" render={() => withAuth(<Home />, '/')} />
            <Route path="/logs" render={() => withAuth(<Logs />, '/logs')} />
            <Route path="/login" component={Login} />
            <Route
              path="/register-almost-done"
              component={RegisterAlmostDone}
            />
            <Route path="/register" component={Register} />
            <Route path="/reset/:hash" component={ResetPwd} />
            <Route path="/reset" component={Reset} />
            <Route path="/verify/:hash" component={VerifyEmail} />
            <Route path="/about" component={About} />
            <Route path="/policies-and-terms-of-service" component={Policies} />
            <Route component={NoMatch} />
          </Switch>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = ({ userData, theme }) => ({
  userData,
  theme
});

export default connect(mapStateToProps)(App);
