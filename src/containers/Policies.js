//
// HTML part is built from separate Markdown sources.
// https://bitbucket.org/vtex/akis-policy-docs/src
//
import React from 'react';

export default props => {
  return (
    <div
      style={{
        padding: '0px 20px',
        overflow: 'auto',
        width: '100%',
        height: '95%'
      }}
    >
      <ul>
        <li>
          <a href="#cookie-policy">Cookie Policy</a>
        </li>
        <li>
          <a href="#privacy-notice">Privacy Notice</a>
        </li>
        <li>
          <a href="#terms-of-service">Terms of Service</a>
        </li>
        <li>
          <a href="#use-policy">Use Policy</a>
        </li>
      </ul>
      <p>
        <span id="cookie-policy" />
      </p>
      <h3>Cookie Policy</h3>
      <p>
        We use "cookies" to collect information and improve Subject-Index. A
        cookie is a small data file that we transfer to your device. We may use
        "persistent cookies" to save your registration ID and login password for
        future logins to Subject-Index. We may use "session ID cookies" to
        enable certain features of Subject-Index, to better understand how you
        interact with it and to monitor aggregate usage and web traffic routing.
        You can instruct your browser, by changing its options, to stop
        accepting cookies or to prompt you before accepting a cookie from the
        websites you visit. If you do not accept cookies, however, you may not
        be able to use all aspects of Subject-Index.
      </p>
      <p>
        We also collect some information (ourselves or using third party
        services, including Google Analytics) using logging and cookies, such as
        IP address, which can sometimes be correlated with personal data. We use
        this information for the above purposes and to monitor and analyse use
        of Subject-Index, for Subject-Index’s technical administration, to
        increase its functionality and user-friendliness, and to verify users
        have the authorisation needed to process their requests.
      </p>
      <p>
        More information on data privacy in respect of Google Analytics is
        available at
        https://www.google.co.uk/intl/en/analytics/privacyoverview.html and
        details about how to opt out can be found at
        https://www.google.com/intl/en/policies/privacy/partners.
      </p>
      <p>
        <span id="privacy-notice" />
      </p>
      <h3>Privacy Notice</h3>
      <p>
        This Notice applies to the websites at subject-index.com and related
        services ("Subject-Index").
      </p>
      <p>
        Subject-Index is operated by Lithuanian-Dutch Joint Venture VTeX (VTeX),
        with registered office at Mokslininkų str. 2A, LT-08412 Vilnius,
        Lithuania ("Subject-Index" or "we"). This Notice explains what personal
        data we collect, how we may use and manage it and the rights you may
        have in relation to such personal data. When we refer to "personal data"
        in this Notice, we mean information relating to an identified or
        identifiable individual that we collect and use in connection with
        Subject-Index; not aggregate or other anonymised data or information we
        process on behalf of our customers.
      </p>
      <h4>How do we collect and use personal data?</h4>
      <p>We collect personal data in the following ways:</p>
      <ul>
        <li>
          <strong>information you provide to us directly online.</strong> For
          example, when you sign-up to use Subject-Index, complete one of our
          web forms or make a support request, we collect the personal data you
          provide, like your name, email address and other basic contact details
          / professional information. We will use this information to enable you
          to access and use Subject-Index or fulfil the request you’ve made. You
          may also provide us with additional information to access and use
          particular features within Subject-Index, such as to populate your
          profile, which you may update within your account settings.
        </li>
        <li>
          <strong>
            information we collect from your use of Subject-Index.
          </strong>{' '}
          When you use Subject-Index, we may collect information about that
          usage and other technical information, such as your IP address,
          browser type and any referring website addresses. We may combine this
          automatically collected log information with other information we
          collect about you and use it to keep a record of our interaction and
          to enable us to support, personalise and improve Subject-Index. This
          may involve the creation of aggregate or other non-personal data. We
          may also collect this type of information using cookies and other
          similar technologies — please see our cookie policy for further
          details.
        </li>
        <li>
          <strong>information you provide to us in person.</strong> For example,
          when you visit one of our exhibition booths or attend one of our
          events, you may provide us with your contact details. We will use this
          information to answer your enquiries or provide other information
          requested by you.
        </li>
        <li>
          <strong>
            information we collect from our other interactions / business
            dealings.
          </strong>{' '}
          For example, if you attend a webinar, contact us via social media or
          otherwise interact with our business, including as a representative of
          a current / prospective customer, supplier or partner, we will make a
          record of those dealings, which may contain personal data.
        </li>
        <li>
          <strong>
            information provided to us by other Subject-Index users.
          </strong>{' '}
          We may receive personal data (for example, your email address) from
          other Subject-Index users, for example if they have tried to refer
          Subject-Index to you.
        </li>
      </ul>
      <p>
        In all the above cases, where we have a relationship with you, we may
        also use the personal data we collect to manage and keep a record of
        that relationship and other business administration purposes you’d
        reasonably expect and, subject always to your preferences, to provide
        information we think may be of interest to you.
      </p>
      <p>
        If you provide personal data to us about someone else (such as one of
        your colleagues), please ensure that you have their permission to do so
        and that they’re aware of the contents of this Notice.
      </p>
      <h4>The legal basis for our use of your personal data</h4>
      <p>
        In order to comply with European data privacy laws, we are required to
        set out the legal bases for our use of your personal data, which are as
        follows:
      </p>
      <ul>
        <li>
          where you have given us your explicit consent, which you can withdraw
          at any time. For example, we rely on your consent to fulfil specific
          requests you’ve made, such as to receive our blog emails, or provide
          information you’ve opted-in to receive;
        </li>
        <li>
          where the processing is necessary for the performance of our contract
          with you, or to enter into such a contract. For example, if you
          sign-up to use Subject-Index, we will need to use your details to
          set-up and administer your account;
        </li>
        <li>
          where the processing is necessary to comply with our legal
          obligations; or
        </li>
        <li>
          the processing is in our legitimate interests, provided these are not
          overridden by your individual rights. For example, we rely on our
          legitimate interests to retain personal data that’s associated with
          content you’ve made public, so that personal information, associated
          with that content can be preserved, e.g. a reference on our site to a
          book with the subject index which was compiled using Subject-Index. We
          also rely on our legitimate interests to contact you when you’ve not
          previously given us your consent to do so.
        </li>
      </ul>
      <h4>Who we share your personal data with</h4>
      <p>
        We may share your personal data within our corporate group on a
        confidential basis for our internal administrative, billing and other
        business purposes. We do not generally disclose or share personal data
        with third parties, except where it’s necessary for legitimate business
        reasons, such as:
      </p>
      <ul>
        <li>
          to the agents, advisers and service providers that assist us in
          running, and that we use to administer, our business;
        </li>
        <li>
          to the subcontractors and service providers we use to provide and
          support Subject-Index, including hosting providers such as AWS and
          support desk service providers like Github;
        </li>
        <li>
          if part of our business is sold to or integrated with another
          business, to our advisers and any prospective purchasers (and their
          advisers);
        </li>
        <li>in such circumstances for which you have given your consent;</li>
        <li>
          if we are required by law or ordered by a court to disclose such
          information;
        </li>
        <li>
          in the case of personal data you share in any Subject-Index community
          services you participate in (for example blogs, forums, and wikis), to
          the public.
        </li>
      </ul>
      <p>
        We include appropriate confidentiality and security obligations in our
        contracts with our service providers and only permit them to process
        your personal data for specified purposes and in accordance with our
        instructions (not for their own purposes).
      </p>
      <h4>Security</h4>
      <p>
        We take appropriate technical and organisational security measures to
        protect personal data from accidental or unlawful destruction,
        accidental loss and unauthorised access, destruction, misuse,
        modification or disclosure.
      </p>
      <h4>Retention of your personal data</h4>
      <p>
        We only keep your personal data for as long as it is necessary for the
        purposes for which it was collected, after which it will be destroyed,
        erased or anonymised. For example, if you are a Subject-Index user, we
        will delete your account profile if you close your account, but may
        however retain certain limited personal data about you as required to
        comply with applicable law.
      </p>
      <h4>International transfers</h4>
      <p>
        In order to run our business and provide Subject-Index, we may transfer
        personal data from the European Economic Area (EEA), including to our
        affiliates and service providers, many of whom are located outside of
        these jurisdictions. Whenever we make such transfers, we will ensure an
        appropriate level of protection is afforded to your personal data by
        implementing at least one of the following safeguards:
      </p>
      <ul>
        <li>
          making sure the destination country has been deemed to provide an
          adequate level of protection for personal data;
        </li>
        <li>
          by using model form contracts that have been officially declared to
          afford your personal data an appropriate protection;
        </li>
        <li>
          in the case of transfers to the United States, checking the recipient
          is part of the "Privacy Shield".
        </li>
      </ul>
      <h4>Marketing</h4>
      <p>
        We do not send you any marketing communications. We may still send you
        emails about the services you use, such as details of new functionality
        / changes to legal terms of use.
      </p>
      <h4>Cookies</h4>
      <p>
        Please see our cookie policy for information on how we collect personal
        data using cookies and similar technologies.
      </p>
      <h4>Your rights</h4>
      <p>
        European data privacy laws give rights to individuals in respect of
        personal data that organisations hold about them, for example:
      </p>
      <ul>
        <li>to request a copy of the personal data that we hold about them;</li>
        <li>to object to the processing of their personal data; or</li>
        <li>
          to request that their personal data is rectified or deleted, or its
          processing limited.
        </li>
      </ul>
      <p>
        To make any requests regarding your personal data, please email us at
        subject-index@vtex.lt. We will comply with any requests to exercise your
        rights in accordance with applicable law. Please be aware, however, that
        there are a number of limitations to these rights, and there may be
        circumstances where we’re not able to comply with your request.
      </p>
      <h4>Third party sites</h4>
      <p>
        If any part of Subject-Index is made available on or through third party
        websites or other resources, includes links to such resources, or other
        resources contain links to any part of Subject-Index, this is done for
        convenience only. We recommend that you check the privacy and security
        policies of such resources as they are not subject to this Notice.
      </p>
      <h4>Contact details</h4>
      <p>
        If you would like any further information, or have any questions or
        concerns, regarding your personal data, as a first step, please email us
        at subject-index@vtex.lt or write to us at UAB VTEX, Mokslininkų str.
        2A, LT-08412 Vilnius, Lithuania.
      </p>
      <p>
        You have the right to make a complaint at any time to your local
        supervisory authority for data protection issues. We would, however,
        appreciate the chance to deal with your concerns in the first instance.
      </p>
      <h4>Changes to this Notice</h4>
      <p>
        We reserve the right to modify or replace this Notice at any time by
        posting the revised Notice on our website. You are responsible for
        reviewing and becoming familiar with any such change each time you
        access any part of Subject-Index.
      </p>
      <p>
        <span id="terms-of-service" />
      </p>
      <h3>Terms of Service</h3>
      <p>
        These terms of service (the "Terms") govern your access to and use of
        the website at subject-index.com (the "Site") and services (the
        "Services") provided by Lithuanian-Dutch Joint Venture VTeX, with
        registered office at Mokslininkų str. 2A, LT-08412 Vilnius, Lithuania
        ("we" or "our" or "VTEX" or "Subject-Index"), so please carefully read
        them before using the Services.
      </p>
      <p>
        By using the Services, you agree to be bound by these Terms. If you are
        using the Services on behalf of an organization, you are agreeing to
        these Terms for that organization and promising that you have the
        authority to bind that organization to these terms. In that case, "you"
        and "your" will refer to that organization.
      </p>
      <p>
        You may use the Services only in compliance with these Terms. You may
        use the Services only if you have the power to form a contract with us
        and are not barred under any applicable laws from doing so, including if
        you are too young to enter into a binding contract. The Services may
        continue to change over time as we refine and add more features. We may
        stop, suspend, or modify the Services at any time without prior notice
        to you (but recognising your work is important to you, please read the
        note below on access to your files in these circumstances). We may also
        remove any content from our Services at our discretion.
      </p>
      <h4>Your Stuff &amp; Your Privacy</h4>
      <p>
        By using our Services, you may provide us with information, files, and
        folders via our products, services and otherwise (together, "your
        stuff"). You retain full ownership to your stuff. We don’t claim any
        ownership to it. These Terms do not grant us any rights to your stuff or
        intellectual property except for the limited rights that are needed to
        run the Services, as explained below.
      </p>
      <p>
        We may need your permission to do things you ask us to do with your
        stuff. This includes product features visible to you. It also includes
        design choices we make to technically administer our Services, for
        example, how we redundantly backup data to keep it safe. You give us the
        permissions we need to do those things solely to provide the Services.
        This permission also extends to our affiliates and trusted third parties
        we work with to provide the Services.
      </p>
      <p>
        To be clear, aside from the rare exceptions we identify in our Privacy
        Notice, no matter how the Services change, we won’t share your content
        with others for any purpose unless you direct us to. How we collect and
        use your information generally is also explained in our Privacy Notice.
      </p>
      <p>
        You are solely responsible for your conduct, the content of your stuff
        while using the Services. For example, it’s your responsibility to
        ensure that you have the rights or permission needed to comply with
        these Terms.
      </p>
      <p>
        You acknowledge that we have no obligation to review or monitor any
        information on the Services. We are not responsible for the accuracy,
        completeness, appropriateness, or legality of your stuff, or any other
        information you provide or may be able to access using the Services.
      </p>
      <p>
        Although we reserve the right to stop, suspend, or modify the Services
        at any time without prior notice to you, we recognise that your stuff is
        important to you, and that you may wish to retrieve your stuff from
        Subject-Index in this event. We will make reasonable effort to contact
        you in such circumstances if you have signed-up for an account, and aim
        to keep your stuff available for retrieval from Subject-Index in
        read-only form for up to 3 months beyond the time of suspension (or
        otherwise) of the Services.
      </p>
      <h4>Registering with us</h4>
      <p>
        When signing-up to use a Service you must choose a username and
        password, unless you use a single sign-on (SSO) option where supported.
        You are responsible for all actions taken under your chosen username and
        password, which you will keep private and not allow anyone else to use.
      </p>
      <p>
        You must ensure that the information you provide is accurate and
        truthful, and to tell us promptly if it changes.
      </p>
      <p>
        You should immediately notify us of any unauthorised use of your
        account. You acknowledge that if you wish to protect your transmission
        of data or files to Subject-Index, it is your responsibility to use a
        secure encrypted connection to communicate with the Services.
      </p>
      <h4>Our Rights</h4>
      <p>
        The Service is owned by or licensed to VTEX and/or its affiliates, and
        is protected by copyrights, trademarks, service marks, patents, trade
        secrets and/or other industrial and proprietary rights and laws,
        including international conventions and treaties ("Proprietary Rights").
        In particular, you acknowledge that the Site and other parts of the
        Service are protected by copyright as collective works and/or
        compilations pursuant to U.S. copyright laws. Nothing in these Terms
        shall operate to transfer any Proprietary Rights in any part of the
        Service.
      </p>
      <p>
        You agree not to remove, suppress or modify in any way the proprietary
        markings, including any trademark or copyright notice, used in relation
        to part of the Service.
      </p>
      <p>
        Unless otherwise agreed in a Contract, the Service is provided for your
        own <strong>non-commercial</strong>, <strong>internal</strong> and{' '}
        <strong>personal use</strong>, at all times subject to these Terms. You
        shall not otherwise use any part of the Service without our prior and
        express written agreement.
      </p>
      <h4>Your Responsibilities</h4>
      <p>
        You must comply with all laws and regulations applicable to you and the
        use of the Services.
      </p>
      <p>
        You, not VTEX, will be fully responsible and liable for what you upload,
        download, receive or otherwise use while using the Services.
      </p>
      <p>
        You are responsible for maintaining and protecting all of your stuff,
        and should always make sure to keep a local copy. We will not be liable
        for any loss or corruption of your stuff, or for any costs or expenses
        associated with backing up or restoring it.
      </p>
      <h4>Your Feedback</h4>
      <p>
        While we appreciate it when users send us or post in our forums
        feedback, suggestions or other comments, please be aware that we may
        use, edit and disclose these without any obligation to you.
      </p>
      <p>
        You are responsible for ensuring that any comments you make do not
        contain any material that could be considered offensive, false,
        defamatory or unlawful or violates any Proprietary Rights or other
        rights.
      </p>
      <h4>Acceptable Use Policy</h4>
      <p>
        You will not, and will not attempt to, misuse the Services, and will use
        the Services only in a manner consistent with the Subject-Index Use
        Policy.
      </p>
      <h4>Links to or from the Service</h4>
      <p>
        The Services may contain links to third-party websites or resources. We
        do not endorse and are not responsible or liable for such resources in
        any way, including their availability, accuracy, nor for any related
        content, products, or services. You accept sole responsibility for your
        use of any such websites or resources. If you want to link to any part
        of the Service, you must ask our permission first, and only do so for
        non-commercial purposes and in a way that is fair and legal and does not
        damage our reputation or take advantage of it.
      </p>
      <h4>Termination</h4>
      <p>
        Though we’d much rather you stay, you can stop using our Services any
        time. If you wish to cancel a Service, you must do this via your Account
        Settings.
      </p>
      <p>
        Unless otherwise agreed, we reserve the right to suspend or end the
        Services at any time, with or without cause, and with or without notice.
        For example, we may suspend or terminate your use if you are not
        complying with these Terms, or use the Services in any way that would
        cause us legal liability or disrupt others’ use of the Services. If we
        suspend or terminate your use without cause, we will try to let you know
        in advance and help you retrieve data, though there may be some cases
        (for example, repeatedly or flagrantly violating these Terms, a court
        order, or danger to other users) where we may suspend immediately.
      </p>
      <p>
        We reserve the right to suspend the provision of any part of the
        Services, or terminate our agreement to provide Services to you for any
        deliberate or material breach, which will include any breach of the
        Subjecti-Index Use Policy, fraudulent or illegal activity or misuse of
        the Service, or if you fail to provide us upon request with sufficient
        information to enable us to determine the accuracy and validity of any
        information supplied by you, or your identity.
      </p>
      <p>This clause does not affect your statutory rights as a consumer.</p>
      <h4>Use post-termination</h4>
      <p>
        We may retain and use your stuff as necessary to comply with our legal
        obligations, resolve disputes, and enforce our agreements. Consistent
        with these requirements, we will try to delete your stuff quickly but
        there might be latency in deletions from our servers, and backed-up
        versions might persist.
      </p>
      <h4>Subject-Index is provided "AS-IS"</h4>
      <p>
        WE CAN’T PROMISE THAT ALL PARTS OF THE SERVICE WILL BE FREE FROM ERRORS
        AND BUGS OR SECURE AT ALL TIMES. AS SUCH, THE SERVICES ARE PROVIDED "AS
        IS" AND "AS AVAILABLE", AT YOUR OWN RISK, WITHOUT ANY EXPRESS, IMPLIED,
        STATUTORY OR OTHER WARRANTY OR CONDITION OF ANY KIND. WITHOUT LIMITING
        THE FOREGOING, WE DISCLAIM ANY WARRANTIES OF MERCHANTABILITY, FITNESS
        FOR A PARTICULAR PURPOSE, FREEDOM FROM ERRORS AND NON-INFRINGEMENT.
        Neither VTEX nor its affiliates will have any responsibility for any
        harm to your computer system, loss or corruption of data, or other harm
        that results from your access to or use of the Services.
      </p>
      <h4>Limitation of Liability</h4>
      <p>
        TO THE FULLEST EXTENT PERMITTED BY LAW, IN NO EVENT WILL VTEX, ITS
        AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS OR LICENSORS BE
        LIABLE FOR (A) ANY INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, EXEMPLARY OR
        CONSEQUENTIAL DAMAGES, REGARDLESS OF LEGAL THEORY, WHETHER OR NOT VTEX
        HAS BEEN WARNED OF THE POSSIBILITY OF SUCH DAMAGES, AND EVEN IF A REMEDY
        FAILS OF ITS ESSENTIAL PURPOSE; (B) ANY LOSS OF PROFITS, REVENUE,
        BUSINESS OR SAVINGS, DEPLETION OF GOODWILL AND/OR SIMILAR LOSSES, OR
        LOSS OR CORRUPTION OF DATA; OR (C) IN RESPECT OF ANY CLAIMS RELATING TO
        THE SERVICES, AN AMOUNT IN AGGREGATE THAT IS MORE THAN THE GREATER OF
        €0.01.
      </p>
      <p>
        US Electronic Communications Privacy Act Notice (18 U.S.C. §§
        2701-2711): COMPANY MAKES NO GUARANTY OF CONFIDENTIALITY OR PRIVACY OF
        ANY COMMUNICATION OR INFORMATION TRANSMITTED ON THE SITE OR ANY WEBSITE
        LINKED TO THE SITE. Company will not be liable for the privacy of email
        addresses, registration and identification information, disk space,
        communications, confidential or trade-secret information, or any other
        Content stored on Company’s equipment, transmitted over networks
        accessed by the Site, or otherwise connected with your use of the
        Service.
      </p>
      <p>
        Notwithstanding any other provision in these Terms, nothing will affect
        or limit your statutory rights; or will exclude or limit our liability
        to the extent not permitted by applicable laws or regulations.
      </p>
      <p>
        You agree to fully indemnify, defend and hold VTEX, and its affiliates,
        officers, employees, agents, suppliers and licensors, harmless on
        demand, from and against all claims, including losses (including loss of
        profit, revenue, goodwill or reputation), costs and expenses, including
        reasonable administrative and legal costs, arising out of any breach of
        these Terms by you.
      </p>
      <h4>Separate Contract and Additional Terms</h4>
      <p>
        If you are accessing and/or otherwise using the Service pursuant to a
        separate agreement between the organization that you belong to or are
        acting for ("Contract"), your use will also be subject to the Contract,
        provided to the extent there is any conflict, the terms of the Contract
        shall prevail. For the avoidance of doubt, nothing in these Terms shall
        impose additional obligations on that organization.
      </p>
      <p>
        Usage restrictions and other additional terms and conditions
        ("Additional Terms") may apply to certain parts of the Service and shall
        form part of these Terms. If we provide you with any software under an
        open source license, these may include the terms of those licenses. You
        shall comply with all Additional Terms referenced on any part of the
        Service you use, posted to the Site or otherwise that you are given
        notice of, and to the extent there is any conflict between the
        Additional Terms and other parts of these Terms, the Additional Terms
        shall prevail in respect of the relevant part of the Service.
      </p>
      <h4>Change to these Terms</h4>
      <p>
        We may revise these Terms from time to time and the most current version
        will always be posted on our website. If a revision, in our sole
        discretion, is material we will notify you (for example via email to the
        email address associated with your account). Other changes may be posted
        to our news or terms page, so please check those pages regularly. By
        continuing to access or use the Services after revisions become
        effective, you agree to be bound by the revised Terms. If you do not
        agree to the new terms, please stop using the Services.
      </p>
      <h4>Miscellaneous Legal Terms</h4>
      <p>
        These Terms, and any dispute, claim or proceedings of whatever nature
        arising out of, or in any way relating to, these Terms will be governed
        by and construed in accordance with English law, without regard to its
        conflict of law principles, except if you are resident (as determined by
        the most current information about yourself provided to VTEX) in the
        United States, in which case the laws of the State of New York, shall
        apply. Both parties submit to the exclusive jurisdiction of the English
        courts, except if you are a resident in the United States in which case
        action may be brought in any federal or state court located in the
        County of New York, State of New York. Notwithstanding the foregoing,
        VTEX shall have the right to bring an action in any court of proper
        jurisdiction for injunctive or other equitable relief.
      </p>
      <p>
        These Terms constitute the entire and exclusive agreement between you
        and VTEX with respect to the Services, and supersede and replace any
        other agreements, terms and conditions applicable to the Services. These
        Terms create no third party beneficiary rights. VTEX's failure to
        enforce a provision is not a waiver of its right to do so later. If a
        provision is found unenforceable the remaining provisions of the
        Agreement will remain in full effect and an enforceable term will be
        substituted reflecting our intent as closely as possible. Any reference
        to "includes" and "including" shall mean including without limitation
        and general words shall not be given a restrictive meaning by reason of
        the fact that they are followed by particular examples intended to be
        embraced by the general words. You may not assign any of your rights in
        these Terms, and any such attempt is void, but VTEX may assign its
        rights to any of its affiliates or subsidiaries, or to any successor in
        interest of any business associated with the Services. VTEX and you are
        not legal partners or agents; instead, our relationship is that of
        independent contractors.
      </p>
      <h4>Force Majeure</h4>
      <p>
        We shall have no liability for delays or failures in delivery or
        performance of our obligations to you resulting from any act, events,
        omissions, failures or accidents that are outside of our control
        including: late, defective performance or non-performance by suppliers
        and private or public telecommunication, computer network failures or
        breakdown of equipment. If a delay is caused, we will be entitled to a
        reasonable extension of time for performing its obligations. If the
        period of delay or non-performance continues for 15 days, either party
        may terminate by written notice.
      </p>
      <h4>Notices</h4>
      <p>
        Any notice required to be given under these Terms must be in writing and
        may be delivered by hand or sent by registered mail or email to the
        other party at the contact address provided (subject in the case of any
        notice to be sent to VTEX in respect of any legal proceedings, to a copy
        also being sent by hand or registered mail and marked for the attention
        of the Legal Department to its registered address) and shall be deemed
        to have been duly given or made, if delivered by hand, upon delivery, if
        sent by registered mail, on the recorded date of receipt, or if sent by
        e-mail when actually received by the intended recipient in readable
        form.
      </p>
      <h4>VTEX Details</h4>
      <p>
        Lithuanian-Dutch Joint Venture VTeX (doing business as Subject-Index) is
        a limited company registered in Lithuania under company number:
        210012070 having its registered office at Mokslininkų str. 2A, LT-08412
        Vilnius, Lithuania.
      </p>
      <p>
        <span id="use-policy" />
      </p>
      <h3>Use Policy</h3>
      <p>
        You agree not to misuse the Subject-Index services. For example, you
        must not, and must not attempt to, do any of the following things using,
        or in respect of, any part of Subject-Index:
      </p>
      <ul>
        <li>
          probe, scan, or test the vulnerability of any system or network;
        </li>
        <li>
          tamper with, breach or otherwise circumvent any security or
          authentication measure;
        </li>
        <li>
          access or use any non-public area / content or shared area you have
          not been invited to;
        </li>
        <li>
          interfere with or disrupt any user, host, or network, for example by
          overloading, flooding, spamming, or mail-bombing;
        </li>
        <li>
          introduce, or otherwise use to facilitate the spread of, spyware or
          any other malware;
        </li>
        <li>
          access or search by any means other than our publicly supported
          interfaces (for example, "scraping");
        </li>
        <li>send unsolicited communications or spam;</li>
        <li>
          send altered, deceptive or false source-identifying information,
          including "spoofing" or "phishing";
        </li>
        <li>
          impersonate or misrepresent your affiliation with any person or
          entity;
        </li>
        <li>
          upload, publish or share anything that is unlawful, fraudulent,
          misleading, infringes another's rights, pornographic, indecent, false,
          defamatory, threatening, obscene, abusive, offensive, hateful,
          inflammatory or is likely to harass, upset, annoy, alarm, embarrass or
          invade the privacy of, any person, deceitful, promotes or advocates an
          unlawful act or activity, discriminatory, sexually explicit or
          violent, comprises educational records or sensitive personal
          information, or which you are not authorised to upload, publish or
          share;
        </li>
        <li>
          infringe any trademark, copyright (including design rights), database
          right, or other intellectual property rights, or privacy or other
          rights, of any other person or breach any contractual or other legal
          duty;
        </li>
        <li>violate or cause the violation of the law in any way;</li>
        <li>
          sell, rent, lease, license, frame, commercialize or use for the
          benefit of any other person;
        </li>
        <li>
          copy, modify, adapt or create derivative works decipher, decompile,
          disassemble, reverse engineer or attempt to derive any source code or
          underlying ideas or algorithms.
        </li>
      </ul>
    </div>
  );
};
