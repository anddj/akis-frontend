import React from 'react';
import { connect } from 'react-redux';
import { Card, Colors, Text } from '@blueprintjs/core';
import { UnControlled as CodeMirror } from 'react-codemirror2';

import * as constants from '../lib/constants';
import { updateBookItem } from '../actions/actionCreators';
import store from '../lib/store';
import { bringIndexToView, scrollIntoView } from '../lib/utils';

// CSS
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/scroll/simplescrollbars.css';
import 'codemirror/addon/dialog/dialog.css';
import 'codemirror/theme/neo.css'; // light editor theme
import 'codemirror/theme/lucario.css'; // dark editor theme
import '../styles/CodeMirror.css';

// node_modules JS
import 'codemirror/mode/stex/stex';
import 'codemirror/addon/selection/active-line';
import 'codemirror/addon/selection/mark-selection';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/addon/scroll/annotatescrollbar';
import 'codemirror/addon/search/search';
import 'codemirror/addon/search/searchcursor';
import 'codemirror/addon/search/jump-to-line';
import 'codemirror/addon/dialog/dialog';

class Supplementary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { editors: [] };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.showConcordances === true;
  }

  runUpdate(prevProps, prevState) {
    const { editorLinkedDocs, bookItems, theme } = this.props;
    const editors = [];
    const { fileNamesMap } = store.getState();
    let filepathBuf = null;
    let filepathEl = null;
    let editorsCounter = 1;

    Object.keys(editorLinkedDocs).forEach((bookItemId, index) => {
      const filepath = Object.keys(fileNamesMap).find(key => {
        return fileNamesMap[key] === bookItemId;
      });
      editorLinkedDocs[bookItemId].forEach((linkedDoc, idx) => {
        if (filepath !== filepathBuf) {
          filepathEl = (
            <Text className="akisCodeMirrorFilePathSuppl">{filepath}</Text>
          );
          filepathBuf = filepath;
        } else {
          filepathEl = null;
        }
        editors.push(
          <div
            key={`${index}-${idx}-${new Date()}`}
            id={`akisConcordanceListItem-${bookItemId}-${idx}`}
          >
            {filepathEl}
            <h3>{editorsCounter}</h3>
            <CodeMirror
              editorDidMount={editor => {
                editor.swapDoc(linkedDoc);
                editor.refresh();
                editor.getWrapperElement().onmousedown = bringIndexToView.bind(
                  editor
                ); //set selection, bring index
                const el = editor.getWrapperElement();
                el.style['border-color'] = Colors.GRAY2;
                editor.on('focus', function() {
                  el.style['border-color'] = Colors.GREEN4;
                });
                editor.on('blur', function() {
                  el.style['border-color'] = Colors.GRAY2;
                });
              }}
              onChange={(editor, data, value) => {
                const newBookItem = Object.assign({}, bookItems[bookItemId], {
                  current_content: value
                });
                updateBookItem(newBookItem);
              }}
              options={{
                lineNumbers: true,
                lineWrapping: true,
                mode: constants.CODEMIRROR_MODE_STEX,
                scrollbarStyle: 'simple',
                styleActiveLine: true,
                styleSelectedText: true,
                viewportMargin: Infinity,
                theme:
                  theme === constants.THEME_DARK
                    ? constants.CODEMIRROR_THEME_DARK
                    : constants.CODEMIRROR_THEME_LIGHT
              }}
            />
          </div>
        ); //push
        editorsCounter++;
      }); // forEach
    }); // forEach

    this.setState({ editors });
  }

  scroll() {
    setTimeout(() => {
      // Bring current concordance item to view
      const {
        editorLinkedDocs,
        currentIndexLocatorId,
        bookItemActive
      } = store.getState();
      if (currentIndexLocatorId !== null) {
        let concordanceIndex = 0;
        let concordanceIndexLocal = 0;
        let found = null;
        Object.keys(editorLinkedDocs).forEach((bookItemId, index) => {
          concordanceIndexLocal = 0;
          editorLinkedDocs[bookItemId].forEach((linkedDoc, idx) => {
            if (concordanceIndex === currentIndexLocatorId) {
              found = concordanceIndexLocal;
            }
            concordanceIndexLocal++;
            concordanceIndex++;
          });
        });

        if (found !== null) {
          scrollIntoView(
            `akisConcordanceListItem-${bookItemActive}-${found}`,
            'akisConcordanceList',
            'smooth'
          );
        }
      }
    }, 0);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.currentIndexNodeId === this.props.currentIndexNodeId) {
      // iterating locators, concordance update not needed
      // only scrolling
      this.scroll();
      return;
    }
    this.runUpdate(prevProps, prevState);
    this.scroll();
  }

  render() {
    const { editors } = this.state;
    return (
      <div id="akisConcordanceList" className="akisColumn akisCodeMirrorList">
        {editors && editors.length ? editors : <Card className="akisCard" />}
      </div>
    );
  }
}

const mapStateToProps = ({
  editorLinkedDocs,
  bookItems,
  theme,
  bookItemsBuffers,
  currentIndexNodeId,
  bookItemActive,
  showConcordances
}) => {
  return {
    editorLinkedDocs,
    bookItems,
    theme,
    bookItemsBuffers,
    currentIndexNodeId,
    bookItemActive,
    showConcordances
  };
};

export default connect(mapStateToProps)(Supplementary);
