import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Card, Elevation, Intent, Icon, H3 } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

import Header from '../components/Header';
import config from '../config/appConfig';

class About extends React.Component {
  render() {
    const serviceType = config.vtexMode ? 'production' : 'public';
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%'
        }}
      >
        <Header />
        <Card
          elevation={Elevation.FOUR}
          style={{
            padding: '30px',
            width: '40%'
          }}
        >
          <H3>About</H3>
          <div>
            <p>
              <em>{config.siteTitleShort}</em> is a {serviceType} version
              of&nbsp;
              <a href="https://vtex.lt">VTeX</a> internal&nbsp;
              <a href="https://en.wikipedia.org/wiki/Subject_indexing">
                subject indexing
              </a>{' '}
              service that contributes towards creating books and articles&nbsp;
              <a href="https://en.wikipedia.org/wiki/Index_(publishing)">
                indexes
              </a>{' '}
              for authors and publishers.
            </p>
            <p>
              This process tags&nbsp;
              <a href="https://www.latex-project.org/">LaTeX</a> files with the{' '}
              <em>\index</em> commands.
            </p>
          </div>
          <div
            style={{
              textAlign: 'right',
              paddingTop: '50px'
            }}
          >
            <em>
              <Link to="/login">Back to sign in page</Link>
            </em>
            <Icon
              icon={IconNames.CHEVRON_RIGHT}
              intent={Intent.PRIMARY}
              style={{ marginLeft: '10px' }}
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default withRouter(About);
