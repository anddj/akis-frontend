import React from 'react';
import { connect } from 'react-redux';
import { UnControlled as CodeMirror } from 'react-codemirror2';
import {
  Button,
  ButtonGroup,
  Classes,
  Colors,
  Intent,
  Text
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { hotkeys, hotkey_display } from 'react-keyboard-shortcuts';

import * as constants from '../lib/constants';
import {
  updateBookItem,
  setEditorInstance,
  setShowConcordances
} from '../actions/actionCreators';
import store from '../lib/store';
import { resetBookItemsData, bringIndexToView } from '../lib/utils';
import { TopToaster } from '../components/TopToaster';
import config from '../config/appConfig';

// CSS
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/scroll/simplescrollbars.css';
import 'codemirror/addon/dialog/dialog.css';
import 'codemirror/theme/neo.css'; // light editor theme
import 'codemirror/theme/lucario.css'; // dark editor theme
import '../styles/CodeMirror.css';

// node_modules JS
import 'codemirror/mode/stex/stex';
import 'codemirror/addon/selection/active-line';
import 'codemirror/addon/selection/mark-selection';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/addon/scroll/annotatescrollbar';
import 'codemirror/addon/search/search';
import 'codemirror/addon/search/searchcursor';
import 'codemirror/addon/search/jump-to-line';
import 'codemirror/addon/dialog/dialog';

class ButtonMakeIndexStringComponent extends React.Component {
  hot_keys = {
    'alt+i': {
      priority: 1,
      handler: e => this.makeIndexString()
    }
  };

  makeIndexString() {
    const { editorInstance } = store.getState();
    const selectedText = editorInstance.getSelection();
    const selectedHead = editorInstance.getCursor();
    if (selectedText.trim() === '') {
      TopToaster.show({
        message: 'No text selected',
        intent: Intent.WARNING
      });
      return;
    }
    const newIndex = `\\index{${selectedText}}`;
    const newStr = selectedText + newIndex;
    editorInstance.replaceSelection(newStr);
    editorInstance.setSelection(selectedHead, {
      line: selectedHead.line,
      ch: selectedHead.ch + newIndex.length
    });
  }

  render() {
    return (
      <Button
        text="{ }"
        title={'Apply index{} to selection ' + hotkey_display('Alt+I')}
        className={Classes.SMALL}
        onClick={e => {
          this.makeIndexString();
        }}
      />
    );
  }
}

const ButtonMakeIndexString = hotkeys(ButtonMakeIndexStringComponent);

class FileContent extends React.Component {
  componentWillUnmount() {
    resetBookItemsData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { bookItemActive, theme } = this.props;
    // Swap DOC
    if (
      prevProps.bookItemActive !== bookItemActive ||
      prevProps.theme !== theme
    ) {
      const {
        editorGoToCoords,
        editorInstance,
        bookItemsBuffers
      } = store.getState();

      if (!bookItemsBuffers[bookItemActive]) return;

      editorInstance.swapDoc(bookItemsBuffers[bookItemActive]);

      if (editorGoToCoords) {
        const el = document.getElementById('akisFileContent');
        const browserHeight = el.clientHeight;
        editorInstance.focus();
        editorInstance.scrollIntoView(
          { line: editorGoToCoords.line },
          browserHeight / 2
        );
        editorInstance.setCursor(editorGoToCoords);
        const markersAtPos = editorInstance.findMarksAt(editorGoToCoords);
        if (markersAtPos.length) {
          const idx = markersAtPos.length - 1; // last marker
          // Select active marker area
          const marker = markersAtPos[idx].find();
          markersAtPos[idx].doc.setSelection(marker.from, marker.to);
        }
      }
    }
  }

  render() {
    const { setEditorInstance, updateBookItem, bookItem, theme } = this.props;
    return (
      <div
        id="akisFileContent"
        className="akisColumn"
        style={{
          display: 'flex',
          height: '100%',
          flexDirection: 'column',
          overflow: 'hidden'
        }}
      >
        <div
          style={{
            display: 'flex',
            justify: 'space-between'
          }}
        >
          <Text className="akisCodeMirrorFilePathMain">
            {bookItem ? '/' + bookItem.filepath : ''}
          </Text>
          <ButtonGroup>
            <ButtonMakeIndexString />
            {config.vtexMode && (
              <Button
                title="Show/Hide concordance window"
                className={Classes.SMALL}
                icon={IconNames.TH_LIST}
                onClick={e => {
                  const { showConcordances, editorInstance } = store.getState();
                  store.dispatch(setShowConcordances(!showConcordances));
                  setTimeout(() => {
                    editorInstance.refresh();
                  }, 0);
                }}
              />
            )}
          </ButtonGroup>
        </div>
        <CodeMirror
          style={{ flex: 1 }}
          editorDidMount={editor => {
            editor.getWrapperElement().onmousedown = bringIndexToView.bind(
              editor
            );
            editor.on('focus', function() {
              const el = document.getElementById('akisFileContent');
              el.style['border-color'] = Colors.BLUE4;
            });
            editor.on('blur', function() {
              const el = document.getElementById('akisFileContent');
              el.style['border-color'] = Colors.GRAY2;
            });
            setEditorInstance(editor);
          }}
          options={{
            lineNumbers: true,
            lineWrapping: true,
            mode: constants.CODEMIRROR_MODE_STEX,
            scrollbarStyle: 'simple',
            styleActiveLine: true,
            styleSelectedText: true,
            theme:
              theme === constants.THEME_DARK
                ? constants.CODEMIRROR_THEME_DARK
                : constants.CODEMIRROR_THEME_LIGHT
          }}
          onChange={(editor, data, value) => {
            const newBookItem = Object.assign({}, bookItem, {
              current_content: value
            });
            updateBookItem(newBookItem);
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = ({
  bookItemActive, // ID
  theme,
  currentIndexNodeId
}) => {
  const { bookItems } = store.getState();
  const bookItem = bookItems.find(item => {
    return item._id === bookItemActive;
  });
  return {
    bookItem,
    bookItemActive,
    theme,
    currentIndexNodeId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateBookItem: bookItem => {
      dispatch(updateBookItem(bookItem));
    },
    setEditorInstance: editor => {
      dispatch(setEditorInstance(editor));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FileContent);
