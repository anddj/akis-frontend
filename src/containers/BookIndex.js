import React from 'react';
import { basename } from 'path';
import { connect } from 'react-redux';
import {
  Button,
  ButtonGroup,
  Classes,
  Intent,
  Position,
  Tooltip,
  Tree,
  Card,
  InputGroup,
  Popover,
  PopoverInteractionKind,
  Tag
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import store from '../lib/store';
import * as constants from '../lib/constants';
import { scrollIntoView } from '../lib/utils';
import {
  setBookItemActive,
  setCurrentIndexNodeId,
  setCurrentIndexLocatorId,
  setEditorGoToCoords,
  setEditorLinkedDocs,
  toggleIsGettingIndexData,
  updateBookItem,
  setLocatorsButtons,
  setNodesMarkers
} from '../actions/actionCreators';
import { TopToaster } from '../components/TopToaster';
import { saveBookItems } from '../services/bookItems';
import { getIndex } from '../services/bookIndex';
import EditIndexItem from './EditIndexItem';
import {
  ButtonIndexUp,
  ButtonIndexDown,
  ButtonJSONDownload,
  ButtonTXTDownload
} from './BookIndexNavigation';

class BookIndex extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nodes: [],
      selectedNodesIds: [],
      isMultiRemoveOpen: false,
      isMultiEditOpen: false,
      lastMarkerText: ''
    };
    this.afterIndexNodeUpdate = this.afterIndexNodeUpdate.bind(this);
    this.onViewConcordance = this.onViewConcordance.bind(this);
    this.runUpdate = this.runUpdate.bind(this);
    this.goToNodeIndex = this.goToNodeIndex.bind(this);
    this.isIndexTreeItemSelected = this.isIndexTreeItemSelected.bind(this);
    this.goToNodeIndexVal = null;
    this.activeIndexTreePosition = [null, null]; // [ nodeIndex, locatorIndex ]
    this.handleNodeClick = this.handleNodeClick.bind(this);
    this.removeNode = this.removeNode.bind(this);
    this.removeNodes = this.removeNodes.bind(this);
    this.updateNodeMarkers = this.updateNodeMarkers.bind(this);
    this.updateOneNodeMarkers = this.updateOneNodeMarkers.bind(this);
  }

  handleNodeClick(nodeData, _nodePath, e) {
    // Show locators
    if (!e.shiftKey) {
      const { currentIndexNodeId } = store.getState();
      if (currentIndexNodeId !== nodeData.id) {
        const el = document.getElementById(
          `akisBookIndexDetailButton-${nodeData.id}-0`
        );
        if (el) {
          this.setState({ selectedNodesIds: [nodeData.id] });
          el.click();
          return; // node will be selected by click()
        }
      } else {
        this.setState({ selectedNodesIds: [nodeData.id] });
      }
    } else {
      // Shift pressed
      let ids = [];
      if (this.state.selectedNodesIds.indexOf(nodeData.id) === -1) {
        ids = this.state.selectedNodesIds.concat(nodeData.id);
      } else {
        const selectedIds = this.state.selectedNodesIds.concat();
        ids = selectedIds.filter(item => {
          return item !== nodeData.id;
        });
      }
      this.setState({ selectedNodesIds: ids });
    }

    const originallySelected = nodeData.isSelected;
    if (!e.shiftKey) {
      this.forEachNode(this.state.nodes, n => (n.isSelected = false));
    }
    nodeData.isSelected =
      originallySelected == null ? true : !originallySelected;
    this.setState({ nodes: this.state.nodes });
  }

  forEachNode(nodes, callback) {
    // Do not place resources concuming functions here
    if (nodes == null) {
      return;
    }
    for (const node of nodes) {
      callback(node);
      this.forEachNode(node.childNodes, callback);
    }
  }

  goToNodeIndex() {
    // Bring Index Tree item to view, mark active index row, show concordance
    if (this.goToNodeIndexVal !== null && this.props.indexData.Right) {
      const _nodeIndex = this.goToNodeIndexVal; // Index Tree row number
      const { showConcordances } = store.getState();
      // Highlight index tree item, show related concordance content
      this.forEachNode(this.state.nodes, node => {
        if (node.id === _nodeIndex) {
          node.isSelected = true;
          if (showConcordances) {
            this.onViewConcordance(node.id, node.locators);
          }
        } else {
          node.isSelected = false;
        }
      });
      this.setState(
        {
          nodes: this.state.nodes,
          selectedNodesIds: [_nodeIndex]
        },
        () => {
          const indexTreeRowElementSelector = `${
            constants.AKIS_INDEX_LABEL
          }-${_nodeIndex}`;
          const previewElementSelector = 'akisIndexTreeContainer';
          scrollIntoView(
            indexTreeRowElementSelector,
            previewElementSelector,
            'smooth',
            `div.${Classes.TREE_NODE_CONTENT}` // go to closest parent element
          );
          this.props.dispatch(setCurrentIndexNodeId(_nodeIndex));
        }
      );
    }
  }

  afterIndexNodeUpdate() {
    // Save and index tree rebuild needed
    const { dispatch } = this.props;
    this.setState({ nodes: [] }, () => {
      const { bookItemIdsToIndex, savedContent, bookItems } = store.getState();
      const changedBookItems = bookItems.filter(bookItem => {
        return bookItem.current_content !== savedContent[bookItem._id];
      });
      dispatch(saveBookItems(changedBookItems)).then(
        bookItems => {
          TopToaster.show({
            intent: Intent.SUCCESS,
            message: `Saved ${bookItems.length} book items.`
          });
          if (!store.getState().isGettingIndexData) {
            dispatch(toggleIsGettingIndexData());
          }
          dispatch(getIndex(bookItemIdsToIndex, this.goToNodeIndex));
        },
        error => {
          console.log(error);
        }
      );
    });
  }

  // Called on locator button click
  goToMainEditorMarker(marker, locator, nodeIndex) {
    const markerPos = marker.find();
    if (!markerPos) return;

    const { fileNamesMap, bookItemActive } = store.getState();
    const el = document.getElementById('akisFileContent');
    const editorHeight = el.clientHeight;
    const newCursorCoords = {
      line: markerPos.from.line,
      ch: markerPos.from.ch
    };
    const bookItemId = fileNamesMap[locator.fileName];

    this.props.dispatch(setCurrentIndexNodeId(nodeIndex));
    if (bookItemActive === bookItemId) {
      const { editorInstance } = store.getState();
      editorInstance.focus();
      // Use Editor ability to scroll line into view
      editorInstance.scrollIntoView(
        { line: newCursorCoords.line },
        editorHeight / 2
      );
      editorInstance.setCursor(newCursorCoords);
      editorInstance.setSelection(markerPos.from, markerPos.to);
      return;
    }
    // otherwise (active bookitem changed)
    this.props.dispatch(setEditorGoToCoords(newCursorCoords));
    this.props.dispatch(setBookItemActive(bookItemId));
  }

  onViewConcordance(nodeIndex, locators) {
    // constructing concordance list
    const linkedDocs = {};
    const {
      fileNamesMap,
      bookItemsBuffers,
      concordanceLines
    } = store.getState();
    // Unlink docs
    for (let i = 0; i < 4; i++) {
      for (const key in bookItemsBuffers) {
        bookItemsBuffers[key].iterLinkedDocs(function(doc) {
          if (doc) {
            bookItemsBuffers[key].unlinkDoc(doc);
          }
        });
      }
    }
    locators.forEach((locator, locatorIndex) => {
      const bookItemId = fileNamesMap[locator.fileName];
      const start =
        locator.startLn > concordanceLines.top
          ? locator.startLn - (concordanceLines.top + 1)
          : 0;
      const editorLastLineNumber = bookItemsBuffers[bookItemId].lineCount() - 1;
      const end =
        locator.endLn < editorLastLineNumber - concordanceLines.bottom
          ? locator.endLn + concordanceLines.bottom
          : editorLastLineNumber;
      const linkedDoc = bookItemsBuffers[bookItemId].linkedDoc({
        from: start,
        to: end,
        sharedHist: true
      });
      const markerPos = {};
      markerPos.from = {
        line: locator.startLn - 1,
        ch: locator.startCol - 1
      };
      markerPos.to = {
        line: locator.endLn - 1,
        ch: locator.endCol - 1
      };
      const marker = linkedDoc.markText(markerPos.from, markerPos.to, {
        className: constants.MARKER_CLASS_ALT
      });
      // concordance area marker bringToView method, will be called on click
      marker.bringIndexToView = (() => {
        const _nodeIndex = nodeIndex;
        const _locatorIndex = locatorIndex;
        const _bookItemId = bookItemId;

        return () => {
          const { dispatch } = this.props;
          const newPos = [_nodeIndex, _locatorIndex];
          this.goToNodeIndexVal = _nodeIndex;
          this.activeIndexTreePosition = newPos;
          dispatch(setCurrentIndexNodeId(_nodeIndex));
          dispatch(setCurrentIndexLocatorId(_locatorIndex));
          setTimeout(() => {
            // scrolling main editor
            const markerPos = marker.find();
            if (!markerPos) return;
            const { bookItemActive, editorInstance } = store.getState();
            if (bookItemActive === _bookItemId) {
              const el = document.getElementById('akisFileContent');
              const editorHeight = el.clientHeight;
              editorInstance.scrollIntoView(
                { line: markerPos.from.line },
                editorHeight / 2
              );
              editorInstance.setSelection(markerPos.from, markerPos.to);
              editorInstance.refresh();

              const indexTreeRowElementSelector = `${
                constants.AKIS_INDEX_LABEL
              }-${_nodeIndex}`;
              const previewElementSelector = 'akisIndexTreeContainer';
              scrollIntoView(
                indexTreeRowElementSelector,
                previewElementSelector,
                'smooth',
                `li.${Classes.TREE_NODE}` // go to closest parent DOM element
              );
              this.props.dispatch(setCurrentIndexNodeId(_nodeIndex));
            } else {
              // will scroll after changing active bookitem
              dispatch(setEditorGoToCoords(markerPos.from));
              dispatch(setBookItemActive(_bookItemId));
            }
          }, 0);
        };
      })();

      if (!linkedDocs[bookItemId]) {
        linkedDocs[bookItemId] = [];
      }
      linkedDocs[bookItemId].push(linkedDoc);
    }); // forEach
    store.dispatch(setEditorLinkedDocs(linkedDocs));
  }

  isIndexTreeItemSelected(nodeIndex, locatorIndex) {
    const [activeNodeIndex, activeLocatorIndex] = this.activeIndexTreePosition;
    return nodeIndex === activeNodeIndex && locatorIndex === activeLocatorIndex;
  }

  runUpdate(prevProps, prevState, snapshot) {
    const props = this.props;
    const {
      fileNamesMap,
      bookItemsBuffers,
      currentIndexNodeId
    } = store.getState();
    let nodeIndex = 0; // hierarchical node index (recursive)
    const allLocatorsButtons = {}; // nodeIndex -> [buttons]
    const allNodesMarkers = {}; // nodeIndex -> [nodeMarkers]

    const makeNode = nodeData => {
      nodeIndex++;

      const hasSubTree = nodeData.indexTree && nodeData.indexTree.length > 0;
      const buttonStyleRotationClasses = [constants.BUTTON_CLASS_ALT, ''];
      const buttonActiveClass = constants.BUTTON_CLASS_ACTIVE;
      let buttonClassIndex = 0;
      const nodeMarkers = [];

      // Construct locators buttons for this index node,
      // create \index{...} markers
      const locatorsButtons = nodeData.locators.map(
        (locator, locatorIndex, locators) => {
          const bookItemId = fileNamesMap[locator.fileName];
          const buffer = bookItemsBuffers[bookItemId];
          const marker = buffer.markText(
            { line: locator.startLn - 1, ch: locator.startCol - 1 },
            { line: locator.endLn - 1, ch: locator.endCol - 1 },
            { className: constants.MARKER_CLASS }
          );
          marker.bookItemId = bookItemId;
          nodeMarkers.push(marker);
          // Was filename switched to another
          const isNextFile =
            locatorIndex > 0 &&
            locator.fileName !== nodeData.locators[locatorIndex - 1].fileName;
          const isSelected = false;
          if (isNextFile) {
            buttonClassIndex = buttonClassIndex === 0 ? 1 : 0;
          }
          const buttonClass = isSelected
            ? buttonActiveClass
            : buttonStyleRotationClasses[buttonClassIndex];

          // main editor marker
          marker.bringIndexToView = (() => {
            const _nodeIndex = nodeIndex;
            const _locator = locator;
            const _locatorIndex = locatorIndex;
            const { dispatch } = props;
            return () => {
              this.goToNodeIndexVal = _nodeIndex;
              const newPos = [_nodeIndex, _locatorIndex];
              this.activeIndexTreePosition = newPos;
              this.goToNodeIndex();
              dispatch(setCurrentIndexNodeId(_nodeIndex));
              dispatch(setCurrentIndexLocatorId(_locatorIndex));
              dispatch(
                setEditorGoToCoords({
                  line: _locator.startLn - 1,
                  ch: _locator.startCol - 1
                })
              );
            };
          })();

          // Locator Button
          return (
            <Tooltip
              content={`${basename(locator.fileName)} : ${locator.startLn}`}
              key={locatorIndex}
              position={Position.AUTO}
            >
              <Button
                id={`akisBookIndexDetailButton-${nodeIndex}-${locatorIndex}`}
                className={`${Classes.SMALL} ${buttonClass}`}
                onClick={(() => {
                  const _nodeIndex = nodeIndex;
                  const _locatorIndex = locatorIndex;
                  return e => {
                    e.stopPropagation();
                    this.goToNodeIndexVal = _nodeIndex;
                    const newPos = [_nodeIndex, _locatorIndex];
                    this.activeIndexTreePosition = newPos;
                    this.goToNodeIndex();
                    props.dispatch(setCurrentIndexLocatorId(_locatorIndex));
                    const mrk = store.getState().nodesMarkers[_nodeIndex][
                      _locatorIndex
                    ];
                    if (mrk) {
                      this.goToMainEditorMarker(mrk, locator, _nodeIndex);
                    }
                  };
                })()}
              >
                {locatorIndex + 1}
              </Button>
            </Tooltip>
          );
        }
      ); // locatorsButtons

      allLocatorsButtons[nodeIndex] = locatorsButtons;
      allNodesMarkers[nodeIndex] = nodeMarkers;

      const _updateNodeMarkers = (() => {
        const _nodeIndex = nodeIndex;
        return (newContent, nodeIds) => {
          this.updateOneNodeMarkers(newContent, nodeMarkers, _nodeIndex);
        };
      })();

      // Index Node (edit button, locators counter)
      return {
        id: nodeIndex,
        isSelected: currentIndexNodeId === nodeIndex,
        isExpanded: hasSubTree,
        icon: IconNames.FOLDER_CLOSE,
        label: (
          <div id={`${constants.AKIS_INDEX_LABEL}-${nodeIndex}`}>
            {nodeData.term}
            &nbsp;
            {nodeData.locators && nodeData.locators.length > 0 && (
              <span>
                <ButtonGroup>
                  <EditIndexItem
                    inputVal={nodeData.locators[0].originTex}
                    nodeIds={null}
                    minimal={false}
                    updateNodeMarkers={_updateNodeMarkers}
                    removeNode={this.removeNode}
                    locatorsCount={nodeData.locators.length}
                    nodeIndex={nodeIndex}
                  />
                  <span style={{ display: 'none' }}>{locatorsButtons[0]}</span>
                </ButtonGroup>
                &nbsp;
                <Tag round={true} minimal={true}>
                  {nodeData.locators.length}
                </Tag>
              </span>
            )}
          </div>
        ),
        childNodes: hasSubTree
          ? nodeData.indexTree.map(item => makeNode(item))
          : null,
        locators: nodeData.locators
      };
    }; // makeNode

    const data = props.indexData.Right || [];
    const nodes = data.map(item => makeNode(item));
    this.setState({ nodes }, () => {
      this.goToNodeIndex();
      store.dispatch(setLocatorsButtons(allLocatorsButtons));
      store.dispatch(setNodesMarkers(allNodesMarkers));
      setTimeout(() => {
        store.getState().editorInstance.focus();
      }, 0);
    });
  } // runUpdate

  updateNodeMarkers(newContent, nodeIds) {
    // if nodeIds supplied: iterate over index tree nodes
    if (!(nodeIds && nodeIds.length)) return;
    const { nodesMarkers } = store.getState();
    nodeIds.forEach(nodeId => {
      const _nodeMarkers = nodesMarkers[nodeId];
      if (!_nodeMarkers) return;
      this.updateOneNodeMarkers(newContent, _nodeMarkers, nodeId);
    });
  }

  updateOneNodeMarkers(newContent, nodeMarkers, nodeId) {
    if (!nodeMarkers || nodeMarkers.length === 0) return;
    nodeMarkers.forEach((nodeMarker, locatorIndex) => {
      const markerLocation = nodeMarker.find();
      if (markerLocation) {
        const { from, to } = markerLocation;
        const _bookItemId = nodeMarker.bookItemId;
        const _bringIndexToView = nodeMarker.bringIndexToView;
        nodeMarker.doc.replaceRange(newContent, from, to);
        const newMarker = nodeMarker.doc.markText(
          from,
          { line: from.line, ch: from.ch + newContent.length },
          { className: constants.MARKER_CLASS_ALT2 }
        );
        // Restoring replaced marker values
        newMarker.bookItemId = _bookItemId;
        newMarker.bringIndexToView = _bringIndexToView;
        // Update nodesMarkers state (in store)
        // for given nodeId and locatorIndex values
        const { nodesMarkers } = store.getState();
        nodesMarkers[nodeId][locatorIndex] = newMarker;
        store.dispatch(setNodesMarkers(nodesMarkers));
        const { bookItems } = store.getState();
        const bookItemActive = nodeMarker.bookItemId;
        const bookItem = bookItems.find(item => {
          return item._id === bookItemActive;
        });
        const newBookItem = Object.assign({}, bookItem, {
          current_content: nodeMarker.doc.getValue()
        });
        store.dispatch(updateBookItem(newBookItem));
      }
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    let indexDataChanged = false;

    if (
      store.getState().bookItems.length === 0 &&
      Object.keys(this.props.indexData).length === 0 &&
      Object.keys(prevProps.indexData).length > 0
    ) {
      // Book index changes to empty, need update
      // (on new zip file open)
      indexDataChanged = true;
    } else if (
      Object.keys(prevProps.indexData).length !== 0 &&
      Object.keys(this.props.indexData).length === 0 &&
      prevState.nodes.length !== 0 &&
      this.state.nodes.length !== 0 &&
      prevState.nodes.length === this.state.nodes.length
    ) {
      // after re-build
      indexDataChanged = true;
    } else if (Object.keys(this.props.indexData).length === 0) {
      // on reset bookitems data
      indexDataChanged = false;
    } else if (prevProps.indexData !== this.props.indexData) {
      // if index data changed
      indexDataChanged = true;
    }

    if (!indexDataChanged) {
      return; // nothing changed to update GUI
    }

    setTimeout(() => {
      this.setState({ nodes: [] }, () => {
        this.runUpdate(prevProps, prevState, snapshot);
      });
    }, 0);
  } // componentDidUpdate

  removeOneMarker(nodeMarker, nodeId, locatorIndex) {
    const markerLocation = nodeMarker.find();
    if (markerLocation) {
      const { from, to } = markerLocation;
      const _bookItemId = nodeMarker.bookItemId;
      const _bringIndexToView = nodeMarker.bringIndexToView;
      nodeMarker.doc.replaceRange('', from, to);
      // Creating new empty marker
      const newMarker = nodeMarker.doc.markText(
        from,
        { line: from.line, ch: from.ch },
        {
          className: constants.MARKER_CLASS_ALT2,
          clearWhenEmpty: false
        }
      );
      nodeMarker.clear();
      // Restoring replaced marker values
      newMarker.bookItemId = _bookItemId;
      newMarker.bringIndexToView = _bringIndexToView;
      // Update nodesMarkers state (in store)
      // for given nodeId and locatorIndex values
      const { nodesMarkers } = store.getState();
      nodesMarkers[nodeId][locatorIndex] = newMarker;
      store.dispatch(setNodesMarkers(nodesMarkers));
      const { bookItems } = store.getState();
      const bookItemActive = _bookItemId;
      const bookItem = bookItems.find(item => {
        return item._id === bookItemActive;
      });
      const newBookItem = Object.assign({}, bookItem, {
        current_content: newMarker.doc.getValue()
      });
      store.dispatch(updateBookItem(newBookItem));
    }
  }

  removeNodes() {
    const { nodesMarkers } = store.getState();
    this.state.selectedNodesIds.forEach(nodeId => {
      if (!nodesMarkers[nodeId] || nodesMarkers[nodeId].length === 0) return;
      nodesMarkers[nodeId].forEach((nodeMarker, locatorIndex) => {
        this.removeOneMarker(nodeMarker, nodeId, locatorIndex);
      });
    });
  }

  // remove markers
  // removeFrom, removeTo: 0-based indexes
  removeNode(nodeId, removeFrom, removeTo) {
    const { nodesMarkers } = store.getState();
    if (!nodesMarkers[nodeId] || nodesMarkers[nodeId].length === 0) return;
    if (!removeFrom) removeFrom = 0;
    if (!removeTo) removeTo = nodesMarkers[nodeId].length;
    for (let i = removeFrom; i < removeTo; i++) {
      this.removeOneMarker(nodesMarkers[nodeId][i], nodeId, i);
    }
  }

  modifyIndexItemDOM(mode) {
    const ids = this.state.selectedNodesIds.concat();
    if (!(ids && ids.length)) return;
    if (!mode) mode = 'edited';
    this.setState({ selectedNodesIds: [] }, () => {
      EditIndexItem.modifyDOM(ids, null, mode);
    });
  }

  render() {
    const nodesExist = this.state.nodes && this.state.nodes.length > 0;
    if (nodesExist) {
      const { currentIndexNodeId, locatorsButtons, indexData } = this.props;
      const { theme } = store.getState();
      const backdropProps =
        theme === constants.THEME_DARK
          ? { className: 'akisOverlayBackdropDark' }
          : { className: 'akisOverlayBackdropLight' };
      return (
        <div
          className="akisFullArea"
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between'
          }}
        >
          <Card
            style={{
              padding: '0 0 10px 0',
              margin: 0,
              borderRadius: 0
            }}
          >
            <ButtonGroup fill={true}>
              <ButtonIndexDown {...{ currentIndexNodeId, locatorsButtons }} />
              <ButtonIndexUp {...{ currentIndexNodeId, locatorsButtons }} />
              <ButtonJSONDownload indexData={indexData} />
              <ButtonTXTDownload indexData={indexData} />
              {/* Target */}
              <InputGroup
                value={this.state.selectedNodesIds.join(',')}
                readOnly={true}
                rightElement={
                  <ButtonGroup>
                    <EditIndexItem
                      inputVal={null}
                      minimal={true}
                      updateNodeMarkers={this.updateNodeMarkers}
                      nodeIds={this.state.selectedNodesIds}
                      nodeIndex={currentIndexNodeId}
                      disabled={this.state.selectedNodesIds.length === 0}
                    />
                    <Popover
                      backdropProps={backdropProps}
                      canEscapeKeyClose={true}
                      hasBackdrop={true}
                      interactionKind={PopoverInteractionKind.CLICK}
                      isOpen={this.state.isMultiRemoveOpen}
                      popoverClassName={Classes.POPOVER_CONTENT_SIZING}
                    >
                      {/* Target */}
                      <Button
                        title="Remove"
                        minimal={true}
                        round={false}
                        className={Classes.SMALL}
                        icon={IconNames.REMOVE}
                        disabled={this.state.selectedNodesIds.length === 0}
                        onClick={e => {
                          this.setState({
                            isMultiRemoveOpen: true
                          });
                        }}
                      />
                      {/* Content */}
                      <div>
                        <ButtonGroup>
                          <Button
                            title="Remove from index tree"
                            icon={IconNames.REMOVE}
                            onClick={e => {
                              this.removeNodes();
                              this.setState(
                                { isMultiRemoveOpen: false },
                                () => {
                                  this.modifyIndexItemDOM('removed');
                                }
                              );
                            }}
                          >
                            Remove
                          </Button>
                          <Button
                            title="Cancel"
                            icon={IconNames.CROSS}
                            onClick={e => {
                              e.stopPropagation();
                              this.setState({ isMultiRemoveOpen: false });
                            }}
                          >
                            Cancel
                          </Button>
                        </ButtonGroup>
                      </div>
                    </Popover>
                  </ButtonGroup>
                }
              />
            </ButtonGroup>
          </Card>
          <div
            id="akisIndexTreeContainer"
            className="akisFullArea akisScrollable"
          >
            <Tree
              contents={this.state.nodes}
              className="akisFullArea"
              onNodeClick={this.handleNodeClick}
            />
          </div>
        </div>
      );
    }
    return <div className="akisFullArea" />;
  }
}

const mapStateToProps = ({
  indexData,
  currentIndexNodeId,
  locatorsButtons
}) => {
  return {
    indexData,
    currentIndexNodeId,
    locatorsButtons
  };
};

export default connect(mapStateToProps)(BookIndex);
