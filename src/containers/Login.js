import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import {
  Button,
  Card,
  Elevation,
  Icon,
  Intent,
  Label,
  InputGroup
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { connect } from 'react-redux';

import auth from '../lib/auth';
import { TopToaster } from '../components/TopToaster';
import MainContainer from './MainContainer';
import { updateUserData } from '../actions/actionCreators';
import Header from '../components/Header';
import config from '../config/appConfig';

const { storageProvider } = config;

class Login extends React.Component {
  constructor(props) {
    super(props);
    if (typeof storageProvider !== 'undefined') storageProvider.clear();
    this.state = {
      username: '',
      password: '',
      error: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target,
      value = target.value,
      name = target.name;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const username = this.state.username;
    const password = this.state.password;
    const { location, history, dispatch } = this.props;
    if (!(username && password)) {
      dispatch(updateUserData({ loggedIn: false, username: '' }));
      TopToaster.show({
        intent: Intent.DANGER,
        message: 'Please provide valid username and password.'
      });
      return this.setState({ error: true });
    }
    auth.login(username, password, loggedIn => {
      if (!loggedIn) {
        this.setState({ error: true });
        dispatch(updateUserData({ loggedIn: false, username: '' }));
        TopToaster.show({
          intent: Intent.DANGER,
          message: 'Sign in failed.'
        });
        return;
      }
      if (location.state && location.state.nextPathname) {
        history.push(location.state.nextPathname);
      } else {
        history.push('/');
      }
      dispatch(updateUserData({ username, loggedIn: true }));
    });
  }

  render() {
    return (
      <MainContainer>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%'
          }}
        >
          <Header />
          <Card
            elevation={Elevation.FOUR}
            style={{
              padding: '70px',
              width: '30%'
            }}
          >
            <form method="POST" onSubmit={this.handleSubmit}>
              <Label>
                <InputGroup
                  large={true}
                  leftIcon={IconNames.USER}
                  name="username"
                  onChange={this.handleInputChange}
                  placeholder="Username or email"
                  type="text"
                />
              </Label>
              <Label>
                <InputGroup
                  large={true}
                  leftIcon={IconNames.KEY}
                  name="password"
                  onChange={this.handleInputChange}
                  placeholder="Password"
                  type="password"
                />
              </Label>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'space-between'
                }}
              >
                <Button
                  fill={true}
                  intent={Intent.PRIMARY}
                  large={true}
                  text="Sign in"
                  type="submit"
                />
                <div
                  style={{
                    textAlign: 'right',
                    paddingTop: '50px'
                  }}
                >
                  <em>
                    <Link to="/reset" title="Password reset">
                      I forgot my password
                    </Link>
                  </em>
                  <Icon
                    icon={IconNames.CHEVRON_RIGHT}
                    intent={Intent.PRIMARY}
                    style={{ marginLeft: '10px' }}
                  />
                  <br />
                  <em>
                    <Link to="/register" title="Sign up">
                      Sign up
                    </Link>
                  </em>
                  <Icon
                    icon={IconNames.CHEVRON_RIGHT}
                    intent={Intent.PRIMARY}
                    style={{ marginLeft: '10px' }}
                  />
                  <br />
                  <br />
                  <em>
                    <Link to="/about" title="About">
                      About
                    </Link>
                  </em>
                  <Icon
                    icon={IconNames.CHEVRON_RIGHT}
                    intent={Intent.PRIMARY}
                    style={{ marginLeft: '10px' }}
                  />
                </div>
              </div>
            </form>
            <p
              style={{
                fontSize: '0.8em',
                marginTop: '50px',
                borderTop: '1px solid lightgray',
                padding: '5px'
              }}
            >
              * This web application requires modern version of&nbsp;
              <a href="https://www.mozilla.org/en-US/firefox/new/">
                Firefox
              </a>{' '}
              or&nbsp;
              <a href="https://www.google.com/chrome/">Chrome</a> browser.
            </p>
          </Card>
        </div>
      </MainContainer>
    );
  }
}

export default connect()(withRouter(Login));
