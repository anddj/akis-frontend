import React from 'react';
import {
  Alignment,
  Button,
  ButtonGroup,
  Classes,
  Colors,
  Divider,
  InputGroup,
  Popover,
  PopoverInteractionKind,
  Position,
  RangeSlider,
  Switch
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import Transition from 'react-transition-group/Transition';
import Differ from 'react-differ';
import * as constants from '../lib/constants';
import {
  setCurrentIndexNodeId,
  setCurrentIndexLocatorId
} from '../actions/actionCreators';

import store from '../lib/store';

const transitionDuration = 100;
const defaultTransitionStyle = {
  transition: `height ${transitionDuration}ms`,
  height: '0px'
};
const transitionStylesDiffer = {
  entering: { height: '0px' },
  entered: { height: 'auto' }
};
const transitionStylesSaveButton = {
  entering: { height: '0px' },
  entered: { height: '40px' }
};

export default class EditIndexItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputVal: props.inputVal,
      oldVal: props.inputVal,
      isOpen: false,
      isRemoveOpen: false,
      showConfirmSave: false,
      removeLocatorsFrom: 1, // locators range to remove
      removeLocatorsTo: this.props.locatorsCount,
      locatorsRangeChecked: false
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.onInteraction = this.onInteraction.bind(this);
    this.onOpened = this.onOpened.bind(this);
    this.onSave = this.onSave.bind(this);
    this.getInputVal = this.getInputVal.bind(this);
    this.fontScaling = store.getState().fontScaling;
    this.popoverRef = null;
    this.editButtonRef = null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.nodeIds &&
      prevProps.nodeIds !== this.props.nodeIds &&
      this.props.nodeIds.length !== 0
    ) {
      const inputVal = this.getInputVal();
      this.setState({ inputVal, oldVal: inputVal });
    }
  }

  static getMarkerText(marker) {
    if (!marker) return;
    const markerLocation = marker.find();
    if (!markerLocation) return;

    return marker.doc.getRange(markerLocation.from, markerLocation.to);
  }

  onInputChange(e) {
    this.setState({ inputVal: e.target.value });
  }

  static getLastIndexStr(indexStr) {
    // Get last index string (trimmed):
    // '\\index{ level 1 ! level 2 ! level 3 }' -> 'level 3'
    // '\\index{ level 1 }' -> 'level 1'
    if (!indexStr) return '';
    const indices = indexStr.split('!');
    let found = '';
    if (indices.length > 1) {
      const lastIndex = indices[indices.length - 1].trim();
      if (lastIndex.endsWith('}')) {
        found = lastIndex.substring(0, lastIndex.length - 1).trim();
      }
    }
    if (indices.length === 1) {
      const lastIndex = indices[0].trim();
      const startStr = '\\index{';
      if (lastIndex.startsWith(startStr) && lastIndex.endsWith('}')) {
        found = lastIndex
          .substring(startStr.length, lastIndex.length - 1)
          .trim();
      }
    }

    return found ? found : indexStr;
  }

  static modifyDOM(nodeIds, inputVal, mode) {
    if (!nodeIds) return;
    if (!mode) mode = 'edited';
    nodeIds.forEach(id => {
      const el = document.getElementById(`${constants.AKIS_INDEX_LABEL}-${id}`);
      if (el) {
        const treeNode = el.closest(`li.${Classes.TREE_NODE}`);
        if (treeNode) {
          const backgroundColor =
            mode === 'edited' ? Colors.GREEN5 : Colors.RED5;
          let overlayEl = document.getElementById(
            `${constants.AKIS_INDEX_ITEM_OVERLAY}-${id}`
          );
          if (!overlayEl) {
            overlayEl = document.createElement('div');
            overlayEl.id = `${constants.AKIS_INDEX_ITEM_OVERLAY}-${id}`;
            Object.assign(overlayEl.style, {
              position: 'absolute',
              width: '5px',
              height: '30px',
              background: backgroundColor,
              opacity: 0.75,
              'z-index': 7
            });
            treeNode.prepend(overlayEl);
          } else {
            overlayEl.style.background = backgroundColor;
          }
        }
        // Change tree node text directly (avoiding full tree rerender)
        let indexVal = '';
        if (inputVal !== null) {
          indexVal = EditIndexItem.getLastIndexStr(inputVal);
        } else {
          // Getting copy of original text value
          // (index items were multi-removed)
          indexVal = el.firstChild.textContent;
        }

        const elem = document.getElementById(
          `${constants.AKIS_INDEX_LABEL}-${id}`
        );
        if (elem) {
          if (elem.firstChild) {
            elem.firstChild.nodeValue = indexVal + ' ';
          } else {
            elem.appendChild(document.createTextNode(indexVal + ' '));
          }

          elem.style['font-style'] = 'italic';

          if (mode === 'edited') {
            elem.style['color'] = Colors.GREEN5;
          }

          if (mode === 'removed') {
            elem.style['color'] = Colors.RED5;
          }
        }
      }
    });
  }

  modifyIndexItemDOM(mode) {
    if (!mode) mode = 'edited';
    const { nodeIds, nodeIndex } = this.props;
    const ids = nodeIds ? nodeIds.concat() : [nodeIndex];
    EditIndexItem.modifyDOM(ids, this.state.inputVal, mode);
  }

  onSave() {
    const { nodeIds, updateNodeMarkers } = this.props;
    const { inputVal } = this.state;
    updateNodeMarkers(inputVal, nodeIds || null);
    this.setState({ inputVal, oldVal: inputVal, isOpen: false }, () => {
      this.modifyIndexItemDOM('edited');
    });
  }

  contentIsChanged() {
    return this.state.inputVal !== this.state.oldVal;
  }

  onInteraction(nextOpenState) {
    // This function called on ESC
    // This function is NOT called on buttons click

    const isClosing = !nextOpenState;

    if (isClosing && this.contentIsChanged()) {
      if (!this.state.showConfirmSave) {
        this.setState({ showConfirmSave: true });
        return;
      }
    }
    this.setState({ isOpen: nextOpenState });
  }

  onOpened() {
    if (this.popoverRef) {
      const elInput = this.popoverRef.getElementsByClassName(Classes.INPUT)[0];
      if (elInput) {
        elInput.focus();
      }
    }
  }

  onSubmit(e) {
    e.preventDefault();
    if (this.state.showConfirmSave) return;
    this.onSave();
  }

  getInputVal() {
    // Resolving NodeId -> Marker -> Marker text
    const { nodeIds } = this.props;
    let inputVal = '';
    const { nodesMarkers } = store.getState();
    if (nodeIds) {
      const lastId = nodeIds[nodeIds.length - 1];
      let lastMarkerText = '';
      if (nodesMarkers[lastId] && nodesMarkers[lastId].length !== 0) {
        const lastMarker = nodesMarkers[lastId][0];
        lastMarkerText = EditIndexItem.getMarkerText(lastMarker);
        if (lastMarkerText) {
          inputVal = lastMarkerText;
        }
      }
    } else {
      let markerText = '';
      const { currentIndexNodeId, currentIndexLocatorId } = store.getState();
      if (
        nodesMarkers[currentIndexNodeId] &&
        nodesMarkers[currentIndexNodeId].length !== 0
      ) {
        const marker = nodesMarkers[currentIndexNodeId][currentIndexLocatorId];
        markerText = EditIndexItem.getMarkerText(marker);
        if (markerText) {
          inputVal = markerText;
        }
      }
    }
    return inputVal;
  }

  render() {
    const { theme } = store.getState();
    const backdropProps =
      theme === constants.THEME_DARK
        ? { className: 'akisOverlayBackdropDark' }
        : { className: 'akisOverlayBackdropLight' };
    return (
      <ButtonGroup
        onClick={e => {
          e.stopPropagation();
        }}
      >
        <Popover
          position={Position.LEFT}
          minimal={true}
          backdropProps={backdropProps}
          canEscapeKeyClose={true}
          hasBackdrop={true}
          interactionKind={PopoverInteractionKind.CLICK}
          isOpen={this.state.isOpen}
          popoverClassName={`${
            Classes.POPOVER_CONTENT_SIZING
          } akisIndexEditPopover`}
          onInteraction={state => this.onInteraction(state)}
          onOpened={this.onOpened}
          popoverRef={ref => {
            this.popoverRef = ref;
          }}
        >
          {/* Target */}
          <Button
            minimal={this.props.minimal}
            icon={IconNames.EDIT}
            title="Edit this index item"
            className={Classes.SMALL}
            elementRef={ref => (this.editButtonRef = ref)}
            disabled={this.props.disabled}
            onClick={e => {
              // Opening edit dialog,
              // positioning to the editor marker by calling first
              // locator button onclick.
              e.stopPropagation();
              const state = {
                isOpen: true,
                showConfirmSave: false
              };
              this.setState(state, () => {
                const { currentIndexNodeId } = store.getState();
                const { nodeIndex } = this.props;
                if (currentIndexNodeId !== nodeIndex) {
                  store.dispatch(setCurrentIndexNodeId(nodeIndex));
                  store.dispatch(setCurrentIndexLocatorId(0));
                  const el = document.getElementById(
                    `akisBookIndexDetailButton-${nodeIndex}-0`
                  );
                  if (el) {
                    const inputVal = this.getInputVal();
                    this.setState({ inputVal, oldVal: inputVal }, () => {
                      el.click();
                    });
                  }
                }
              });
            }}
          />
          {/* Content */}
          <div>
            <Transition
              in={this.state.oldVal !== this.state.inputVal}
              timeout={transitionDuration}
            >
              {state => {
                return (
                  <div
                    style={{
                      ...defaultTransitionStyle,
                      ...transitionStylesDiffer[state]
                    }}
                  >
                    {this.state.inputVal !== null &&
                      this.state.oldVal !== null && (
                        <Differ
                          from={this.state.oldVal}
                          to={this.state.inputVal}
                        />
                      )}
                  </div>
                );
              }}
            </Transition>
            <form onSubmit={this.onSubmit.bind(this)}>
              <InputGroup
                disabled={this.state.showConfirmSave}
                type="text"
                value={this.state.inputVal}
                onChange={e => {
                  this.onInputChange(e);
                  if (this.state.showConfirmSave) {
                    this.setState({
                      showConfirmSave: false
                    });
                  }
                }}
                style={{
                  fontFamily: 'Roboto Mono',
                  fontSize: this.fontScaling + '%',
                  letterSpacing: '0.1px',
                  minWidth: '600px'
                }}
                large={true}
                rightElement={
                  <ButtonGroup>
                    <Button
                      disabled={this.state.showConfirmSave}
                      icon={IconNames.TICK}
                      title="Save"
                      onClick={this.onSave}
                    />
                    <Button
                      disabled={
                        this.state.showConfirmSave || !this.contentIsChanged()
                      }
                      icon={IconNames.REFRESH}
                      title="Reset"
                      onClick={() => {
                        this.setState({ inputVal: this.state.oldVal });
                      }}
                    />
                    <Divider vertical="true" />
                    <Button
                      disabled={this.state.showConfirmSave}
                      icon={IconNames.CROSS}
                      title="Cancel"
                      className={Classes.POPOVER_DISMISS}
                    />
                  </ButtonGroup>
                }
              />
            </form>

            <Transition
              in={this.state.showConfirmSave}
              timeout={transitionDuration}
            >
              {state => {
                let showContent = false;
                if (state === 'entered') {
                  showContent = true;
                }
                return (
                  <div
                    style={{
                      ...defaultTransitionStyle,
                      ...transitionStylesSaveButton[state]
                    }}
                  >
                    <div
                      style={{
                        display: showContent ? 'initial' : 'none'
                      }}
                    >
                      <Divider />
                      <ButtonGroup fill={true}>
                        <Button
                          icon={IconNames.SAVE}
                          text="SAVE changes"
                          onClick={this.onSave}
                          alignText={Alignment.CENTER}
                          fill={true}
                        />
                        <Button
                          icon={IconNames.CROSS}
                          text="CANCEL"
                          onClick={() =>
                            this.setState({ showConfirmSave: false })
                          }
                          alignText={Alignment.CENTER}
                          fill={true}
                        />
                      </ButtonGroup>
                    </div>
                  </div>
                );
              }}
            </Transition>
          </div>
        </Popover>
        {!this.props.nodeIds && (
          <Popover
            backdropProps={backdropProps}
            canEscapeKeyClose={true}
            hasBackdrop={true}
            interactionKind={PopoverInteractionKind.CLICK}
            isOpen={this.state.isRemoveOpen}
            popoverClassName={Classes.POPOVER_CONTENT_SIZING}
            position={Position.BOTTOM_RIGHT}
          >
            {/* Target */}
            <Button
              title="Remove"
              round={false}
              className={Classes.SMALL}
              icon={IconNames.REMOVE}
              onClick={e => {
                this.setState({
                  isRemoveOpen: true
                });
              }}
            />
            {/* Content */}
            <div
              onClick={e => {
                e.stopPropagation();
              }}
            >
              <div
                style={{
                  display: this.props.locatorsCount > 1 ? 'initial' : 'none'
                }}
              >
                <Switch
                  label="Remove range"
                  alignIndicator={Alignment.RIGHT}
                  innerLabel="All"
                  innerLabelChecked="Selected"
                  checked={this.state.locatorsRangeChecked}
                  onChange={e => {
                    this.setState({
                      locatorsRangeChecked: e.target.checked,
                      removeLocatorsFrom: 1,
                      removeLocatorsTo: this.props.locatorsCount
                    });
                  }}
                />
                <div
                  style={{
                    display: this.state.locatorsRangeChecked
                      ? 'initial'
                      : 'none'
                  }}
                >
                  <RangeSlider
                    disabled={!this.state.locatorsRangeChecked}
                    max={this.props.locatorsCount}
                    min={1}
                    labelStepSize={
                      this.props.locatorsCount / 10 > 1
                        ? this.props.locatorsCount / 10
                        : 1
                    }
                    value={[
                      this.state.removeLocatorsFrom,
                      this.state.removeLocatorsTo
                    ]}
                    onChange={range => {
                      this.setState({
                        removeLocatorsFrom: range[0],
                        removeLocatorsTo: range[1]
                      });
                    }}
                  />
                </div>
                <Divider style={{ margin: '15px 0px' }} />
              </div>
              <div style={{ textAlign: 'center' }}>
                <ButtonGroup>
                  <Button
                    title="Remove from index tree"
                    icon={IconNames.REMOVE}
                    onClick={e => {
                      this.props.removeNode(
                        this.props.nodeIndex,
                        this.state.removeLocatorsFrom - 1, // convert to 0-based
                        this.state.removeLocatorsTo
                      );
                      this.setState({ isRemoveOpen: false }, () => {
                        this.modifyIndexItemDOM('removed');
                      });
                    }}
                  >
                    Remove
                  </Button>
                  <Button
                    title="Cancel"
                    icon={IconNames.CROSS}
                    onClick={e => {
                      e.stopPropagation();
                      this.setState({ isRemoveOpen: false });
                    }}
                  >
                    Cancel
                  </Button>
                </ButtonGroup>
              </div>
            </div>
          </Popover>
        )}
      </ButtonGroup>
    );
  }
}
