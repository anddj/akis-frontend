import React from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Callout,
  Card,
  Classes,
  Dialog,
  Divider,
  FormGroup,
  Icon,
  InputGroup,
  Intent,
  NumericInput,
  Slider,
  Switch,
  Tag,
  TextArea
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

import { THEME_DARK } from '../lib/constants';
import store from '../lib/store';
import {
  toggleTheme,
  setFontScaling,
  setConcordanceLines
} from '../actions/actionCreators';
import {
  saveUserSettings,
  getClientConfig,
  deleteUser
} from '../services/userSettings';
import { TopToaster } from '../components/TopToaster';
import config from '../config/appConfig';
import * as constants from '../lib/constants';
import ChangePasswordDialog from './ChangePasswordDialog';

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadedFromServer: false,
      buildIndexParamsString: '{}',
      showDeleteAccountDialog: false,
      deleteAccountDialogConfirmText: '',
      deleteAccountDialogProcessing: false,
      isChangePasswordDialogOpen: false
    };
    this.onDeleteUserAccount = this.onDeleteUserAccount.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (
      !state.loadedFromServer &&
      typeof props.userSettings.build_index_params !== 'undefined'
    ) {
      return {
        ...state,
        buildIndexParamsString: JSON.stringify(
          props.userSettings.build_index_params || {},
          null,
          2
        ),
        loadedFromServer: true
      };
    }
    return state;
  }

  onDeleteUserAccount() {
    this.setState({ deleteAccountDialogProcessing: true }, () => {
      this.props.dispatch(
        deleteUser(() => {
          this.setState({ deleteAccountDialogProcessing: false });
        })
      );
    });
  }

  renderProcentLabel(val) {
    return `${val}%`;
  }

  render() {
    const { showSettings } = this.props;
    const theme =
      this.props.theme === constants.THEME_DARK ? Classes.DARK : Classes.LIGHT;
    return (
      <div
        className="akisColumn"
        style={{
          display: showSettings ? 'block' : 'none'
        }}
      >
        <ChangePasswordDialog
          isOpen={this.state.isChangePasswordDialogOpen}
          onClose={() => {
            this.setState({ isChangePasswordDialogOpen: false });
          }}
        />
        <Dialog
          isOpen={this.state.showDeleteAccountDialog}
          canOutsideClickClose={false}
          onClose={() => {
            this.setState({ showDeleteAccountDialog: false });
          }}
          onOpening={e => {
            this.setState({
              deleteAccountDialogConfirmText: '',
              deleteAccountDialogProcessing: false
            });
          }}
          title="Delete user account"
          className={theme}
        >
          <div className={Classes.DIALOG_BODY}>
            <Callout intent={Intent.DANGER}>
              This will remove your user data from our servers. Please confirm
              by typing word <em>delete</em> below.
            </Callout>
            <br />
            <InputGroup
              type="text"
              placeholder="Type here..."
              onChange={e => {
                this.setState({
                  deleteAccountDialogConfirmText: e.target.value
                });
              }}
              disabled={this.state.deleteAccountDialogProcessing}
            />
            <br />
            <Button
              text="YES, delete my user account"
              intent={Intent.DANGER}
              onClick={this.onDeleteUserAccount}
              disabled={
                this.state.deleteAccountDialogConfirmText !== 'delete' ||
                this.state.deleteAccountDialogProcessing
              }
              loading={this.state.deleteAccountDialogProcessing}
              fill={true}
            />
          </div>
        </Dialog>
        <Card className="akisCard">
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between'
            }}
          >
            <h6 className={`${Classes.HEADING} ${Classes.TEXT_MUTED} `}>
              <Icon icon={IconNames.CHEVRON_DOWN} />
              &nbsp;User Settings
            </h6>
            {config.vtexMode && (
              <Button
                text="CONFIG"
                icon={IconNames.DOWNLOAD}
                onClick={() => {
                  this.props.dispatch(getClientConfig());
                }}
                title="Download client configuration file"
              />
            )}
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between'
            }}
          >
            <div style={{ width: config.vtexMode ? 'auto' : '100%' }}>
              <FormGroup label="Dark theme" inline={true}>
                <Switch
                  checked={this.props.theme === THEME_DARK}
                  onChange={e => {
                    store.dispatch(toggleTheme());
                  }}
                  large={true}
                />
              </FormGroup>
              <FormGroup
                label={
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width: '100%'
                    }}
                  >
                    <span>UI font scaling</span>
                    <Button
                      minimal={true}
                      onClick={() => {
                        store.dispatch(
                          setFontScaling(constants.DEFAULT_FONT_SCALING)
                        );
                      }}
                      title="Reset to default value"
                    >
                      reset
                    </Button>
                  </div>
                }
              >
                <Slider
                  min={80}
                  max={160}
                  stepSize={1}
                  labelStepSize={40}
                  onChange={val => {
                    store.dispatch(setFontScaling(val));
                    // Refresh main editor to adjust to the changes
                    store.getState().editorInstance.refresh();
                  }}
                  labelRenderer={this.renderProcentLabel}
                  value={this.props.fontScaling}
                />
              </FormGroup>
            </div>
            {config.vtexMode && <Divider />}
            {config.vtexMode && (
              <FormGroup
                label={
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between'
                    }}
                  >
                    <span>Concordance lines</span>
                    <Button
                      minimal={true}
                      onClick={() => {
                        store.dispatch(
                          setConcordanceLines(
                            constants.DEFAULT_CONCORDANCE_LINES
                          )
                        );
                      }}
                      title="Reset to default value"
                    >
                      reset
                    </Button>
                  </div>
                }
              >
                <FormGroup helperText="Top">
                  <NumericInput
                    min={0}
                    max={10}
                    fill={true}
                    value={this.props.concordanceLines.top}
                    onValueChange={val => {
                      const { concordanceLines } = store.getState();
                      store.dispatch(
                        setConcordanceLines(
                          Object.assign(concordanceLines, { top: val })
                        )
                      );
                    }}
                  />
                </FormGroup>
                <FormGroup helperText="Bottom">
                  <NumericInput
                    min={0}
                    max={10}
                    fill={true}
                    value={this.props.concordanceLines.bottom}
                    onValueChange={val => {
                      const { concordanceLines } = store.getState();
                      store.dispatch(
                        setConcordanceLines(
                          Object.assign(concordanceLines, { bottom: val })
                        )
                      );
                    }}
                  />
                </FormGroup>
              </FormGroup>
            )}
          </div>
          <FormGroup
            label={
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between'
                }}
              >
                <span>Params</span>
                <Button
                  minimal={true}
                  onClick={() => {
                    this.setState({
                      buildIndexParamsString: JSON.stringify(
                        config.buildIndexParamsDefault,
                        null,
                        2
                      )
                    });
                  }}
                  title="Reset to default value"
                >
                  reset
                </Button>
              </div>
            }
            helperText={
              <span>
                Build index parameters,&nbsp;
                <abbr title="JavaScript Object Notation">JSON</abbr>
                &nbsp;
              </span>
            }
          >
            <TextArea
              rows="12"
              onChange={e => {
                this.setState({
                  buildIndexParamsString: e.target.value
                });
              }}
              value={this.state.buildIndexParamsString}
              fill={true}
              className={`${Classes.MONOSPACE_TEXT} akisTextarea`}
            />
          </FormGroup>
          <Button
            text="Update settings"
            fill={true}
            intent={Intent.PRIMARY}
            onClick={() => {
              // Validate params JSON
              let buildIndexParamsStringObj = null;
              try {
                buildIndexParamsStringObj = JSON.parse(
                  this.state.buildIndexParamsString.trim() || '{}'
                );
              } catch (e) {
                return TopToaster.show({
                  intent: Intent.DANGER,
                  message: 'Error parsing JSON. ' + String(e)
                });
              }
              const userSettings = {
                build_index_params: buildIndexParamsStringObj,
                concordance_lines: this.props.concordanceLines,
                dark_theme: this.props.theme === THEME_DARK,
                font_scaling: this.props.fontScaling
              };
              this.props.dispatch(saveUserSettings(userSettings));
            }}
          />
          <br />
          <h6 className={`${Classes.HEADING} ${Classes.TEXT_MUTED}`}>
            <Icon icon={IconNames.CHEVRON_DOWN} />
            &nbsp;LaTeX editor shortcuts
          </h6>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Tag>Find/Replace</Tag>
          </div>
          <ul className={`${Classes.LIST} ${Classes.LIST_UNSTYLED}`}>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Ctrl</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>F</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Start searching</em>
            </li>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Ctrl</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>G</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Find next</em>
            </li>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Shift</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>Ctrl</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>G</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Find previous</em>
            </li>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Shift</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>Ctrl</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>F</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Replace</em>
            </li>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Shift</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>Ctrl</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>R</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Replace all</em>
            </li>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Alt</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>G</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Jump to line</em>
            </li>
          </ul>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Tag>Selected Text Transformation</Tag>
          </div>
          <ul className={`${Classes.LIST} ${Classes.LIST_UNSTYLED}`}>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Alt</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>I</code>
              </span>
              <em className={Classes.TEXT_MUTED}>
                \index
                {'{<selection>}'}
              </em>
            </li>
          </ul>
          <br />
          <h6 className={`${Classes.HEADING} ${Classes.TEXT_MUTED}`}>
            <Icon icon={IconNames.CHEVRON_DOWN} />
            &nbsp;Other shortcuts
          </h6>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Tag>Index Locators Navigation</Tag>
          </div>
          <ul className={`${Classes.LIST} ${Classes.LIST_UNSTYLED}`}>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Alt</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>.</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Right</em>
            </li>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Alt</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>,</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Left</em>
            </li>
          </ul>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Tag>Index Tree Terms Navigation</Tag>
          </div>
          <ul className={`${Classes.LIST} ${Classes.LIST_UNSTYLED}`}>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Alt</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>M</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Down</em>
            </li>
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
              <span>
                <code className={Classes.CODE}>Alt</code>
                <span className={Classes.TEXT_MUTED}>+</span>
                <code className={Classes.CODE}>K</code>
              </span>
              <em className={Classes.TEXT_MUTED}>Up</em>
            </li>
          </ul>
          <br />
          <h6 className={`${Classes.HEADING} ${Classes.TEXT_MUTED}`}>
            <Icon icon={IconNames.CHEVRON_DOWN} />
            &nbsp;Account administrative tasks
          </h6>
          <Button
            fill={true}
            onClick={e => {
              this.setState({ isChangePasswordDialogOpen: true });
            }}
          >
            Change password
          </Button>
          <Button
            fill={true}
            onClick={e => {
              this.setState({ showDeleteAccountDialog: true });
            }}
          >
            Delete account
          </Button>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = ({
  concordanceLines,
  fontScaling,
  showSettings,
  theme,
  userSettings
}) => ({
  concordanceLines,
  fontScaling,
  showSettings,
  theme,
  userSettings
});

export default connect(mapStateToProps)(Settings);
