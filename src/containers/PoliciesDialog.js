import React from 'react';
import Policies from './Policies';
import { Classes, Dialog } from '@blueprintjs/core';

// Higher oder component for policies dialog

export default props => {
  return (
    <Dialog
      {...props}
      title="Policies and terms of service"
      style={{
        borderRadius: '0px',
        height: '600px',
        minWidth: '800px',
        width: '800px'
      }}
    >
      <div
        className={Classes.DIALOG_BODY}
        style={{
          height: '100%',
          width: '780px',
          overflow: 'hidden',
          borderRadius: '0px'
        }}
      >
        <a
          href="/policies-and-terms-of-service"
          target="_blank"
          style={{
            float: 'right',
            paddingRight: '10px',
            fontSize: 'small'
          }}
        >
          open in separate window
        </a>
        <Policies />
      </div>
    </Dialog>
  );
};
