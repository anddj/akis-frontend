import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  Button,
  Callout,
  Card,
  Checkbox,
  Colors,
  Elevation,
  InputGroup,
  Intent,
  Label
} from '@blueprintjs/core';
import { isEmail, trim, escape } from 'validator';

import auth from '../lib/auth';
import MainContainer from './MainContainer';
import config from '../config/appConfig';
import * as constants from '../lib/constants';
import {
  REGEXP_UNICODE_NAME,
  REGEXP_UNICODE_STRING,
  REGEXP_USERNAME
} from '../lib/constants-regexp';
import Header from '../components/Header';
import PoliciesDialog from './PoliciesDialog';

const { storageProvider } = config;

class Register extends React.Component {
  constructor(props) {
    super(props);
    if (typeof storageProvider !== 'undefined') storageProvider.clear();
    this.state = {
      username: '',
      email: '',
      password: '',
      passwordConfirm: '',
      firstname: '',
      lastname: '',
      error: '',
      isPolicyDialogOpen: false,
      isPolicyAccepted: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target,
      value = target.value,
      name = target.name;
    this.setState({ [name]: value });
  }

  getUserValidationError(user) {
    // Validate User data
    // return error text on first error or null
    const username = trim(user.username);
    const password = trim(user.password);
    const passwordConfirm = trim(user.passwordConfirm);
    const email = trim(user.email);
    const firstname = trim(user.firstname);
    const lastname = trim(user.lastname);

    if (
      username === '' ||
      password === '' ||
      passwordConfirm === '' ||
      email === ''
    ) {
      return constants.ERROR_ALL_FIELDS_REQUIRED;
    }
    if (password !== passwordConfirm) {
      return constants.ERROR_PASSWORDS_DO_NOT_MATCH;
    }
    if (password.length < 8) {
      return constants.ERROR_PASSWORD_TOO_SHORT_8;
    }
    if (!isEmail(email)) {
      return constants.ERROR_EMAIL_NOT_VALID;
    }
    const { registerEmailsAllowed } = config;
    if (registerEmailsAllowed && registerEmailsAllowed.length > 0) {
      const emailsFound = registerEmailsAllowed.find(domain => {
        return email.endsWith(domain);
      });
      if (!emailsFound) {
        return constants.ERROR_LIMITED_DOMAINS_ONLY;
      }
    }
    if (username.length > 30) {
      return constants.ERROR_USERNAME_TOO_LONG_30;
    }
    if (!REGEXP_USERNAME.test(username)) {
      return constants.ERROR_USERNAME;
    }
    if (username.endsWith('.')) {
      return constants.ERROR_USERNAME_ENDS_DOT;
    }
    if (username.indexOf('.') !== -1) {
      if (username.indexOf('.') !== username.lastIndexOf('.')) {
        return constants.ERROR_USERNAME_MULTIPLE_DOTS;
      }
    }
    if (firstname) {
      if (firstname.length > 30) {
        return constants.ERROR_FIRSTNAME_LENGTH_30;
      }
      if (REGEXP_UNICODE_STRING.exec(firstname[0]) === null) {
        return constants.ERROR_FIRSTNAME_FIRST_LETTER;
      }
      if (REGEXP_UNICODE_NAME.exec(firstname) === null) {
        return constants.ERROR_FIRSTNAME_NOT_VALID;
      }
    }
    if (lastname) {
      if (lastname.length > 30) {
        return constants.ERROR_LASTNAME_LENGTH_30;
      }
      if (REGEXP_UNICODE_STRING.exec(lastname[0]) === null) {
        return constants.ERROR_LASTNAME_FIRST_LETTER;
      }
      if (REGEXP_UNICODE_NAME.exec(lastname) === null) {
        return constants.ERROR_LASTNAME_NOT_VALID;
      }
    }

    return null;
  }

  handleSubmit(event) {
    event.preventDefault();

    const {
      username,
      password,
      passwordConfirm,
      email,
      firstname,
      lastname
    } = this.state;

    const firstError = this.getUserValidationError({
      username,
      email,
      password,
      passwordConfirm,
      firstname,
      lastname
    });

    if (firstError) {
      this.setState({ error: firstError });
    } else {
      auth.register(
        {
          username,
          email,
          password,
          firstname,
          lastname
        },
        (registered, respCode, respText) => {
          if (!registered) {
            let errorMessage = 'Failed to register.';
            if (respCode && (respCode === 429 || respCode === 503)) {
              errorMessage += ` ${constants.ERROR_TO_MANY_REQUESTS}`;
            }
            if (respCode && respCode === 409) {
              // Conflict report
              const text = respText ? ' ' + escape(respText) : '';
              errorMessage += text;
            }
            this.setState({
              error: errorMessage
            });
          } else {
            window.location = '/register-almost-done';
          }
        }
      );
    }
  }

  render() {
    const { registerEmailsAllowed } = config;
    return (
      <MainContainer>
        <PoliciesDialog
          onClose={e => {
            this.setState({ isPolicyDialogOpen: false });
          }}
          isOpen={this.state.isPolicyDialogOpen}
        />
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%'
          }}
        >
          <Header />
          <Card elevation={Elevation.FOUR} style={{ padding: '70px' }}>
            <form method="POST" onSubmit={this.handleSubmit}>
              {this.state.error && (
                <Callout intent={Intent.DANGER} style={{ marginBottom: '5px' }}>
                  {this.state.error}
                </Callout>
              )}
              {registerEmailsAllowed && registerEmailsAllowed.length > 0 && (
                <Callout
                  intent={Intent.WARNING}
                  style={{ marginBottom: '5px' }}
                  icon={null}
                >
                  Current email registration is restricted to specific domains (
                  {registerEmailsAllowed.join(', ')}) only.
                </Callout>
              )}
              <Label>
                <span
                  style={{
                    color: Colors.RED5,
                    marginRight: '2px'
                  }}
                >
                  *
                </span>
                Username
                <InputGroup
                  fill={true}
                  large={true}
                  name="username"
                  onChange={this.handleInputChange}
                  placeholder="Username"
                  type="text"
                />
              </Label>
              {config.vtexMode && (
                <div>
                  <Label>
                    First name
                    <InputGroup
                      fill={true}
                      large={true}
                      name="firstname"
                      onChange={this.handleInputChange}
                      placeholder="First name"
                      type="text"
                    />
                  </Label>

                  <Label>
                    Last name
                    <InputGroup
                      fill={true}
                      large={true}
                      name="lastname"
                      onChange={this.handleInputChange}
                      placeholder="Last name"
                      type="text"
                    />
                  </Label>
                </div>
              )}
              <Label>
                <span
                  style={{
                    color: Colors.RED5,
                    marginRight: '2px'
                  }}
                >
                  *
                </span>
                Email
                <InputGroup
                  fill={true}
                  large={true}
                  name="email"
                  onChange={this.handleInputChange}
                  placeholder="Email"
                  type="text"
                />
              </Label>

              <Label>
                <span
                  style={{
                    color: Colors.RED5,
                    marginRight: '2px'
                  }}
                >
                  *
                </span>
                Password
                <InputGroup
                  fill={true}
                  large={true}
                  name="password"
                  onChange={this.handleInputChange}
                  placeholder="Password"
                  type="password"
                />
              </Label>

              <Label>
                <span
                  style={{
                    color: Colors.RED5,
                    marginRight: '2px'
                  }}
                >
                  *
                </span>
                Repeat password
                <InputGroup
                  fill={true}
                  large={true}
                  name="passwordConfirm"
                  onChange={this.handleInputChange}
                  placeholder="Password confirm"
                  type="password"
                />
              </Label>

              <Checkbox
                checked={this.state.isPolicyAccepted}
                label={
                  <span>
                    By registering I agree to the&nbsp;
                    <button
                      type="button"
                      className="akisLinkButton"
                      onClick={e => {
                        this.setState({ isPolicyDialogOpen: true });
                      }}
                    >
                      policies and terms of service
                    </button>
                    .
                  </span>
                }
                name="isPolicyAccepted"
                onChange={e => {
                  this.setState({ isPolicyAccepted: e.target.checked });
                }}
                style={{ marginTop: '30px' }}
              />

              <div
                style={{
                  marginTop: '30px'
                }}
              >
                <Button
                  intent={Intent.PRIMARY}
                  type="submit"
                  fill={true}
                  large={true}
                  text="Sign up"
                  disabled={!this.state.isPolicyAccepted}
                />
              </div>
            </form>
          </Card>
        </div>
      </MainContainer>
    );
  }
}

export default connect()(withRouter(Register));
