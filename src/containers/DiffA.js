import React from 'react';
import Differ from 'react-differ';
import { connect } from 'react-redux';
import { Callout, Intent } from '@blueprintjs/core';

import store from '../lib/store';

// Diff between base_content and current_content

class DiffA extends React.Component {
  render() {
    if (!(this.props.selectedTabId === 'diffA' && this.props.bookItem))
      return <div>no data</div>;
    return (
      <div
        style={{
          height: '100%',
          width: '100%'
        }}
      >
        <Callout intent={Intent.WARNING} icon={null}>
          Diff between base and current versions.
        </Callout>
        <Differ
          from={this.props.bookItem.base_content}
          to={this.props.bookItem.current_content}
        />
      </div>
    );
  }
}

const mapStateToProps = ({
  bookItemActive // ID
}) => {
  const { bookItems } = store.getState();
  const bookItem = bookItems.find(item => {
    return item._id === bookItemActive;
  });

  return {
    bookItem
  };
};

export default connect(mapStateToProps)(DiffA);
