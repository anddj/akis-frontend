import React from 'react';
import { connect } from 'react-redux';
import path from 'path';
import {
  Alignment,
  Button,
  ButtonGroup,
  Divider,
  Card,
  Classes,
  ControlGroup,
  Icon,
  Intent,
  Label,
  Menu,
  MenuItem,
  Popover,
  Position,
  ProgressBar,
  Tag
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { basename } from 'path';

import {
  setBookItemActive,
  setBookItemIdsToIndex,
  setEditorGoToCoords
} from '../actions/actionCreators';
import store from '../lib/store';
import { resetBookIndexData } from '../lib/utils';
import { TopToaster } from '../components/TopToaster';
import * as constants from '../lib/constants';

class FilesList extends React.Component {
  constructor(props) {
    super(props);
    this.resetToInitial = this.resetToInitial.bind(this);
    this.resetToLastSaved = this.resetToLastSaved.bind(this);
    this.state = {
      collapsed: false,
      changedBookItemsCount: 0
    };
  }

  static getDerivedStateFromProps(props, state) {
    const changedBookItems = props.bookItems.filter(bookItem => {
      return bookItem.current_content !== props.savedContent[bookItem._id];
    });
    if (changedBookItems.length !== state.changedBookItemsCount) {
      return { changedBookItemsCount: changedBookItems.length };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // TODO: check if update needed
    if (prevProps.bookItems !== this.props.bookItems) {
      // Reset
      setBookItemIdsToIndex([]);
    }
  }

  resetToInitial(bookItem) {
    // Partial reset
    const { editorInstance, bookItemsBuffers } = store.getState();
    bookItemsBuffers[bookItem._id].setValue(bookItem.base_content);
    resetBookIndexData(bookItem._id);
    editorInstance.refresh();
    const fileName = path.basename(bookItem.filepath);
    TopToaster.show({
      message: 'Reset to initial (' + fileName + ')',
      intent: Intent.SUCCESS
    });
  }

  resetToLastSaved(bookItem) {
    const { editorInstance, bookItemsBuffers } = store.getState();
    bookItemsBuffers[bookItem._id].setValue(
      this.props.savedContent[bookItem._id]
    );
    resetBookIndexData(bookItem._id);
    editorInstance.refresh();
    const fileName = path.basename(bookItem.filepath);
    TopToaster.show({
      message: 'Reset to last saved (' + fileName + ')',
      intent: Intent.SUCCESS
    });
  }

  render() {
    const {
      bookItems,
      savedContent,
      bookItemIdsToIndex,
      setBookItemIdsToIndex,
      bookItemActive,
      indexData
    } = this.props;
    if (!bookItems) return;
    const indexCounters = {};
    const uniqueIndeces = {};
    let maxNumber = 0;

    function countIndeces(indeces) {
      indeces.forEach(indexItem => {
        if (indexItem.locators && indexItem.locators.length > 0) {
          indexItem.locators.forEach(locator => {
            if (!indexCounters[locator.fileName]) {
              indexCounters[locator.fileName] = 0;
            }
            if (!uniqueIndeces[locator.fileName]) {
              uniqueIndeces[locator.fileName] = [];
            }
            indexCounters[locator.fileName]++;
            const { originTex } = locator;
            if (
              originTex &&
              uniqueIndeces[locator.fileName].indexOf(originTex) === -1
            ) {
              uniqueIndeces[locator.fileName].push(originTex);
            }
            maxNumber = Math.max(maxNumber, indexCounters[locator.fileName]);
          });
        }
        if (indexItem.indexTree && indexItem.indexTree.length > 0) {
          countIndeces(indexItem.indexTree);
        }
      });
    }

    // Count indeces (recursive)
    if (indexData && indexData.Right && indexData.Right.length > 0) {
      countIndeces(indexData.Right);
    }
    const bookItemsUI = bookItems.map((bookItem, index) => {
      const filepath = '/' + bookItem.filepath;

      return (
        <ControlGroup
          key={bookItem._id}
          fill="true"
          style={{ paddingTop: '5px', paddingLeft: '5px' }}
        >
          <Label
            className={`${Classes.CONTROL} ${Classes.CHECKBOX} ${
              Classes.LARGE
            }`}
            title={filepath}
            style={{ marginTop: '10px' }}
          >
            <input
              type="checkbox"
              checked={bookItemIdsToIndex.indexOf(bookItem._id) !== -1}
              onChange={e => {
                if (e.target.checked) {
                  if (bookItemIdsToIndex.indexOf(bookItem._id) === -1) {
                    setBookItemIdsToIndex([
                      ...bookItemIdsToIndex,
                      bookItem._id
                    ]);
                  }
                } else {
                  setBookItemIdsToIndex(
                    bookItemIdsToIndex.filter(id => {
                      return id !== bookItem._id;
                    })
                  );
                }
              }}
            />
            <span className="bp3-control-indicator" />
          </Label>

          <Popover
            content={
              <Menu>
                <MenuItem
                  text="Reset to initial version"
                  onClick={() => {
                    this.resetToInitial(bookItem);
                  }}
                />
                <MenuItem
                  text="Reset to last saved version"
                  onClick={() => {
                    this.resetToLastSaved(bookItem);
                  }}
                />
              </Menu>
            }
            position={Position.BOTTOM_RIGHT}
          >
            <Button
              className={Classes.SMALL}
              style={{ minHeight: '46px' }}
              icon={<Icon icon={IconNames.MORE} />}
              fill={true}
              onClick={() => {
                if (bookItem._id !== bookItemActive) {
                  this.props.setEditorGoToCoords({ line: 0, ch: 0 });
                  this.props.setBookItemActive(bookItem._id);
                }
              }}
            />
          </Popover>

          <Button
            alignText={Alignment.LEFT}
            style={{ overflow: 'hidden' }}
            className={
              (bookItemIdsToIndex.indexOf(bookItem._id) !== -1
                ? Classes.INTENT_PRIMARY
                : '') + ' akisFilesListButton'
            }
            minimal={true}
            type="button"
            large={true}
            active={bookItemActive === bookItem._id}
            rightIcon={
              bookItem.current_content === savedContent[bookItem._id]
                ? null
                : IconNames.ANNOTATION
            }
            onClick={e => {
              this.props.setEditorGoToCoords({ line: 0, ch: 0 });
              this.props.setBookItemActive(bookItem._id);
            }}
            title={`${filepath}, ${bookItem.current_content.length}`}
          >
            <div>
              {basename(bookItem.filepath)}
              {indexCounters[filepath] && (
                <span className={Classes.TEXT_SMALL}>
                  &nbsp;
                  <em>
                    {uniqueIndeces[filepath].length}/{indexCounters[filepath]}
                  </em>
                </span>
              )}
            </div>
            <div
              style={{ display: indexCounters[filepath] ? 'initial' : 'none' }}
            >
              <ProgressBar
                intent={Intent.SUCCESS}
                value={
                  uniqueIndeces[filepath]
                    ? uniqueIndeces[filepath].length / maxNumber
                    : 0
                }
                animate={false}
                stripes={false}
                fill={true}
              />
              <ProgressBar
                intent={Intent.PRIMARY}
                value={
                  indexCounters[filepath]
                    ? indexCounters[filepath] / maxNumber
                    : 0
                }
                animate={false}
                stripes={false}
                fill={true}
              />
            </div>
          </Button>
        </ControlGroup>
      );
    });
    return (
      <div className="akisColumn">
        <Card className="akisCard">
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              height: '100%',
              overflow: 'hidden',
              minWidth: '80px'
            }}
          >
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                width: '100%'
              }}
            >
              <ButtonGroup>
                <Button
                  title="Collapse"
                  icon={
                    this.state.collapsed ? (
                      <Icon icon={IconNames.CHEVRON_RIGHT} />
                    ) : (
                      <Icon icon={IconNames.CHEVRON_LEFT} />
                    )
                  }
                  onClick={e => {
                    this.setState({ collapsed: !this.state.collapsed }, () => {
                      this.props.splitPercentage(
                        this.state.collapsed
                          ? constants.FILES_LIST_SPLIT_PERCENTAGE_MIN
                          : constants.FILES_LIST_SPLIT_PERCENTAGE_MAX
                      );
                    });
                  }}
                />
                {!this.state.collapsed && (
                  <ControlGroup>
                    <Divider />
                    <Button
                      title="Select all"
                      icon={<Icon icon={IconNames.TICK} />}
                      onClick={e => {
                        setBookItemIdsToIndex(
                          bookItems.map(bookItem => {
                            return bookItem._id;
                          })
                        );
                      }}
                    />
                    <Button
                      title="Deselect all"
                      icon={<Icon icon={IconNames.CROSS} />}
                      onClick={e => {
                        setBookItemIdsToIndex([]);
                      }}
                    />
                    <Tag>
                      {this.props.bookItemIdsToIndex.length}/
                      {this.props.bookItems.length}
                    </Tag>
                  </ControlGroup>
                )}
              </ButtonGroup>
              {!this.state.collapsed &&
                this.state.changedBookItemsCount > 0 && (
                  <Tag>
                    modified &nbsp;
                    <Tag round={true} intent={Intent.WARNING}>
                      {this.state.changedBookItemsCount}
                    </Tag>
                  </Tag>
                )}
            </div>
            <Divider
              style={{
                display: this.state.collapsed ? 'none' : 'initial'
              }}
            />
            <div
              style={{
                width: '100%',
                flex: 1,
                overflow: 'auto',
                display: this.state.collapsed ? 'none' : 'initial'
              }}
            >
              <ButtonGroup vertical={true} fill={true}>
                {bookItemsUI}
              </ButtonGroup>
            </div>
          </div>
        </Card>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setBookItemIdsToIndex: bookItemIds => {
      dispatch(setBookItemIdsToIndex(bookItemIds));
    },
    setBookItemActive: bookItemId => {
      dispatch(setBookItemActive(bookItemId));
    },
    setEditorGoToCoords: coords => {
      dispatch(setEditorGoToCoords(coords));
    }
  };
};

const mapStateToProps = ({
  bookItems,
  bookItemActive,
  savedContent,
  bookItemIdsToIndex,
  indexData
}) => ({
  bookItems,
  bookItemActive,
  savedContent,
  bookItemIdsToIndex,
  indexData
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilesList);
