import React from 'react';
import { connect } from 'react-redux';
import { hotkeys, hotkey_display } from 'react-keyboard-shortcuts';
import { Card, Button, Classes } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

class ButtonLocatorsUpComponent extends React.Component {
  hot_keys = {
    'alt+,': {
      priority: 1,
      handler: e => this.up()
    }
  };

  up() {
    const {
      currentIndexNodeId,
      currentIndexLocatorId,
      locatorsButtons
    } = this.props;
    const buttonsArray = locatorsButtons[currentIndexNodeId]
      ? locatorsButtons[currentIndexNodeId]
      : [];
    const nextIndex =
      currentIndexLocatorId - 1 < 0
        ? buttonsArray.length - 1
        : currentIndexLocatorId - 1;
    const el = document.getElementById(
      `akisBookIndexDetailButton-${currentIndexNodeId}-${nextIndex}`
    );
    if (el) {
      el.click();
    }
  }

  render() {
    return (
      <Button
        title={hotkey_display('Alt+,')}
        icon={IconNames.CARET_LEFT}
        className={Classes.SMALL}
        onClick={() => {
          this.up();
        }}
      />
    );
  }
}

class ButtonLocatorsDownComponent extends React.Component {
  hot_keys = {
    'alt+.': {
      priority: 1,
      handler: e => this.down()
    }
  };

  down() {
    const {
      currentIndexNodeId,
      currentIndexLocatorId,
      locatorsButtons
    } = this.props;
    const buttonsArray = locatorsButtons[currentIndexNodeId]
      ? locatorsButtons[currentIndexNodeId]
      : [];
    const nextIndex =
      currentIndexLocatorId + 1 >= buttonsArray.length
        ? 0
        : currentIndexLocatorId + 1;
    const el = document.getElementById(
      `akisBookIndexDetailButton-${currentIndexNodeId}-${nextIndex}`
    );
    if (el) {
      el.click();
    }
  }

  render() {
    return (
      <Button
        title={hotkey_display('Alt+.')}
        icon={IconNames.CARET_RIGHT}
        className={Classes.SMALL}
        onClick={() => {
          this.down();
        }}
      />
    );
  }
}

const ButtonUp = hotkeys(ButtonLocatorsUpComponent);
const ButtonDown = hotkeys(ButtonLocatorsDownComponent);

class BookIndexDetail extends React.Component {
  render() {
    const {
      currentIndexNodeId,
      currentIndexLocatorId,
      locatorsButtons
    } = this.props;

    const buttonsArray = locatorsButtons[currentIndexNodeId]
      ? locatorsButtons[currentIndexNodeId]
      : [];

    const buttons = buttonsArray.map((btn, index) => {
      return (
        <span
          key={index}
          className={
            currentIndexLocatorId === index ? 'akisActiveButtonContainer' : null
          }
        >
          {btn}
        </span>
      );
    });

    return (
      <div className="akisColumn">
        {buttonsArray.length ? (
          <Card className="akisCard">
            <ButtonUp {...this.props} />
            <ButtonDown {...this.props} />
            &nbsp;
            {buttons}
          </Card>
        ) : (
          <Card className="akisCard" />
        )}
      </div>
    );
  }
}

const mapStateToProps = ({
  currentIndexLocatorId,
  currentIndexNodeId,
  locatorsButtons
}) => {
  return {
    currentIndexLocatorId,
    currentIndexNodeId,
    locatorsButtons
  };
};

export default connect(mapStateToProps)(BookIndexDetail);
