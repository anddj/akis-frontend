import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  Intent,
  InputGroup,
  Card,
  Callout,
  Button,
  Elevation
} from '@blueprintjs/core';
import { isEmpty, equals, isLength, isAlphanumeric } from 'validator';

import Header from '../components/Header';
import config from '../config/appConfig';
import {
  ERROR_PASSWORDS_DO_NOT_MATCH,
  ERROR_PASSWORD_TOO_SHORT_8,
  ERROR_ALL_FIELDS_REQUIRED
} from '../lib/constants';

class ResetPwd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sent: false,
      password: '',
      passwordReapeat: '',
      message: 'Please provide us with the new password.'
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  static verifyPassword(password, passwordRepeat) {
    if (!(password && passwordRepeat)) {
      return { error: ERROR_ALL_FIELDS_REQUIRED };
    }
    const emptyOptions = { ignore_whitespace: true };
    const lengthOptions = { min: 8 };
    if (
      isEmpty(password, emptyOptions) ||
      isEmpty(passwordRepeat, emptyOptions)
    ) {
      return { error: ERROR_ALL_FIELDS_REQUIRED };
    }
    if (!equals(password, passwordRepeat)) {
      return { error: ERROR_PASSWORDS_DO_NOT_MATCH };
    }
    if (!isLength(password, lengthOptions)) {
      return { error: ERROR_PASSWORD_TOO_SHORT_8 };
    }
    return { error: null };
  }

  send(cb) {
    // Make the API call, get response status
    const { hash } = this.props.match.params;
    if (!(hash && isAlphanumeric(hash))) {
      return cb('Error.');
    }
    const api = `${config.api}/userreset/${hash}`;
    try {
      const initObject = {
        method: 'POST',
        body: JSON.stringify({
          password: this.state.password
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      };
      fetch(api, initObject)
        .then(res => {
          if (res.status === 404) {
            return cb('We do not have this email registered.');
          }
          if (res.ok) {
            return cb(null);
          }
          return cb(res.status + ' ' + res.statusText);
        })
        .catch(err => {
          console.log(err);
          cb('Error.');
        });
    } catch (e) {
      console.log(e);
      cb('Error.');
    }
  }

  onSubmit(event) {
    event.preventDefault();
    const { password, passwordRepeat } = this.state;
    try {
      const { error } = ResetPwd.verifyPassword(password, passwordRepeat);
      const isValid = error ? false : true;
      if (this.state.error) {
        // Error already shown, let's draw attention to
        // repeated error.
        this.setState({ error: 'Verifying...' });
        setTimeout(() => {
          if (!isValid) {
            return this.setState({ error });
          }
          this.send(err => {
            if (err) {
              return this.setState({ error: err });
            }
            this.setState({ sent: true });
          });
        }, 400);
      } else {
        if (!isValid) {
          return this.setState({ error });
        }
        this.send(err => {
          if (err) {
            return this.setState({ error: err });
          }
          this.setState({ sent: true });
        });
      }
    } catch (e) {
      console.log(e);
      this.setState({ error: 'Error.' });
    }
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%'
        }}
      >
        <Header />
        {!this.state.sent && (
          <Card elevation={Elevation.FOUR} style={{ width: '30%' }}>
            <Callout>{this.state.message}</Callout>
            {this.state.error && (
              <Callout intent={Intent.DANGER}>{this.state.error}</Callout>
            )}
            <form onSubmit={this.onSubmit}>
              <InputGroup
                style={{ marginTop: '2px' }}
                large={true}
                placeholder="Password"
                type="password"
                onChange={e => {
                  this.setState({ password: e.target.value });
                }}
              />
              <InputGroup
                style={{ marginTop: '2px' }}
                large={true}
                placeholder="Password (repeat)"
                type="password"
                onChange={e => {
                  this.setState({ passwordRepeat: e.target.value });
                }}
              />
              <div
                style={{
                  paddingTop: '25px'
                }}
              >
                <Button
                  fill={true}
                  intent={Intent.PRIMARY}
                  large={true}
                  text="Send"
                  type="submit"
                />
              </div>
            </form>
          </Card>
        )}
        {this.state.sent && (
          <Card elevation={Elevation.FOUR}>
            <Callout intent={Intent.SUCCESS}>
              Password was successfully changed (reset).
            </Callout>
            <p style={{ marginTop: '25px' }}>
              <a href="/login">Back to sign in page</a>
            </p>
          </Card>
        )}
      </div>
    );
  }
}

export default withRouter(ResetPwd);
