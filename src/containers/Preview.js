import React from 'react';
import { Card, Tab, Tabs } from '@blueprintjs/core';
import { connect } from 'react-redux';

import BookIndex from './BookIndex';
import DiffA from './DiffA';
import DiffB from './DiffB';

class Preview extends React.Component {
  constructor(props) {
    super(props);
    this.state = { selectedTabId: 'bookIndex' };
  }

  handleChange(tabId) {
    this.setState({ selectedTabId: tabId });
  }

  render() {
    return (
      <div
        className="akisColumn"
        style={{
          height: '100%',
          width: '100%',
          overflow: 'hidden'
        }}
      >
        <Card
          id="akisPreview"
          className="akisCard"
          style={{
            width: '100%',
            overflow: 'visible'
          }}
        >
          <Tabs
            id="PreviewTabs"
            onChange={this.handleChange.bind(this)}
            selectedTabId={this.state.selectedTabId}
            large={true}
            className="akisFullHeightAlt akisPreviewTabsContainer"
          >
            <Tab
              id="bookIndex"
              title="Index"
              panel={<BookIndex />}
              className="akisFullHeightAlt akisScrollable"
            />
            <Tabs.Expander />
            <Tab
              id="diffA"
              title="Diff A"
              panel={<DiffA selectedTabId={this.state.selectedTabId} />}
              className="akisFullHeightAlt akisScrollable"
            />
            <Tab
              id="diffB"
              title="Diff B"
              panel={<DiffB selectedTabId={this.state.selectedTabId} />}
              className="akisFullHeightAlt akisScrollable"
            />
          </Tabs>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = ({ indexData }) => {
  return {
    indexData
  };
};

export default connect(mapStateToProps)(Preview);
