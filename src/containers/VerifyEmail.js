import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { isAlphanumeric } from 'validator';
import { Callout, Card, Elevation, Intent, Icon } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

import Header from '../components/Header';
import config from '../config/appConfig';

class VerifyEmail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hash: props.match.params.hash,
      message: 'Running email verification procedure...',
      error: false
    };
  }

  componentDidMount() {
    const { hash } = this.state;
    if (!hash) {
      return this.setState({ message: 'Link error.', error: true });
    }
    if (!isAlphanumeric(hash)) {
      return this.setState({ message: 'Link error', error: true });
    }
    const req = new XMLHttpRequest();
    req.onload = () => {
      if (req.status !== 200) {
        if (req.status === 410) {
          return this.setState({
            message: 'Error. The link has been expired.',
            error: true
          });
        } else {
          return this.setState({ message: 'Verification error.', error: true });
        }
      } else {
        return this.setState({
          message: 'Successfully verified. Now you are ready to sign in.',
          error: false
        });
      }
    };
    req.open('POST', `${config.api}/verify/${hash}`);
    req.send();
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%'
        }}
      >
        <Header />
        <Card elevation={Elevation.FOUR} style={{ minWidth: '30%' }}>
          <Callout intent={this.state.error ? Intent.DANGER : Intent.SUCCESS}>
            {this.state.message}
          </Callout>
          <div
            style={{
              textAlign: 'right',
              paddingTop: '50px'
            }}
          >
            <em>
              <Link to="/login">Back to sign in page</Link>
            </em>
            <Icon
              icon={IconNames.CHEVRON_RIGHT}
              intent={Intent.PRIMARY}
              style={{ marginLeft: '10px' }}
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default withRouter(VerifyEmail);
