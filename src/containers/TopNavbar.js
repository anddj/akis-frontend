import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { IconNames } from '@blueprintjs/icons';
import {
  Alignment,
  Button,
  Classes,
  Dialog,
  Icon,
  Intent,
  Navbar,
  NavbarDivider,
  NavbarGroup,
  NavbarHeading,
  Slider,
  Switch,
  Popover,
  Position,
  Menu,
  MenuItem
} from '@blueprintjs/core';

import {
  toggleIsGettingIndexData,
  toggleSettings,
  setLogsShowOnlyErrors,
  updateUserData
} from '../actions/actionCreators';
import { saveBookItems } from '../services/bookItems';
import store from '../lib/store';
import * as constants from '../lib/constants';
import { TopToaster } from '../components/TopToaster';
import { getLogs } from '../services/logs.js';
import { updateBook, getBookZip } from '../services/books.js';
import { getIndex, buildIndex } from '../services/bookIndex';
import config from '../config/appConfig';
import auth from '../lib/auth';
import { resetBookItemsData } from '../lib/utils';
import PoliciesDialog from './PoliciesDialog';

class TopNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.onSaveBookItems = this.onSaveBookItems.bind(this);
    this.onGetIndex = this.onGetIndex.bind(this);
    this.isNotSaved = this.isNotSaved.bind(this);
    this.onDownloadZip = this.onDownloadZip.bind(this);
    this.state = {
      logPageNr: 1,
      changedBookItemsCount: 0,
      isPolicyDialogOpen: false
    };
    this.logDateFrom = null;
  }

  static getDerivedStateFromProps(props, state) {
    const changedBookItems = props.bookItems.filter(bookItem => {
      return bookItem.current_content !== props.savedContent[bookItem._id];
    });
    if (changedBookItems.length !== state.changedBookItemsCount) {
      return { changedBookItemsCount: changedBookItems.length };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.logsShowOnlyErrors !== this.props.logsShowOnlyErrors) {
      // Refetch logs data
      this.props.dispatch(getLogs(1, null, this.props.logsShowOnlyErrors));
    }
  }

  onSaveBookItems(e) {
    const { savedContent, bookItems } = store.getState();
    const changedBookItems = bookItems.filter(bookItem => {
      return bookItem.current_content !== savedContent[bookItem._id];
    });
    this.props.dispatch(saveBookItems(changedBookItems)).then(
      bookItems => {
        TopToaster.show({
          intent: Intent.SUCCESS,
          message:
            'Saved ' + (bookItems ? bookItems.length : '0') + ' book items.',
          timeout: constants.TOASTER_TIMEOUT
        });
      },
      error => {
        console.log(error);
        TopToaster.show({
          intent: Intent.DANGER,
          message: error.message
        });
      }
    );
  }

  onGetIndex(e) {
    const { dispatch } = this.props;
    if (this.isNotSaved()) {
      return TopToaster.show({
        intent: Intent.DANGER,
        message: 'Please save first!'
      });
    }
    if (!store.getState().isGettingIndexData) {
      dispatch(toggleIsGettingIndexData());
    }
    dispatch(getIndex(store.getState().bookItemIdsToIndex));
  }

  onFinished(e) {
    this.props.dispatch(updateBook({ is_finished: true }));
  }

  onFinishedRemove(e) {
    this.props.dispatch(updateBook({ is_finished: false }));
  }

  onDownloadZip(e) {
    if (this.isNotSaved()) {
      return TopToaster.show({
        intent: Intent.DANGER,
        message: 'Please save first!'
      });
    }
    if (this.props.bookItems.length) {
      this.props.dispatch(getBookZip());
    } else {
      return TopToaster.show({
        intent: Intent.WARNING,
        message: 'No files to download.'
      });
    }
  }

  isNotSaved(e) {
    // Check if editor content is saved
    const { bookItems, savedContent } = store.getState();
    return bookItems.find(function(bookItem) {
      return bookItem.current_content !== savedContent[bookItem._id];
    });
  }

  onBuild(e) {
    const { dispatch } = this.props;
    if (this.isNotSaved()) {
      return TopToaster.show({
        intent: Intent.DANGER,
        message: 'Please save first!'
      });
    }
    if (!store.getState().isGettingIndexData) {
      dispatch(toggleIsGettingIndexData());
    }
    dispatch(buildIndex(store.getState().bookItemIdsToIndex));
  }

  render() {
    const { logs, logsShowOnlyErrors } = this.props;
    if (logs && logs.meta) this.logDateFrom = logs.meta.dateFrom;
    const themeClass =
      store.getState().theme === constants.THEME_DARK
        ? Classes.DARK
        : Classes.LIGHT;
    return (
      <Navbar className={Classes.FIXED_TOP}>
        <PoliciesDialog
          onClose={e => {
            this.setState({ isPolicyDialogOpen: false });
          }}
          isOpen={this.state.isPolicyDialogOpen}
        />
        <Dialog
          canEscapeKeyClose={false}
          canOutsideClickClose={false}
          className={themeClass}
          isCloseButtonShown={false}
          isOpen={this.props.bookFinished}
          title="Proditem Finished"
        >
          <div className={Classes.DIALOG_BODY}>
            <p style={{ minHeight: '150px' }}>
              <Icon
                icon={IconNames.TICK}
                iconSize={50}
                intent={Intent.SUCCESS}
              />
              This proditem is marked as
              <code className={Classes.CODE}>finished</code>.
            </p>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                intent={Intent.PRIMARY}
                minimal={true}
                onClick={this.onFinishedRemove.bind(this)}
              >
                Remove Finished status
              </Button>
            </div>
          </div>
        </Dialog>
        <NavbarGroup align={Alignment.LEFT}>
          <NavbarHeading>{config.siteTitleShort}</NavbarHeading>
          <NavbarDivider />
          {this.props.match.path === '/' && (
            <NavbarGroup>
              <Button
                className={Classes.MINIMAL}
                icon={IconNames.COG}
                title="Settings"
                onClick={e => this.props.dispatch(toggleSettings())}
              />
              <Button
                className={Classes.MINIMAL}
                icon={IconNames.FOLDER_OPEN}
                title="Open zip"
                onClick={this.props.toggleFileUploadDialog}
              />
              <Button
                className={Classes.MINIMAL}
                icon={IconNames.DOWNLOAD}
                title="Download zip"
                onClick={this.onDownloadZip}
              />
              <NavbarDivider />
              <Button
                minimal={true}
                intent={Intent.PRIMARY}
                icon={IconNames.BUILD}
                text="Build"
                title="Build Index"
                onClick={this.onBuild.bind(this)}
                disabled={
                  !(
                    this.props.bookItemIdsToIndex &&
                    this.props.bookItemIdsToIndex.length > 0
                  )
                }
              />
              <Button
                minimal={true}
                intent={Intent.PRIMARY}
                icon={IconNames.SAVED}
                text="Save"
                title="Save all"
                loading={false}
                onClick={this.onSaveBookItems}
                disabled={!this.state.changedBookItemsCount}
              />
              <Button
                minimal={true}
                intent={Intent.PRIMARY}
                icon={IconNames.LIST_DETAIL_VIEW}
                text="Show"
                title="Show Index (for selected book items)"
                onClick={this.onGetIndex}
                disabled={
                  !(
                    this.props.bookItemIdsToIndex &&
                    this.props.bookItemIdsToIndex.length > 0
                  )
                }
              />
              {config.vtexMode && <NavbarDivider />}
              {config.vtexMode && (
                <Button
                  className={Classes.MINIMAL}
                  icon={IconNames.POWER}
                  intent={Intent.SUCCESS}
                  title="Mark Indexing process as finished"
                  onClick={this.onFinished.bind(this)}
                />
              )}
            </NavbarGroup>
          )}

          {/* Logs Navbar */}

          {this.props.match.path === '/logs' && (
            <NavbarGroup align={Alignment.LEFT}>
              <Button
                icon={IconNames.STEP_BACKWARD}
                onClick={() => {
                  if (this.state.logPageNr < 2) return;
                  this.setState(
                    {
                      logPageNr: 1
                    },
                    () => {
                      this.props.dispatch(
                        getLogs(
                          this.state.logPageNr,
                          this.logDateFrom,
                          logsShowOnlyErrors
                        )
                      );
                    }
                  );
                }}
              />
              <Button
                icon={IconNames.ARROW_LEFT}
                onClick={() => {
                  if (this.state.logPageNr < 2) return;
                  this.setState(
                    {
                      logPageNr: this.state.logPageNr - 1
                    },
                    () => {
                      this.props.dispatch(
                        getLogs(
                          this.state.logPageNr,
                          this.logDateFrom,
                          logsShowOnlyErrors
                        )
                      );
                    }
                  );
                }}
              />
              <Button
                icon={IconNames.ARROW_RIGHT}
                onClick={() => {
                  const logsShift =
                    (this.state.logPageNr - 1) * logs.meta.itemsPerPage;
                  if (
                    logs.meta.totalItems - logsShift <=
                    logs.meta.itemsPerPage
                  )
                    return;
                  this.setState(
                    {
                      logPageNr: this.state.logPageNr + 1
                    },
                    () => {
                      this.props.dispatch(
                        getLogs(
                          this.state.logPageNr,
                          this.logDateFrom,
                          logsShowOnlyErrors
                        )
                      );
                    }
                  );
                }}
              />
              <Button
                icon={IconNames.STEP_FORWARD}
                onClick={() => {
                  const logsShift =
                    (this.state.logPageNr - 1) * logs.meta.itemsPerPage;
                  if (
                    logs.meta.totalItems - logsShift <=
                    logs.meta.itemsPerPage
                  )
                    return;
                  const lastPageNr = Math.ceil(
                    logs.meta.totalItems / logs.meta.itemsPerPage
                  );
                  this.setState(
                    {
                      logPageNr: lastPageNr
                    },
                    () => {
                      this.props.dispatch(
                        getLogs(
                          this.state.logPageNr,
                          this.logDateFrom,
                          logsShowOnlyErrors
                        )
                      );
                    }
                  );
                }}
              />
              <NavbarDivider />
              <Button
                icon={IconNames.REFRESH}
                onClick={() => {
                  this.setState(
                    {
                      logPageNr: 1
                    },
                    () => {
                      this.logDateFrom = null;
                      this.props.dispatch(
                        getLogs(this.state.logPageNr, null, logsShowOnlyErrors)
                      );
                    }
                  );
                }}
              />
              <NavbarDivider />
              <Slider
                value={Math.ceil(
                  ((this.state.logPageNr - 1) /
                    (logs.meta.totalItems / logs.meta.itemsPerPage)) *
                    100
                )}
                min={0}
                max={100}
                stepSize={1}
                labelStepSize={100}
                onChange={val => {
                  const logPageNr =
                    Math.floor(
                      (logs.meta.totalItems * (val / 100)) /
                        logs.meta.itemsPerPage
                    ) + 1;
                  this.setState({ logPageNr });
                }}
                onRelease={val => {
                  this.props.dispatch(
                    getLogs(
                      this.state.logPageNr,
                      this.logDateFrom,
                      logsShowOnlyErrors
                    )
                  );
                }}
                showTrackFill={false}
                labelRenderer={val => {
                  return `${val}%`;
                }}
              />
              <NavbarDivider />
              <Switch
                inline={true}
                checked={logsShowOnlyErrors}
                label={
                  <span style={{ whiteSpace: 'pre' }}>Show only errors</span>
                }
                onChange={e => {
                  this.setState(
                    { logPageNr: 1 },
                    (e => {
                      this.props.dispatch(
                        setLogsShowOnlyErrors(e.target.checked)
                      );
                    })(e)
                  );
                }}
              />
            </NavbarGroup>
          )}
        </NavbarGroup>
        <NavbarGroup align={Alignment.RIGHT}>
          <Popover
            position={Position.BOTTOM_RIGHT}
            content={
              <Menu>
                <MenuItem
                  text="Terms of service"
                  onClick={e => {
                    this.setState({ isPolicyDialogOpen: true });
                  }}
                />
                <MenuItem
                  text="Sign out"
                  onClick={e => {
                    e.preventDefault();
                    auth.logout();
                    store.dispatch(
                      updateUserData({
                        username: '',
                        loggedIn: false
                      })
                    );
                    // Reset book item data
                    resetBookItemsData();
                    window.location = '/login';
                  }}
                />
              </Menu>
            }
          >
            <Button minimal={true} icon={IconNames.USER} />
          </Popover>
        </NavbarGroup>
      </Navbar>
    );
  }
}

const mapStateToProps = ({
  bookItems,
  savedContent,
  bookFinished,
  bookItemIdsToIndex,
  isGettingIndexData,
  logs,
  logsShowOnlyErrors,
  showSettings,
  theme,
  userData
}) => ({
  bookItems,
  savedContent,
  bookFinished,
  bookItemIdsToIndex,
  isGettingIndexData,
  logs,
  logsShowOnlyErrors,
  showSettings,
  theme,
  userData
});

export default connect(mapStateToProps)(withRouter(TopNavbar));
