import React from 'react';
import { Button, Classes } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { hotkeys, hotkey_display } from 'react-keyboard-shortcuts';

class ButtonIndexUpComponent extends React.Component {
  hot_keys = {
    'alt+k': {
      priority: 1,
      handler: e => this.up()
    }
  };

  up() {
    const { currentIndexNodeId, locatorsButtons } = this.props;
    let nextIndex = currentIndexNodeId;
    // look possible 3 level deep to exclude locator-less indeces
    for (let i = 0; i < 4; i++) {
      nextIndex =
        nextIndex - 1 < 1 ? Object.keys(locatorsButtons).length : nextIndex - 1;
      if (locatorsButtons[nextIndex] && locatorsButtons[nextIndex].length) {
        break;
      }
    }
    const el = document.getElementById(
      `akisBookIndexDetailButton-${nextIndex}-0`
    );
    if (el) {
      el.click();
    }
  }

  render() {
    return (
      <Button
        title={hotkey_display('Alt+K')}
        icon={IconNames.CARET_UP}
        className={Classes.SMALL}
        disabled={Object.keys(this.props.locatorsButtons).length === 0}
        onClick={() => {
          this.up();
        }}
      />
    );
  }
}

class ButtonIndexDownComponent extends React.Component {
  hot_keys = {
    'alt+m': {
      priority: 1,
      handler: e => this.down()
    }
  };

  down() {
    const { currentIndexNodeId, locatorsButtons } = this.props;
    let nextIndex = currentIndexNodeId;
    // look possible 3 level deep to exclude locator-less indeces
    for (let i = 0; i < 4; i++) {
      nextIndex =
        nextIndex >= Object.keys(locatorsButtons).length ? 1 : nextIndex + 1;
      if (locatorsButtons[nextIndex] && locatorsButtons[nextIndex].length) {
        break;
      }
    }
    const el = document.getElementById(
      `akisBookIndexDetailButton-${nextIndex}-0`
    );
    if (el) {
      el.click();
    }
  }

  render() {
    return (
      <Button
        title={hotkey_display('Alt+M')}
        icon={IconNames.CARET_DOWN}
        className={Classes.SMALL}
        disabled={Object.keys(this.props.locatorsButtons).length === 0}
        onClick={() => {
          this.down();
        }}
      />
    );
  }
}

const ButtonJSONDownload = props => {
  return (
    <Button
      className={Classes.SMALL}
      icon={IconNames.DIAGRAM_TREE}
      title="Download index tree JSON file"
      disabled={!props.indexData.Right}
      onClick={e => {
        const json = JSON.stringify(props.indexData.Right, null, 2);
        const blobUrl = URL.createObjectURL(
          new Blob([json], { type: 'octet/stream' })
        );
        const link = document.createElement('a');
        link.href = blobUrl;
        link.download = 'akis-index-tree.json';
        document.body.appendChild(link);
        link.click();
        setTimeout(function() {
          document.body.removeChild(link);
          window.URL.revokeObjectURL(blobUrl);
        }, 0);
      }}
    />
  );
};

const ButtonTXTDownload = props => {
  return (
    <Button
      className={Classes.SMALL}
      icon={IconNames.LIST}
      title="Download index tree TXT file"
      disabled={!props.indexData.Right}
      onClick={e => {
        if (!props.indexData.Right || !props.indexData.Right.length) return;
        const EOL = '\r\n';
        let txt = '';

        function replaceNewLines(str) {
          return str
            .trim()
            .replace('index\n{', 'index{')
            .replace('index {', 'index{')
            .replace('\r\n', ' ')
            .replace('\n', ' ');
        }

        function getIndexStrBody(indexStr) {
          const indexStrSanitized = replaceNewLines(indexStr);
          const tempStr = indexStrSanitized.substring(7);
          return tempStr.substr(0, tempStr.length - 1);
        }

        function traverse(obj) {
          if (
            obj.locators &&
            obj.locators.length &&
            obj.locators[0].originTex
          ) {
            txt += getIndexStrBody(obj.locators[0].originTex) + EOL;
          }
          if (obj.hasOwnProperty('indexTree')) {
            obj['indexTree'].forEach(obj => {
              traverse(obj);
            });
          }
        }

        props.indexData.Right.forEach(item => {
          traverse(item);
        });
        const blobUrl = URL.createObjectURL(
          new Blob([txt], { type: 'octet/stream' })
        );
        const link = document.createElement('a');
        link.href = blobUrl;
        link.download = 'akis-index-tree.txt';
        document.body.appendChild(link);
        link.click();
        setTimeout(function() {
          document.body.removeChild(link);
          window.URL.revokeObjectURL(blobUrl);
        }, 0);
      }}
    />
  );
};

export { ButtonJSONDownload, ButtonTXTDownload };
export const ButtonIndexUp = hotkeys(ButtonIndexUpComponent);
export const ButtonIndexDown = hotkeys(ButtonIndexDownComponent);
