import React from 'react';
import { connect } from 'react-redux';
import { trim } from 'validator';
import {
  Callout,
  Classes,
  Dialog,
  FormGroup,
  InputGroup,
  Intent,
  Button
} from '@blueprintjs/core';

import * as constants from '../lib/constants';
import { changePassword } from '../services/userSettings';
import { TopToaster } from '../components/TopToaster';

class ChangePasswordDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      newPasswordRepeat: '',
      message: '',
      processing: false
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onInputChange(e) {
    const { name, value } = e.target;
    const state = {};
    state[name] = value;
    this.setState(state);
  }

  checkError() {
    const oldPassword = trim(this.state.oldPassword);
    const newPassword = trim(this.state.newPassword);
    const newPasswordRepeat = trim(this.state.newPasswordRepeat);

    if (oldPassword === '' || newPassword === '' || newPasswordRepeat === '') {
      return constants.ERROR_ALL_FIELDS_REQUIRED;
    }
    if (newPassword !== newPasswordRepeat) {
      return constants.ERROR_PASSWORDS_DO_NOT_MATCH;
    }
    if (newPassword === oldPassword) {
      return constants.ERROR_PASSWORDS_NEW_OLD_EQUAL;
    }
    if (newPassword.length < 8) {
      return constants.ERROR_PASSWORD_TOO_SHORT_8;
    }
    return null;
  }

  onSubmit(e) {
    e.preventDefault();
    const error = this.checkError();
    if (error) {
      return this.setState({ message: error });
    }
    const oldPassword = trim(this.state.oldPassword);
    const newPassword = trim(this.state.newPassword);
    const userData = { oldPassword, newPassword };
    this.setState({ processing: true }, () => {
      this.props.dispatch(
        changePassword(userData, error => {
          this.setState({ processing: false }, () => {
            if (error) {
              this.setState({ message: 'Error while changing password.' });
            } else {
              TopToaster.show({
                intent: Intent.SUCCESS,
                message: 'Successfully changed password.'
              });
              this.props.onClose();
            }
          });
        })
      );
    });
  }

  render() {
    const theme =
      this.props.theme === constants.THEME_DARK ? Classes.DARK : Classes.LIGHT;
    return (
      <Dialog
        isOpen={this.props.isOpen}
        canOutsideClickClose={false}
        onClose={() => {
          this.setState({
            message: '',
            processing: false,
            oldPassword: '',
            newPassword: '',
            newPasswordRepeat: ''
          });
          this.props.onClose();
        }}
        onOpening={e => {
          this.setState({
            message: '',
            processing: false,
            oldPassword: '',
            newPassword: '',
            newPasswordRepeat: ''
          });
        }}
        title="Change password"
        className={theme}
      >
        <form onSubmit={this.onSubmit}>
          <div className={Classes.DIALOG_BODY}>
            <Callout
              intent={Intent.DANGER}
              style={{
                display: this.state.message ? 'initial' : 'none',
                lineHeight: '50px'
              }}
            >
              {this.state.message}
            </Callout>
            <FormGroup
              label="Old password"
              labelFor="old-password"
              labelInfo="(required)"
            >
              <InputGroup
                id="old-password"
                name="oldPassword"
                placeholder="Old password..."
                value={this.state.oldPassword}
                onChange={this.onInputChange}
                type="password"
                disabled={this.state.processing}
              />
            </FormGroup>
            <FormGroup
              label="New password"
              labelFor="new-password"
              labelInfo="(required)"
            >
              <InputGroup
                id="new-password"
                name="newPassword"
                placeholder="New password..."
                value={this.state.newPassword}
                onChange={this.onInputChange}
                type="password"
                disabled={this.state.processing}
              />
            </FormGroup>
            <FormGroup
              label="New password repeat"
              labelFor="new-password-repeat"
              labelInfo="(required)"
            >
              <InputGroup
                id="new-password-repeat"
                name="newPasswordRepeat"
                placeholder="New password repeat..."
                value={this.state.newPasswordRepeat}
                onChange={this.onInputChange}
                type="password"
                disabled={this.state.processing}
              />
            </FormGroup>
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <Button
              onClick={this.onSubmit}
              text="Change password"
              fill={true}
              intent={Intent.PRIMARY}
              type="submit"
              loading={this.state.processing}
            />
          </div>
        </form>
      </Dialog>
    );
  }
}

const mapStateToProps = ({ theme }) => ({
  theme
});

export default connect(mapStateToProps)(ChangePasswordDialog);
