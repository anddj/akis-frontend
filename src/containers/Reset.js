import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import {
  Button,
  Callout,
  Card,
  Elevation,
  InputGroup,
  Intent,
  Label,
  Icon
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { isEmail, trim } from 'validator';

import Header from '../components/Header';
import config from '../config/appConfig';

class Reset extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sent: false,
      email: '',
      message:
        'Please enter your email below, you will receive ' +
        'instructions how to reset your password.'
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  static verifyEmail(email) {
    return trim(email) && isEmail(email);
  }

  send(cb) {
    // Make the API call, get response status
    const api = `${config.api}/userreset`;
    try {
      const initObject = {
        method: 'POST',
        body: JSON.stringify({
          email: this.state.email
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      };
      fetch(api, initObject)
        .then(res => {
          if (res.status === 404) {
            return cb('We do not have this email registered.');
          }
          if (res.status === 429) {
            return cb(
              'Too many password reset requests for this email address.' +
                ' Please try later (or check your mail box for instrucitons' +
                " we've already sent)."
            );
          }
          if (res.ok) {
            return cb(null);
          }
          return cb(res.status + ' ' + res.statusText);
        })
        .catch(err => {
          console.log(err);
          cb('Error.');
        });
    } catch (e) {
      console.log(e);
      cb('Error.');
    }
  }

  onSubmit(event) {
    event.preventDefault();
    const { email } = this.state;
    const ERROR_INVALID_EMAIL = 'Please provide with the valid email address.';
    try {
      const isValid = Reset.verifyEmail(email);
      if (this.state.error) {
        // Error already shown, let's draw attention to
        // repeated error.
        this.setState({ error: 'Verifying...' });
        setTimeout(() => {
          if (!isValid) {
            return this.setState({ error: ERROR_INVALID_EMAIL });
          }
          this.send(err => {
            if (err) {
              return this.setState({ error: err });
            }
            this.setState({ sent: true });
          });
        }, 400);
      } else {
        if (!isValid) {
          return this.setState({ error: ERROR_INVALID_EMAIL });
        }
        this.send(err => {
          if (err) {
            return this.setState({ error: err });
          }
          this.setState({ sent: true });
        });
      }
    } catch (e) {
      console.log(e);
      this.setState({ error: 'Error.' });
    }
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%'
        }}
      >
        <Header />
        {!this.state.sent && (
          <Card
            elevation={Elevation.FOUR}
            style={{
              padding: '30px',
              width: '30%'
            }}
          >
            <Callout>{this.state.message}</Callout>
            {this.state.error && (
              <Callout intent={Intent.DANGER} icon={null}>
                {this.state.error}
              </Callout>
            )}
            <form onSubmit={this.onSubmit}>
              <Label>
                <InputGroup
                  large={true}
                  placeholder="Email"
                  leftIcon={IconNames.ENVELOPE}
                  onChange={e => {
                    this.setState({ email: e.target.value });
                  }}
                />
              </Label>
              <div>
                <Button
                  fill={true}
                  intent={Intent.PRIMARY}
                  large={true}
                  text="Send"
                  type="submit"
                />
              </div>
            </form>
          </Card>
        )}
        {this.state.sent && (
          <Card elevation={Elevation.FOUR}>
            <Callout intent={Intent.SUCCESS}>
              Request received. You will receive an email shortly with the
              instructions how to reset your password.
            </Callout>
            <div
              style={{
                textAlign: 'right',
                paddingTop: '50px'
              }}
            >
              <em>
                <Link to="/login">Back to sign in page</Link>
              </em>
              <Icon
                icon={IconNames.CHEVRON_RIGHT}
                intent={Intent.PRIMARY}
                style={{ marginLeft: '10px' }}
              />
            </div>
          </Card>
        )}
      </div>
    );
  }
}

export default withRouter(Reset);
