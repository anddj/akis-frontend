import React from 'react';
import { IconNames } from '@blueprintjs/icons';
import { Icon, Intent } from '@blueprintjs/core';

import MainContainer from './MainContainer';

class NoMatch extends React.Component {
  render() {
    return (
      <MainContainer>
        <div className="akisFullHeight">
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <Icon
                icon={IconNames.DELETE}
                intent={Intent.DANGER}
                iconSize={50}
                style={{ marginRight: '5px' }}
              />
              <span>Oops... page not found.</span>
            </div>
          </div>
        </div>
      </MainContainer>
    );
  }
}

export default NoMatch;
