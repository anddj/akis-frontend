import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Card, Elevation, Intent, Icon } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

import Header from '../components/Header';

class RegisterAlmostDone extends React.Component {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          height: '100%',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <Header />
        <Card elevation={Elevation.FOUR}>
          <h3>We are almost done!</h3>
          <p>
            You will receive an email shortly. Please follow instructions found
            there to complete registration.
          </p>
          <div
            style={{
              textAlign: 'right',
              paddingTop: '50px'
            }}
          >
            <em>
              <Link to="/login">Back to sign in page</Link>
            </em>
            <Icon
              icon={IconNames.CHEVRON_RIGHT}
              intent={Intent.PRIMARY}
              style={{ marginLeft: '10px' }}
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default withRouter(RegisterAlmostDone);
