const config = {};

config.vtexMode = false;
config.storageProvider = config.vtexMode ? localStorage : sessionStorage;

config.siteTitleShort = 'AKIS';
config.siteTitleLong = 'subject indexing service';

config.api = 'http://localhost:3000/api';

// Allow time to renew token (secs)
config.graceSecs = 60 * 60 * 2; //2h

// Allow register only these domains (email)
config.registerEmailsAllowed = ['example.com'];

config.buildIndexParamsDefault = {
  version: '1.0.0',
  forbidAnywhere:
    'about,aftermost,afterward,afterwards,again,ahead,although,also,among,an,and,appendix,are,as,aside,at,awkward,be,because,becomes,been,begin,begins,behind,between,book,both,broadly,but,by,can,cannot,case,chapters,clumsy,cm,copyrigth,could,creates,deals,despite,do,does,dots,down,driven,due,each,enables,evaluate,evaluates,even,ever,exhibit,exploit,fairly,famous,farther,figures,for,from,fulfill,fulfillment,fulfills,further,goes,had,has,have,he,her,here,herein,how,i,ideally,if,in,including,indeed,into,is,it,its,just,kind,km,largest,looks,make,makes,mathdisp,may,meant,merely,might,mm,modifies,more,most,must,need,needs,no,nor,not,noting,now,numerous,of,obtains,on,our,one,ones,or,other,others,over,out,outside,parentheses,perhaps,possible,prefer,prefers,pt,refs,regarding,remains,remain,she,should,since,small,smallest,so,sometimes,still,such,terms,than,that,the,their,them,themselves,then,there,thereby,therefore,these,they,those,think,this,through,thus,to,today,tomorrow,towards,trying,twice,under,unlikely,unusual,up,used,uses,using,versus,via,vs,was,we,well,went,were,whatsoever,when,whenever,where,whereas,whereby,wherein,whereupon,which,whichever,while,whilst,who,whose,will,willing,wish,with,wishes,would,yet,yields,you,your,a,s,d,f,g,h,j,k,l,z,x,c,v,b,n,m,q,w,e,r,t,y,u,i,o,p',
  forbidEndings:
    'above,after,al,along,already,always,all,allows,already,around,becomes,been,below,but,certain,clumsy,defines,describing,down,during,enough,eq,everywhere,exist,exploits,follows,given,helps,higher,inside,like,lower,near,often,relates,require,requires,seeks,seems,show,shows,such,toward,towards,throughout,us,uses,within,without',
  entilements: 'chapter,cite,fig,figure,mathdisp,picture,ref,table,url',
  numbers:
    'one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,first,second,third,fourth,fith,sixth,seventh,eighth,ninth,tenth,eleventh,famous,thereof,goto',
  skipTex:
    '\\begin{verbatimtab},\\footnote,\\begin{quote},\\begin{verbatim},\\author,\\institute,\\address,\\fnms,\\fnm,\\snm,\\ead,\\orgname,\\adrline,\\city,\\state,\\postcode,\\cny,\\begin{abstract},\\begin{frontmatter},\\begin{Furtherreading}',
  useMacroExpander: true,
  returnExpandTex: false,
  normalizeEndings: ':es,:s,y:ies,is:es',
  combineStendByTerms: true,
  makeTree: true,
  indexTreeFlatTerms: 3,
  alsoIncludeTermsWith: '',
  includeExtendedTerms: true,
  includeAcronymExpansions: true,
  termCount: 200
};

export default config;
