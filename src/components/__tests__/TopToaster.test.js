import React from 'react';
import ReactDOM from 'react-dom';
import { Intent } from '@blueprintjs/core';

import { TopToaster } from '../TopToaster';

it('renders without crashing', () => {
  TopToaster.show({
    intent: Intent.SUCCESS,
    message: 'Just testing...'
  });
});
