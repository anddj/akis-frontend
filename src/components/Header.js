import React from 'react';
import { withRouter, Link } from 'react-router-dom';

import config from '../config/appConfig';

const Header = function(props) {
  return (
    <div
      style={{
        position: 'absolute',
        left: 0,
        top: 0,
        height: '50px',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        paddingLeft: '7px',
        borderBottom: '3px solid #eee'
      }}
    >
      <Link to="/">
        <h2 style={{ color: '#ccc' }}>{config.siteTitleShort}</h2>
      </Link>
    </div>
  );
};

export default withRouter(Header);
