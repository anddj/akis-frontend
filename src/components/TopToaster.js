import { Position, Toaster } from '@blueprintjs/core';

const TopToaster = Toaster.create({
  position: Position.BOTTOM
});

export { TopToaster };
