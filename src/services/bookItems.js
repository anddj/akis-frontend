import { Intent } from '@blueprintjs/core';

import { TopToaster } from '../components/TopToaster';
import appConfig from '../config/appConfig';
import {
  setBookItemActive,
  setBookItems,
  setBookItemsBuffers,
  setFileNamesMap,
  setSavedContent,
  updateBookItems,
  updateSavedContent
} from '../actions/actionCreators';
import { initFetchHeaders, handleErrors } from './utils.js';
import { TOASTER_TIMEOUT } from '../lib/constants';

const sendBookItems = bookItems => {
  const url = `${appConfig.api}/bookitems`;
  const headers = initFetchHeaders({
    'Content-Type': 'application/json'
  });
  return fetch(url, {
    headers,
    method: 'PATCH',
    body: JSON.stringify(bookItems)
  });
};

const fetchBookItems = bookItems => {
  const url = `${appConfig.api}/bookitems`;
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'GET'
  });
};

// Async action, thunk
export const saveBookItems = bookItems => {
  return dispatch => {
    if (!bookItems || bookItems.length === 0) {
      return new Promise(function(resolve, reject) {
        reject(new Error('No items to save.'));
      });
    }
    return sendBookItems(bookItems)
      .then(handleErrors)
      .then(response => response.json(), error => console.log(error))
      .then(data => {
        return new Promise(function(resolve, reject) {
          try {
            dispatch(updateBookItems(data.bookItems));
            dispatch(updateSavedContent(data.bookItems));
            resolve(data.bookItems);
          } catch (e) {
            reject(e);
          }
        });
      });
  };
};

export const getBookItems = () => {
  return dispatch => {
    return fetchBookItems()
      .then(handleErrors)
      .then(response => response.json(), error => console.log(error))
      .then(data => {
        if (!data) {
          console.log('No book items data received.');
          return;
        }
        dispatch(setBookItems(data.bookItems));
        dispatch(setBookItemsBuffers(data.bookItems));
        dispatch(setSavedContent(data.bookItems));
        dispatch(setFileNamesMap(data.bookItems));
        if (data.bookItems && data.bookItems.length > 0) {
          dispatch(setBookItemActive(data.bookItems[0]._id));
        }
        TopToaster.show({
          intent: Intent.SUCCESS,
          message:
            'Loaded ' +
            (data.bookItems ? data.bookItems.length : '0') +
            ' book items.',
          timeout: TOASTER_TIMEOUT
        });
      });
  };
};
