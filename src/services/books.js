import { Intent } from '@blueprintjs/core';

import appConfig from '../config/appConfig';
import { handleErrors, makeTimeDelta, initFetchHeaders } from './utils.js';
import { TopToaster } from '../components/TopToaster';
import { updateBookFinished } from '../actions/actionCreators';

function fetchBookUpdate(book) {
  const url = `${appConfig.api}/booksmeta`,
    headers = initFetchHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    });

  return fetch(url, {
    headers,
    method: 'PATCH',
    body: JSON.stringify(book)
  });
}

function fetchBookMeta() {
  const url = `${appConfig.api}/booksmeta`,
    headers = initFetchHeaders({
      Accept: 'application/json'
    });

  return fetch(url, {
    headers,
    method: 'GET'
  });
}

function showError(err, timeStart) {
  console.log(err);
  return TopToaster.show({
    message: `${String(err)} (${makeTimeDelta(timeStart)} secs)`,
    intent: Intent.DANGER,
    timeout: 0
  });
}

const fetchBookZip = () => {
  const url = `${appConfig.api}/books?checkfinished=false`,
    headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'GET'
  });
};

export const updateBook = book => {
  return dispatch => {
    const timeStart = new Date();
    return fetchBookUpdate(book)
      .then(handleErrors)
      .then(
        response => response.json(),
        error => {
          showError(error, timeStart);
        }
      )
      .then(data => {
        if (typeof data === 'undefined') {
          throw Error('No data received');
        }
        dispatch(updateBookFinished(data.is_finished));
      })
      .catch(error => {
        showError(error, timeStart);
      });
  };
};

export const getBookMeta = () => {
  return dispatch => {
    const timeStart = new Date();
    return fetchBookMeta()
      .then(handleErrors)
      .then(
        response => response.json(),
        error => {
          if (String(error) !== 'Error: Not Found') {
            showError(error, timeStart);
          }
        }
      )
      .then(data => {
        if (typeof data === 'undefined') {
          console.log('No data received');
          return;
        }
        dispatch(updateBookFinished(data.is_finished));
      })
      .catch(error => {
        showError(error, timeStart);
      });
  };
};

export const getBookZip = () => {
  return dispatch => {
    return fetchBookZip()
      .then(handleErrors)
      .then(
        response => response.blob(),
        error => {
          console.log(error);
          TopToaster.show({
            intent: Intent.DANGER,
            message: String(error)
          });
        }
      )
      .then(blob => {
        const blobUrl = URL.createObjectURL(
          new Blob([blob], { type: 'octet/stream' })
        );
        const link = document.createElement('a');
        link.href = blobUrl;
        link.download = 'akis-book.zip';
        document.body.appendChild(link);
        link.click();
        setTimeout(function() {
          document.body.removeChild(link);
          window.URL.revokeObjectURL(blobUrl);
        }, 0);
      });
  };
};
