import { Intent } from '@blueprintjs/core';

import appConfig from '../config/appConfig';
import {
  setConcordanceLines,
  setFontScaling,
  setTheme,
  setUserSettings
} from '../actions/actionCreators';
import { TopToaster } from '../components/TopToaster';
import {
  THEME_DARK,
  THEME_LIGHT,
  TOASTER_TIMEOUT,
  DEFAULT_CONCORDANCE_LINES,
  DEFAULT_FONT_SCALING
} from '../lib/constants';
import { initFetchHeaders, handleErrors } from './utils.js';

const fetchChangePassword = userData => {
  const url = `${appConfig.api}/userchangepwd`;
  const headers = initFetchHeaders({
    'Content-Type': 'application/json'
  });
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify(userData)
  });
};

const fetchDeleteUser = () => {
  const url = `${appConfig.api}/usersettings`;
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'DELETE'
  });
};

const fetchUserSettings = () => {
  const url = `${appConfig.api}/usersettings`;
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'GET'
  });
};

const fetchClientConfig = () => {
  const url = `${appConfig.api}/clientconfig`;
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'GET'
  });
};

const sendUserSettings = userSettings => {
  const url = `${appConfig.api}/usersettings`;
  const headers = initFetchHeaders({
    'Content-Type': 'application/json'
  });
  return fetch(url, {
    headers,
    method: 'PATCH',
    body: JSON.stringify(userSettings)
  });
};

export const getClientConfig = () => {
  return dispatch => {
    return fetchClientConfig()
      .then(handleErrors)
      .then(
        response => response.blob(),
        error => {
          console.log(error);
          TopToaster.show({
            intent: Intent.DANGER,
            message: String(error)
          });
        }
      )
      .then(blob => {
        if (!blob) return;
        const blobUrl = URL.createObjectURL(
          new Blob([blob], { type: 'octet/stream' })
        );
        const link = document.createElement('a');
        link.href = blobUrl;
        link.download = 'akis_client.conf';
        document.body.appendChild(link);
        link.click();
        setTimeout(function() {
          document.body.removeChild(link);
          window.URL.revokeObjectURL(blobUrl);
        }, 0);
      });
  };
};

export const getUserSettings = () => {
  return dispatch => {
    return fetchUserSettings()
      .then(handleErrors)
      .then(response => response.json(), error => console.log(error))
      .then(data => {
        if (!data) return;
        dispatch(setUserSettings(data));
        dispatch(setTheme(data.dark_theme ? THEME_DARK : THEME_LIGHT));
        dispatch(
          setFontScaling(
            data.font_scaling ? data.font_scaling : DEFAULT_FONT_SCALING
          )
        );
        dispatch(
          setConcordanceLines(
            data.concordance_lines
              ? data.concordance_lines
              : DEFAULT_CONCORDANCE_LINES
          )
        );
      });
  };
};

export const saveUserSettings = userSettings => {
  return dispatch => {
    if (!userSettings) {
      return TopToaster.show({
        intent: Intent.DANGER,
        message: 'No user settings to save.'
      });
    }
    return sendUserSettings(userSettings)
      .then(response => response.json(), error => console.log(error))
      .then(data => {
        dispatch(setUserSettings(data));
        TopToaster.show({
          intent: Intent.SUCCESS,
          message: 'Saved user settings',
          timeout: TOASTER_TIMEOUT
        });
      });
  };
};

export const deleteUser = callback => {
  return dispatch => {
    return fetchDeleteUser()
      .then(handleErrors)
      .then(response => response.json())
      .then(data => {
        if (!data) {
          if (callback) callback();
          return;
        }
        setTimeout(() => {
          window.location = '/login';
        }, 1500);
      })
      .catch(error => {
        console.log(error);
        TopToaster.show({
          intent: Intent.DANGER,
          message: 'Error while removing user account.',
          timeout: 0
        });
        if (callback) callback();
      });
  };
};

export const changePassword = (userData, callback) => {
  return dispatch => {
    return fetchChangePassword(userData)
      .then(handleErrors)
      .then(response => response.json())
      .then(data => {
        if (!data) {
          if (callback) callback('Error');
          return;
        }
        setTimeout(() => {
          if (callback) callback(null);
        }, 1500);
      })
      .catch(error => {
        console.log(error);
        TopToaster.show({
          intent: Intent.DANGER,
          message: 'Error while changing password.'
        });
        if (callback) callback('Error');
      });
  };
};
