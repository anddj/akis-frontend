import React from 'react';
import { Intent, Pre, Divider } from '@blueprintjs/core';

import appConfig from '../config/appConfig';
import { initFetchHeaders, handleErrors, makeTimeDelta } from './utils.js';
import { TopToaster } from '../components/TopToaster';
import {
  setIndexData,
  toggleIsGettingIndexData,
  updateBookItems,
  setEditorLinkedDocs
} from '../actions/actionCreators';
import store from '../lib/store';
import { TOASTER_TIMEOUT } from '../lib/constants';

const fetchIndex = bookItemIdsToIndex => {
  let queryString = bookItemIdsToIndex.length > 0 ? '?' : '';
  bookItemIdsToIndex.forEach((item, index) => {
    const delim = (index === 0 ? '' : '&') + 'bookItemIdsToIndex[]=';
    queryString += delim + item;
  });
  const url = `${appConfig.api}/bookindex${queryString}`;
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'GET'
  });
};

const fetchBuildIndex = bookItemIdsToIndex => {
  let queryString = bookItemIdsToIndex.length > 0 ? '?' : '';
  bookItemIdsToIndex.forEach((item, index) => {
    const delim = (index === 0 ? '' : '&') + 'bookItemIdsToIndex[]=';
    queryString += delim + item;
  });
  const url = `${appConfig.api}/bookindexbuild${queryString}`;
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'POST'
  });
};

export const buildIndex = bookItemsIdsToIndex => {
  return dispatch => {
    const timeStart = new Date();
    return fetchBuildIndex(bookItemsIdsToIndex)
      .then(response => {
        try {
          const respObj = response.json();
          return respObj;
        } catch (e) {
          return response.blob();
        }
      })
      .then(data => {
        if (typeof data === 'undefined') {
          console.log('No data received');
          TopToaster.show({
            message: 'No data received',
            intent: Intent.DANGER,
            timeout: 0
          });
          return;
        }
        if (data.error) {
          console.log(String(data.error));
          TopToaster.show({
            message: (
              <div>
                <Pre style={{ whiteSpace: 'pre-wrap' }}>
                  <code>{String(data.error)}</code>
                </Pre>
                <Divider />
                <div>{makeTimeDelta(timeStart) + ' secs'}</div>
              </div>
            ),
            intent: Intent.DANGER,
            timeout: 0
          });
          return;
        }
        const { editorInstance, bookItemsBuffers } = store.getState();
        const markers = editorInstance.getAllMarks();
        store.dispatch(setIndexData({}));
        dispatch(setEditorLinkedDocs({}));
        if (markers) {
          markers.forEach(marker => {
            marker.clear();
          });
        }
        dispatch(updateBookItems(data.annotatedFiles));

        // Update buffers
        let changedFiles = 0;
        data.annotatedFiles.forEach(annotatedFile => {
          changedFiles +=
            bookItemsBuffers[annotatedFile._id].getValue() ===
            annotatedFile.current_content
              ? 0
              : 1;
          bookItemsBuffers[annotatedFile._id].setValue(
            annotatedFile.current_content
          );
        });
        editorInstance.refresh();

        TopToaster.show({
          intent: Intent.SUCCESS,
          message:
            `Built Index (${makeTimeDelta(timeStart)} secs, ` +
            `annotated files: ${changedFiles}/${data.annotatedFiles.length})`,
          timeout: TOASTER_TIMEOUT
        });
      })
      .catch(error => {
        TopToaster.show({
          intent: Intent.DANGER,
          message: `${String(error)} (${makeTimeDelta(timeStart)} secs)`,
          timeout: 0
        });
      })
      .finally(function() {
        if (store.getState().isGettingIndexData) {
          dispatch(toggleIsGettingIndexData());
        }
      });
  };
};

export const getIndex = (bookItemIdsToIndex, cb) => {
  return dispatch => {
    const timeStart = new Date();
    return fetchIndex(bookItemIdsToIndex)
      .then(handleErrors)
      .then(
        response => response.json(),
        error => {
          console.log(error);
          TopToaster.show({
            message: `${String(error)} (${makeTimeDelta(timeStart)} secs)`,
            intent: Intent.DANGER,
            timeout: 0
          });
        }
      )
      .then(data => {
        if (typeof data === 'undefined') {
          console.log('No data received');
          return;
        }
        const { editorInstance } = store.getState();
        store.dispatch(setIndexData({}));
        dispatch(setEditorLinkedDocs({}));
        const markers = editorInstance.getAllMarks();
        if (markers) {
          markers.forEach(marker => {
            marker.clear();
          });
        }
        dispatch(setIndexData(data.indexData));
        if (cb) cb();
        if (data.indexData && data.indexData.Left) {
          TopToaster.show({
            intent: Intent.DANGER,
            message: data.indexData.Left,
            timeout: 0
          });
        } else {
          TopToaster.show({
            intent: Intent.SUCCESS,
            message: `Loaded Index (${makeTimeDelta(timeStart)} secs)`,
            timeout: TOASTER_TIMEOUT
          });
        }
      })
      .catch(error => {
        TopToaster.show({
          intent: Intent.DANGER,
          message: `${String(error)} (${makeTimeDelta(timeStart)} secs)`,
          timeout: 0
        });
      })
      .finally(function() {
        if (store.getState().isGettingIndexData) {
          dispatch(toggleIsGettingIndexData());
        }
      });
  };
};
