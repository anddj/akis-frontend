import { Intent } from '@blueprintjs/core';

import appConfig from '../config/appConfig';
import { setLogs } from '../actions/actionCreators';
import { initFetchHeaders, handleErrors } from './utils.js';
import { TopToaster } from '../components/TopToaster';

const fetchLogs = (pageNr, dateFrom, onlyErrors) => {
  const getOnlyErrors = typeof onlyErrors === 'boolean' ? onlyErrors : false;
  const url =
    `${appConfig.api}/logs?pageNr=${pageNr || 1}` +
    (dateFrom ? `&dateFrom=${dateFrom}` : '') +
    (getOnlyErrors ? `&onlyErrors=${String(getOnlyErrors)}` : '');
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'GET'
  });
};

export const getLogs = (pageNr, dateFrom, onlyErrors) => {
  return dispatch => {
    return fetchLogs(pageNr, dateFrom, onlyErrors)
      .then(handleErrors)
      .then(response => response.json(), error => console.log(error))
      .then(data => {
        dispatch(setLogs(data));
      });
  };
};

const fetchZip = logId => {
  const url = `${appConfig.api}/logs/${logId}/zip`;
  const headers = initFetchHeaders();
  return fetch(url, {
    headers,
    method: 'GET'
  });
};

export const getZip = (logId, cb) => {
  return dispatch => {
    return fetchZip(logId)
      .then(handleErrors)
      .then(response => response.blob(), error => console.log(error))
      .then(data => {
        if (!data) {
          TopToaster.show({
            intent: Intent.DANGER,
            message:
              'Error retrieving file from server. ' +
              'Possibly the file is removed.'
          });
        }
        if (cb) cb(data);
      });
  };
};
