import { makeTimeDelta, handleErrors } from '../utils';

it('makes time delta', () => {
  expect(makeTimeDelta(3 * 1000, 6 * 1000)).toEqual('3.00');
  expect(makeTimeDelta(0, 0)).toEqual('0.00');
  expect(parseFloat(makeTimeDelta(0))).toBeGreaterThan(0);
});

it('handling HTTP response errors', () => {
  const ERR = {
    ok: false,
    statusText: 'my error'
  };

  const OK = {
    ok: true,
    statusText: 'my ok'
  };

  function makeError() {
    handleErrors(ERR);
  }

  expect(makeError).toThrowError('my error');
  expect(handleErrors(OK).statusText).toEqual(OK.statusText);
  expect(handleErrors(OK).ok).toEqual(OK.ok);
});
