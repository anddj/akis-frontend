import config from '../config/appConfig';
const { storageProvider } = config;

// Handling fetch() errors
export function handleErrors(response) {
  if (response.status === 403) {
    return (window.location = '/login');
  }
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export function makeTimeDelta(timeStart, timeEnd) {
  if (typeof timeEnd === 'undefined') {
    timeEnd = new Date();
  }
  return ((timeEnd - timeStart) / 1000).toFixed(2);
}

export function initFetchHeaders(customHeaders) {
  const baseHeaders = {
    'X-Requested-With': 'XMLHttpRequest'
  };
  if (storageProvider && storageProvider.token) {
    baseHeaders['x-access-token'] = storageProvider.token;
  }
  return Object.assign({}, baseHeaders, customHeaders || {});
}
