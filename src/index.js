import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Position, Toaster, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

import './styles/index.css';
import App from './containers/App';
import store from './lib/store';
import * as serviceWorker from './lib/serviceWorker';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

const showToaster = (message, intent, timeout, reload) => {
  const toasterIntent = intent || Intent.WARNING;
  const toasterTimeout = typeof timeout === 'undefined' ? 400 : timeout;
  const toasterAction = reload
    ? {
        onClick: e => {
          window.location.reload(true);
        },
        text: 'RELOAD',
        icon: IconNames.REFRESH
      }
    : null;
  const BottomToaster = Toaster.create({
    position: Position.BOTTOM_RIGHT
  });
  BottomToaster.show({
    message,
    intent: toasterIntent,
    timeout: toasterTimeout,
    action: toasterAction,
    icon: IconNames.INFO_SIGN
  });
};

const configSW = {
  onUpdate: registration => {
    const message =
      'New content is available and will be used when all ' +
      'tabs for this page are closed.';
    showToaster(message, Intent.WARNING, 0, true);
  },
  onSuccess: registration => {
    const message = 'Content is cached for offline use.';
    showToaster(message, Intent.SUCCESS, 2000, false);
  }
};

// Service worker (requires https if in production)
serviceWorker.register(configSW);
