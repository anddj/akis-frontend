import config from '../config/appConfig';
import moment from 'moment';

const API_TOKEN = `${config.api}/token`;
const API_REGISTER = `${config.api}/userregister`;
const { storageProvider } = config;

const Auth = {
  login(username, pass, cb) {
    if (storageProvider.token) {
      if (cb) cb(true);
      this.onChange(true, this.getUsername(), this.getExpires());
      return;
    }
    makeTokenRequest(username, pass, res => {
      if (res.authenticated) {
        storageProvider.token = res.token;
        storageProvider.username = res.username;
        storageProvider.expires = res.expires;
        if (cb) cb(true);
        this.onChange(true, this.getUsername(), this.getExpires());
      } else {
        storageProvider.clear();
        if (cb) cb(false);
        this.onChange(false, null, null);
      }
    });
  },

  register(user, cb) {
    makeRegisterRequest(user, res => {
      if (res.authenticated) {
        if (cb) cb(true, res.code, res.statusText);
      } else {
        if (cb) cb(false, res.code, res.statusText);
      }
    });
  },

  getToken() {
    return storageProvider.token;
  },

  logout(cb) {
    storageProvider.clear();
    if (cb) cb();
    this.onChange(false, null, null);
  },

  validExpiryDate() {
    const expiryDate = Number(this.getExpires());
    const GRACE_SECS = config.graceSecs;
    return expiryDate && expiryDate - GRACE_SECS - moment().valueOf() > 0;
  },

  loggedIn() {
    if (typeof storageProvider === 'undefined') return false;
    return !!storageProvider.token && this.validExpiryDate();
  },

  getUsername() {
    return storageProvider.username;
  },

  getExpires() {
    return !!storageProvider.expires ? storageProvider.expires : null;
  },

  onChange() {}
};

function makeTokenRequest(username, pass, cb) {
  // Maken token request using basic auth header
  setTimeout(() => {
    const req = new XMLHttpRequest();
    req.onload = function() {
      if (req.status === 404 || req.status === 401) {
        cb({ authenticated: false });
      } else {
        let resp = {};
        try {
          resp = JSON.parse(req.response);
        } catch (e) {
          console.log(e);
          return cb({ authenticated: false });
        }
        if (resp.token) {
          return cb({
            authenticated: true,
            token: resp.token,
            username: resp.user.username,
            expires: resp.expires
          });
        }
        cb({ authenticated: false });
      }
    };
    req.open('GET', API_TOKEN);
    req.setRequestHeader(
      'Authorization',
      'Basic ' + btoa(username + ':' + pass)
    );
    req.send();
  }, 0);
}

function makeRegisterRequest(user, cb) {
  setTimeout(() => {
    const req = new XMLHttpRequest();
    req.onload = function() {
      const response = {
        code: req.status,
        statusText: req.getResponseHeader('x-akis-conflict') || req.statusText
      };
      if (req.status < 200 || req.status >= 400) {
        return cb({ ...response, authenticated: false });
      } else {
        return cb({ ...response, authenticated: true });
      }
    };
    req.open('POST', API_REGISTER);
    req.setRequestHeader('Content-type', 'application/json');
    req.send(JSON.stringify(user));
  }, 0);
}

export default Auth;
