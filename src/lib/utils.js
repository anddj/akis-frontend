// utils.js
//
// Utility functions.
//
import {
  setBookItemActive,
  setBookItemIdsToIndex,
  setBookItems,
  setBookItemsBuffers,
  setCurrentIndexLocatorId,
  setCurrentIndexNodeId,
  setEditorGoToCoords,
  setEditorLinkedDocs,
  setFileNamesMap,
  setIndexData,
  setLocatorsButtons,
  setNodesMarkers,
  setSavedContent
} from '../actions/actionCreators';
import store from '../lib/store';

function removeMarkers(markers) {
  if (markers) {
    markers.forEach(marker => {
      marker.clear();
    });
  }
}

function clearMarkers(allBuffers) {
  const { editorInstance, bookItemsBuffers } = store.getState();
  if (!editorInstance) return;
  if (!allBuffers) {
    // Clear markers for the current buffer
    const markers = editorInstance.getAllMarks();
    if (markers) {
      removeMarkers(markers);
    }
  } else {
    // Iterate all buffers
    Object.keys(bookItemsBuffers).forEach(key => {
      const markers = bookItemsBuffers[key].getAllMarks();
      if (markers) {
        removeMarkers(markers);
      }
    });
  }
}

export const resetBookItemsData = () => {
  // Reset book item data
  const { dispatch } = store;
  dispatch(setBookItemActive(null));
  dispatch(setBookItemIdsToIndex([]));
  dispatch(setBookItems([]));
  dispatch(setBookItemsBuffers([]));
  dispatch(setCurrentIndexNodeId(null));
  dispatch(setCurrentIndexLocatorId(null));
  dispatch(setEditorGoToCoords({ line: 0, ch: 0 }));
  dispatch(setFileNamesMap(null));
  dispatch(setSavedContent([]));
  dispatch(setEditorLinkedDocs({}));
  dispatch(setLocatorsButtons({}));
  dispatch(setNodesMarkers({}));
  dispatch(setIndexData({}));

  clearMarkers();
};

export const resetBookIndexData = bookItemId => {
  const { dispatch } = store;
  dispatch(setIndexData({}));
  dispatch(setEditorGoToCoords({ line: 0, ch: 0 }));
  dispatch(setCurrentIndexNodeId(null));
  dispatch(setCurrentIndexLocatorId(null));
  clearMarkers();
};

export function getBrowserHeight() {
  return Math.max(
    document.documentElement.clientHeight,
    window.innerHeight || 0
  );
}

export function scrollIntoView(
  innerElementSelectorId, // "string"
  outerElementSelectorId, // "string"
  behavior, // "auto" | "instant" | "smooth"
  closest // string
) {
  // Scrolls the element into the visible area of the browser window
  const innerEl = document.getElementById(innerElementSelectorId);
  let closestEl = null;
  if (!innerEl) return;
  if (document.body.scrollIntoView) {
    // Use modern browser feature
    if (closest) {
      closestEl = innerEl.closest(closest);
    }
    if (closestEl) {
      closestEl.scrollIntoView({
        block: 'center',
        inline: 'start', // "start" | "center" | "end" | "neareast"
        behavior: behavior || 'auto'
      });
    } else {
      // fallback to innerEl
      innerEl.scrollIntoView({
        block: 'center',
        inline: 'start', // "start" | "center" | "end" | "neareast"
        behavior: behavior || 'auto'
      });
    }
  } else {
    const outerEl = document.getElementById(outerElementSelectorId);
    const outerHeight = Math.max(outerEl.clientHeight, outerEl.offsetHeight);
    const offsetInner = innerEl.getBoundingClientRect().top;
    outerEl.scrollTop = outerEl.scrollTop + offsetInner - outerHeight / 2;
  }
}

export function bringIndexToView(event) {
  // Find underlying marker on editor click.
  // Call bringIndexToView function bound to found marker.
  const editor = this;
  // Transform mouse X/Y coords to editor LINE/CH
  const pos = editor.coordsChar({
    left: event.clientX,
    top: event.clientY
  });
  const markersAtPos = editor.findMarksAt(pos);
  if (!markersAtPos.length) return;
  const lastMarker = markersAtPos[markersAtPos.length - 1]; // last marker
  if (lastMarker.bringIndexToView) {
    lastMarker.bringIndexToView();
  }
  // Select active marker area
  const marker = lastMarker.find();
  editor.doc.setSelection(marker.from, marker.to);
}
