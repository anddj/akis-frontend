import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from '../reducers/index.js';
import * as constants from './constants';

const initialState = {
  bookItemIdsToIndex: [],
  bookItems: [],
  currentIndexNodeId: null,
  currentIndexLocatorId: null,
  editorGoToCoords: { line: 0, ch: 0 },
  editorLinkedDocs: {},
  fontScaling: constants.DEFAULT_FONT_SCALING,
  concordanceLines: constants.DEFAULT_CONCORDANCE_LINES,
  indexData: {},
  logs: {},
  logsShowOnlyErrors: false,
  showSettings: false,
  theme: constants.THEME_LIGHT,
  userSettings: {},
  userData: {
    loggedIn: false,
    username: ''
  },
  locatorsButtons: {},
  nodesMarkers: {},
  showConcordances: false
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
