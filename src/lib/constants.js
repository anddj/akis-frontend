export const THEME_LIGHT = 'light';
export const THEME_DARK = 'dark';
export const TOASTER_TIMEOUT = 1000;

export const FILES_LIST_SPLIT_PERCENTAGE_MAX = 35;
export const FILES_LIST_SPLIT_PERCENTAGE_MIN = 8;

// Index Tree
export const AKIS_INDEX_LABEL = 'akisIndexLabel';

// CodeMirror
export const CODEMIRROR_THEME_LIGHT = 'neo';
export const CODEMIRROR_THEME_DARK = 'lucario';
export const CODEMIRROR_MODE_STEX = 'stex';

export const MARKER_CLASS = 'akisEditorMarker';
export const MARKER_CLASS_ALT = 'akisEditorMarkerAlt';
export const MARKER_CLASS_ALT2 = 'akisEditorMarkerAlt2';

export const BUTTON_CLASS_ACTIVE = 'akisButtonActive';
export const BUTTON_CLASS_ALT = 'akisButtonAlt';

export const DEFAULT_FONT_SCALING = 110;
export const DEFAULT_CONCORDANCE_LINES = { top: 1, bottom: 1 };

export const AKIS_INDEX_ITEM_OVERLAY = 'akisIndexItemOverlay';

// Validation error messages
export const ERROR_ALL_FIELDS_REQUIRED = 'Please fill in all required fields.';
export const ERROR_PASSWORDS_DO_NOT_MATCH = 'Passwords do not match.';
export const ERROR_PASSWORDS_NEW_OLD_EQUAL =
  'New and old passwords are the same.';
export const ERROR_PASSWORD_TOO_SHORT_8 =
  'Password should contain 8 or more symbols.';
export const ERROR_EMAIL_NOT_VALID = 'Please enter valid email address.';
export const ERROR_LIMITED_DOMAINS_ONLY =
  'Registration is limited to specific domains only.';
export const ERROR_USERNAME_TOO_LONG_30 =
  'Username length should not exceed 30 characters.';
export const ERROR_USERNAME =
  'Please enter valid username. It should contain only latin ' +
  'alphabet letters or numbers/dot and begin with the letter.';
export const ERROR_USERNAME_ENDS_DOT = 'Username cannot end with the dot.';
export const ERROR_USERNAME_MULTIPLE_DOTS =
  'Username cannot contain more than one dot character.';
export const ERROR_FIRSTNAME_LENGTH_30 =
  'Firstname value length should not exceed 30 characters';
export const ERROR_FIRSTNAME_FIRST_LETTER =
  'Firstname should begin with the letter.';
export const ERROR_FIRSTNAME_NOT_VALID = 'Firstname is not valid.';
export const ERROR_LASTNAME_LENGTH_30 =
  'Lastname value length should not exceed 30 characters';
export const ERROR_LASTNAME_FIRST_LETTER =
  'Lastname should begin with the letter.';
export const ERROR_LASTNAME_NOT_VALID = 'Lastname is not valid.';

// Error messages
export const ERROR_TO_MANY_REQUESTS =
  'We are receiving too many requests at the moment. Please try later.';
