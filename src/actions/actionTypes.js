// User
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
export const SET_THEME = 'SET_THEME';

// UI
export const TOGGLE_THEME = 'TOGGLE_THEME';
export const TOGGLE_SETTINGS = 'TOGGLE_SETTINGS';

// Book
export const UPDATE_BOOK_FINISHED = 'UPDATE_BOOK_FINISHED';

// Book items
export const SET_BOOK_ITEMS = 'SET_BOOK_ITEMS';
export const SET_BOOK_ITEMS_BUFFERS = 'SET_BOOK_ITEMS_BUFFERS';
export const SET_BOOK_ITEM_ACTIVE = 'SET_BOOK_ITEM_ACTIVE';
export const SET_SAVED_CONTENT = 'SET_SAVED_CONTENT';
export const UPDATE_BOOK_ITEM = 'UPDATE_BOOK_ITEM';
export const UPDATE_BOOK_ITEMS = 'UPDATE_BOOK_ITEMS';
export const UPDATE_SAVED_CONTENT = 'UPDATE_SAVED_CONTENT';

// Index data
export const SET_BOOK_ITEM_IDS_TO_INDEX = 'SET_BOOK_ITEM_IDS_TO_INDEX';
export const SET_CURRENT_INDEX_LOCATOR_ID = 'SET_CURRENT_INDEX_LOCATOR_ID';
export const SET_CURRENT_INDEX_NODE_ID = 'SET_CURRENT_INDEX_NODE_ID';
export const SET_EDITOR_INSTANCE = 'SET_EDITOR_INSTANCE';
export const SET_FILE_NAMES_MAP = 'SET_FILE_NAMES_MAP';
export const SET_INDEX_DATA = 'SET_INDEX_DATA';
export const SET_LOCATORS_BUTTONS = 'SET_LOCATORS_BUTTONS';
export const SET_NODES_MARKERS = 'SET_NODES_MARKERS';
export const TOGGLE_IS_GETTING_INDEX_DATA = 'TOGGLE_IS_GETTING_INDEX_DATA';

// Editor
export const SET_EDITOR_GO_TO_COORDS = 'SET_EDITOR_GO_TO_COORDS';
export const SET_EDITOR_LINKED_DOCS = 'SET_EDITOR_LINKED_DOCS';
export const SET_SHOW_CONCORDANCES = 'SET_SHOW_CONCORDANCES';

// Logs
export const SET_LOGS = 'SET_LOGS';
export const SET_LOGS_SHOW_ONLY_ERRORS = 'SET_LOGS_SHOW_ONLY_ERRORS';

// User Settings
export const SET_USER_SETTINGS = 'SET_USER_SETTINGS';
export const SET_FONT_SCALING = 'SET_FONT_SCALING';
export const SET_CONCORDANCE_LINES = 'SET_CONCORDANCE_LINES';
