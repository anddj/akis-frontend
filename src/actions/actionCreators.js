import {
  SET_BOOK_ITEMS,
  SET_BOOK_ITEMS_BUFFERS,
  SET_BOOK_ITEM_ACTIVE,
  SET_BOOK_ITEM_IDS_TO_INDEX,
  SET_CONCORDANCE_LINES,
  SET_CURRENT_INDEX_LOCATOR_ID,
  SET_CURRENT_INDEX_NODE_ID,
  SET_EDITOR_GO_TO_COORDS,
  SET_EDITOR_INSTANCE,
  SET_EDITOR_LINKED_DOCS,
  SET_FILE_NAMES_MAP,
  SET_FONT_SCALING,
  SET_INDEX_DATA,
  SET_LOCATORS_BUTTONS,
  SET_LOGS,
  SET_LOGS_SHOW_ONLY_ERRORS,
  SET_SAVED_CONTENT,
  SET_THEME,
  SET_USER_SETTINGS,
  TOGGLE_IS_GETTING_INDEX_DATA,
  TOGGLE_SETTINGS,
  TOGGLE_THEME,
  UPDATE_BOOK_FINISHED,
  UPDATE_BOOK_ITEM,
  UPDATE_BOOK_ITEMS,
  UPDATE_SAVED_CONTENT,
  UPDATE_USER_DATA,
  SET_NODES_MARKERS,
  SET_SHOW_CONCORDANCES
} from './actionTypes';

const makeActionCreator = (type, ...argNames) => {
  return function(...args) {
    const action = { type };
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index];
    });
    return action;
  };
};

// User
export const updateUserData = makeActionCreator(UPDATE_USER_DATA, 'data');
export const setTheme = makeActionCreator(SET_THEME, 'theme');

// UI
export const toggleTheme = makeActionCreator(TOGGLE_THEME);
export const toggleSettings = makeActionCreator(TOGGLE_SETTINGS);

// Editor
export const setEditorGoToCoords = makeActionCreator(
  SET_EDITOR_GO_TO_COORDS,
  'editorGoToCoords'
);

export const setEditorInstance = makeActionCreator(
  SET_EDITOR_INSTANCE,
  'editor'
);

export const setEditorLinkedDocs = makeActionCreator(
  SET_EDITOR_LINKED_DOCS,
  'editorLinkedDocs'
);

export const setShowConcordances = makeActionCreator(
  SET_SHOW_CONCORDANCES,
  'showConcordances'
);

// Logs
export const setLogs = makeActionCreator(SET_LOGS, 'logs');
export const setLogsShowOnlyErrors = makeActionCreator(
  SET_LOGS_SHOW_ONLY_ERRORS,
  'logsShowOnlyErrors'
);

// Book
export const updateBookFinished = makeActionCreator(
  UPDATE_BOOK_FINISHED,
  'bookFinished'
);

// Book items
export const setBookItems = makeActionCreator(SET_BOOK_ITEMS, 'bookItems');
export const setBookItemsBuffers = makeActionCreator(
  SET_BOOK_ITEMS_BUFFERS,
  'bookItems'
);
export const updateBookItems = makeActionCreator(
  UPDATE_BOOK_ITEMS,
  'bookItems'
);
export const setSavedContent = makeActionCreator(
  SET_SAVED_CONTENT,
  'bookItems'
);
export const updateSavedContent = makeActionCreator(
  UPDATE_SAVED_CONTENT,
  'bookItems'
);
export const setBookItemActive = makeActionCreator(
  SET_BOOK_ITEM_ACTIVE,
  'bookItemId'
);
export const updateBookItem = makeActionCreator(UPDATE_BOOK_ITEM, 'bookItem');

// Index data
export const setIndexData = makeActionCreator(SET_INDEX_DATA, 'indexData');
export const setCurrentIndexNodeId = makeActionCreator(
  SET_CURRENT_INDEX_NODE_ID,
  'currentIndexNodeId'
);
export const setCurrentIndexLocatorId = makeActionCreator(
  SET_CURRENT_INDEX_LOCATOR_ID,
  'currentIndexLocatorId'
);
export const setBookItemIdsToIndex = makeActionCreator(
  SET_BOOK_ITEM_IDS_TO_INDEX,
  'bookItemIds'
);
export const toggleIsGettingIndexData = makeActionCreator(
  TOGGLE_IS_GETTING_INDEX_DATA,
  'isGettingIndexData'
);
export const setLocatorsButtons = makeActionCreator(
  SET_LOCATORS_BUTTONS,
  'locatorsButtons'
);
export const setNodesMarkers = makeActionCreator(
  SET_NODES_MARKERS,
  'nodesMarkers'
);

// Mapping path -> ObjectId
export const setFileNamesMap = makeActionCreator(
  SET_FILE_NAMES_MAP,
  'bookItems'
);

// User settings
export const setUserSettings = makeActionCreator(
  SET_USER_SETTINGS,
  'userSettings'
);
export const setFontScaling = makeActionCreator(
  SET_FONT_SCALING,
  'fontScaling'
);
export const setConcordanceLines = makeActionCreator(
  SET_CONCORDANCE_LINES,
  'concordanceLines'
);
